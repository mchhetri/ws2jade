/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import org.apache.axis.encoding.ser.BeanDeserializerFactory;
import org.apache.axis.encoding.ser.BeanSerializerFactory;
import org.apache.axis.wsdl.symbolTable.TypeEntry;
import org.apache.axis.wsdl.toJava.Emitter;
import org.apache.axis.wsdl.toJava.JavaBeanWriter;
import org.apache.axis.wsdl.toJava.Namespaces;
import org.apache.axis.wsdl.toJava.Utils;

import ciamas.wsjade.wsdl2jade.mapping.TypeMapping;
import ciamas.wsjade.wsdl2jade.utils.BeanUtils;
import ciamas.wsjade.wsdl2jade.utils.Common;


import org.apache.axis.Constants;
import org.apache.axis.utils.JavaUtils;

import org.apache.axis.wsdl.symbolTable.ContainedAttribute;
import org.apache.axis.wsdl.symbolTable.ElementDecl;
import org.apache.axis.wsdl.symbolTable.SchemaUtils;



public class JadeBeanWriter extends JavaBeanWriter {
	private TypeEntry type;

	private Vector elements;

	private Vector attributes;

	private TypeEntry extendType;
	private JadeBeanHelperWriter helper;

	public JadeBeanWriter(Emitter emitter, TypeEntry type, Vector elements,
			TypeEntry extendType, Vector attributes, JadeBeanHelperWriter helper) {
		super(emitter, type, elements, extendType, attributes, helper);
		this.type = type;
		this.elements = elements;
		this.attributes = attributes;
		this.extendType = extendType;
		this.helper = helper;

		//Im not going to mess up with the namespace
		this.namespaces = new Namespaces(Common.getJadeOutputDir());
		this.packageName = Common.getFullReflectPkg()
				+ Utils.getJavaPackageName(type.getName());
		this.className = Utils.getJavaLocalName(type.getName());

		enableSetters = true;
		enableGetters = true;
	}

	protected void writeHeaderComments(PrintWriter pw) throws IOException {
		Common.printBanner(pw);

	}

	protected void formatJadeType() {
		GenCache cache = GenCache.getInstance();
		for (int i = 0; i < names.size(); i += 2) {
			String typeName = (String) names.get(i);
			String variable = (String) names.get(i + 1);

			//if(TypeMapping.isComplex(typeName))
			//typeName =Common.getFullReflectPkg() + typeName;

			String javaRealType = typeName;
			typeName = TypeMapping.getJadeDataType(javaRealType, false);
			names.set(i, typeName);
			//System.out.println(typeName +" "+ variable);

			//Add into GenCache the mapping name
			cache.record(packageName + "." + className, type.getQName()
					.getLocalPart(), variable, javaRealType, typeName);

			//Register onto for later print out
			(new BeanUtils(typeName, javaRealType, variable))
					.register(packageName + "." + className);

		}

	}

	protected String getImplementsText() {
		return "implements jade.content.Concept";
	}

	protected void parseVarValue()
	{
		 // Add element names
        if (elements != null) {
            for (int i = 0; i < elements.size(); i++) {
                ElementDecl elem = (ElementDecl) elements.get(i);
                String typeName = elem.getType().getName();
                String variableName;

                if (elem.getAnyElement()) {
                    typeName = "org.apache.axis.message.MessageElement []";
                    variableName = Constants.ANYCONTENT;
                    isAny = true;
                } else {
                    variableName = elem.getName();
                    if (elem.getType().getUnderlTypeNillable() 
                        || (elem.getNillable() && elem.getMaxOccursIsUnbounded())) {
                   
                        typeName = Utils.getWrapperType(elem.getType());
			
                    } else if (elem.getMinOccursIs0() && elem.getMaxOccursIsExactlyOne()
                            || elem.getNillable() || elem.getOptional()) {
                 
                        typeName = Utils.getWrapperType(typeName);
                    } 
                }
                //System.out.println(variableName + " " + typeName);
                // Make sure the property name is not reserved.
                variableName = JavaUtils.getUniqueValue(
                       helper.reservedPropNames, variableName);
                names.add(typeName);
                names.add(variableName);

                if (type.isSimpleType()
                        && (variableName.endsWith("Value")
                        || variableName.equals("_value"))) {
                    simpleValueTypes.add(typeName);
                }

             
                if (null != Utils.getEnumerationBaseAndValues(
                        elem.getType().getNode(), emitter.getSymbolTable())) {
                    enumerationTypes.add(typeName);
                }
            }
        }

        if (enableMemberFields && SchemaUtils.isMixed(type.getNode())) {
            isMixed = true;
            if (!isAny) {
                names.add("org.apache.axis.message.MessageElement []");
                names.add(Constants.ANYCONTENT);
            }
        }

        // Add attribute names
        if (attributes != null) {

            for (int i = 0; i < attributes.size(); i++) {
                ContainedAttribute attr = (ContainedAttribute) attributes.get(i);
                String typeName = attr.getType().getName();
                String variableName = attr.getName();

                //System.out.println(variableName + " " + typeName);
		if (attr.getOptional()) {
		    typeName = Utils.getWrapperType(typeName);
		}

                // Make sure the property name is not reserved.
                variableName = JavaUtils.getUniqueValue(
                        helper.reservedPropNames, variableName);

                names.add(typeName);
                names.add(variableName);

                if (type.isSimpleType()
                        && (variableName.endsWith("Value")
                        || variableName.equals("_value"))) {
                    simpleValueTypes.add(typeName);
                }

              
                if (null != Utils.getEnumerationBaseAndValues(attr.getType().getNode(),
                        emitter.getSymbolTable())) {
                    enumerationTypes.add(typeName);
                }
            }
        }

        if ((extendType != null) && extendType.getDimensions().equals("[]")) {
            String typeName = extendType.getName();
            String elemName = extendType.getQName().getLocalPart();
            String variableName = Utils.xmlNameToJava(elemName);

            names.add(typeName);
            names.add(variableName);
        }

  
        for (int i = 1; i < names.size(); i +=2)
        {
            int suffix = 2;     
            String s = (String) names.elementAt(i);
            if (i < names.size() - 2)
            {
                int dup = names.indexOf(s, i+1);
                while (dup > 0)
                {
                   
                    names.set(dup, names.get(dup) + Integer.toString(suffix));
                    suffix++;
                  
                    if (i >= names.size() - 2)
                        break;
                    dup = names.indexOf(s, dup+1);
                }
            }

        }
	}
	protected void preprocess() {
		parseVarValue();

		//format to jade type & register for Onto
		formatJadeType();

		//register for TypeMapping
		WSTypeMappingTemp typeMappingTemp = WSTypeMappingTemp.getInstance();
		String tName = packageName + "." + className + ".class";
		tName = tName.replace('\\', '.');
		tName = tName.replace('/', '.');
		typeMappingTemp.register(type.getQName(), tName,
				BeanSerializerFactory.class, BeanDeserializerFactory.class);

	}

	protected void writeFileBody(PrintWriter pw) throws IOException {

		this.pw = pw;

		preprocess();

		// Write Member Fields
		writeMemberFields();

		// Write the default constructor
		if (enableDefaultConstructor) {
			writeDefaultConstructor();
		}

		// Write Full Constructor
		if (enableFullConstructor) {
			writeFullConstructor();
		}

		// Write SimpleConstructors
		if (enableSimpleConstructors) {
			writeSimpleConstructors();
		}

		// Write ToString method
		if (enableToString) {
			writeToStringMethod();
		}

		// Write accessor methods
		writeAccessMethods();

	} // writeFileBody

	protected void writeAccessMethods() {
		int j = 0;
		// Define getters and setters for the bean elements
		for (int i = 0; i < names.size(); i += 2, j++) {
			String typeName = (String) names.get(i);
			String name = (String) names.get(i + 1);
			String capName = Utils.capitalizeFirstChar(name);

			String get = "get";
			//if (typeName.equals("boolean"))
			//  get = "is";

			if (enableGetters) {
				pw.println("    public " + typeName + " " + get + capName
						+ "() {");
				pw.println("        return " + name + ";");
				pw.println("    }");
				pw.println();
			}
			if (enableSetters) {
				pw.println("    public void set" + capName + "(" + typeName
						+ " " + name + ") {");
				pw.println("        this." + name + " = " + name + ";");
				pw.println("    }");
				pw.println();
			}

		}
	}

}