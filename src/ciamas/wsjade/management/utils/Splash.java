/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.management.utils;

import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class Splash extends JFrame {

	/**
	 * Method showSplash.
	 */
	public Splash() {
		super();
		initGraph();
	}

	public void initGraph() {

		ImageIcon spl = new ImageIcon(Utility.class
				.getResource("resources/splash.png"));
		JLabel l = new JLabel();
		l.setSize(310, 232);
		l.setIcon(spl);
		getContentPane().add(l);
		setSize(310, 232);
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenSize.width - 400) / 2, (screenSize.height - 300) / 2);
		setUndecorated(true);
		setVisible(true);

	}

}