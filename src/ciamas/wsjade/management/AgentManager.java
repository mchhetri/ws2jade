/*
 * Created on 4/02/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ciamas.wsjade.management;
import java.io.BufferedReader;
import java.io.FileReader;
import java.util.StringTokenizer;


import ciamas.wsjade.management.utils.NodeObject;
import ciamas.wsjade.wsdl2jade.utils.Common;
import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.onto.*;
import jade.core.*;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import jade.content.onto.basic.*;
import jade.content.lang.*;
import jade.content.lang.sl.*;
import org.apache.log4j.Logger;
/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class AgentManager  extends Agent {

		// CREATE THE ONTOLOGY INSTANCE
        private ContentManager manager  = (ContentManager) getContentManager();
        private Codec codec = new SLCodec();
        private Ontology ontology = ciamas.wsjade.wsdl2jade.onto.WebAdminOntology.getInstance();
        private ciamas.wsjade.management.utils.Admin admin = ciamas.wsjade.management.utils.Admin.getInstance();
		
        protected void setup() {

		// REGISTER THE ONTOLOGY
		manager.registerLanguage(codec);
		manager.registerOntology(ontology);
		

		//	ADD SETUPWSAGBEHAVIOUR
		addBehaviour(new SetupWSAGBehaviour());
		// ADD HANDLEINFORMBEHAVIOUR
		addBehaviour(new HandleInformBehaviour(this));

	   }
        
        
	  class SetupWSAGBehaviour extends OneShotBehaviour 
	  {
	  	Logger log = Logger.getLogger("WS2JADE");
	  	public void action()
	  	{
	  		loadAgents();
	  	}
	  	//FIX ME: Send to myself if one containr is used !
	  	private void sendActivateMsg(String agentName, String serviceName, String ontName)
        {
	  		
        	//        	 CREATE A REQUEST MESSAGE
    		ACLMessage requestMsg = new ACLMessage(ACLMessage.REQUEST); 		
    		requestMsg.setSender(getAID());

    		// IDENTIFY THE WEB SERVICE AGENT 
    		requestMsg.addReceiver(new AID(agentName, false)); 
    		requestMsg.setLanguage(codec.getName());
    		requestMsg.setOntology(ontology.getName());
    		ciamas.wsjade.wsdl2jade.utils.AddOntAction requestAction = 
    		new ciamas.wsjade.wsdl2jade.utils.AddOntAction ();
    		requestAction.setOntClassName(ontName);
    		requestAction.setServiceName(serviceName);
    
    	     try 
    	     { 
    			// SEND THE MESSAGE
    			manager.fillContent( requestMsg, 
    					new Action(new AID("WSManager", false), requestAction));
    					
    			send(requestMsg);
    			log.info("Activate "+ agentName);
    			 //System.out.println("Agent service name="+ serviceName);
    	     } 
    	     catch(Exception e) 
    	     {// e.printStackTrace(); 
    	     	log.debug("Exception occurred", e);
    	     	
    	     	
    	     }
        }
	  	private  synchronized void loadAgents()
		{
			try{
				
			java.io.File file = new java.io.File(".wsag");
			if(!file.exists())
			{
				log.info("No startup file. ");
				return;
			}
				BufferedReader br = new BufferedReader(new FileReader(".wsag")); 
				String contents; 
				String line; 
				   while ((line = br.readLine()) != null) 
				   {  // while loop begins here
			          StringTokenizer st = new StringTokenizer(line, "|");
			          if(st.countTokens()!=3)
			          {
			          	log.warn("Warning: Corrupted data in startup file");
			          	continue;
			          }
			          String agentName = st.nextToken().trim();
			          String agentService = st.nextToken().trim();
			         
			          String wsURI = st.nextToken().trim();
			          //ciamas.wsjade.wsdl2jade.utils.WSAgent agent = new ciamas.wsjade.wsdl2jade.utils.WSAgent();
			  		  admin.deployAgent(agentName);		  		  
			  		  
			  		Common.setServiceName(agentService);
					Common.setAgentName(agentName);
					Common.setServiceURL(wsURI);
					Status.registerActionStatus(wsURI);
					
			  		  //Create new Object to send to the agent
			  		ciamas.wsjade.wsdl2jade.utils.AddOntAction addOntAction = new ciamas.wsjade.wsdl2jade.utils.AddOntAction();
			  		String ontoCls = Common.getJadeGenRootPkg() + ".onto." + agentService + ".WebOntology";
			  		this.sendActivateMsg(agentName, agentService, ontoCls);
			  		
			  		javax.swing.tree.DefaultMutableTreeNode agentNode = admin.getGUI().addAgent(agentName, true);
			  		NodeObject agentnodeObject = new NodeObject(agentName, null, true);
			  		agentNode.setUserObject(agentnodeObject);
			  		
			  		javax.swing.tree.DefaultMutableTreeNode serviceNode = admin.getGUI().addService(agentService, wsURI, agentNode);  		
			  		NodeObject serviceNodeObject = new NodeObject(agentService, null, true);
			  		serviceNode.setUserObject(serviceNodeObject);
			  		
			  		ASWSRecord record = new ASWSRecord(agentService, wsURI);
			  		CardinalMappingManager.addRecord(agentName, record);
			  		//System.out.println(admin.getAgentNode("PayFriendProvider"));
			          
			        }
				   br.close();


			}catch(Exception e)
			{
				//System.out.println(e.getMessage());
				log.debug("Exception occurred", e);
				
			}

		}
	  }
	   
     class HandleInformBehaviour extends CyclicBehaviour
     {
     	Logger log = Logger.getLogger("WS2JADE");
        public HandleInformBehaviour(Agent a) { super(a); }
        
        
        public void action() 
        {
            ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
            if (msg != null) 
            {
             try 
             {
                //ContentElement ce = manager.extractContent(msg);
                ContentElement ce = (ContentElement) ((Action) manager
						.extractContent(msg)).getAction();
                
                //Debug
                //System.out.println("Get message type " +ce.getClass());
                ACLMessage resmsg = msg.createReply();
                resmsg.setPerformative(ACLMessage.INFORM);
                ciamas.wsjade.wsdl2jade.onto.DeploymentResponse response=new 
						ciamas.wsjade.wsdl2jade.onto.DeploymentResponse();
                			
					
				if (ce instanceof ciamas.wsjade.wsdl2jade.onto.AgentDeploymentRequest) 
				{
					ciamas.wsjade.wsdl2jade.onto.AgentDeploymentRequest req = (ciamas.wsjade.wsdl2jade.onto.AgentDeploymentRequest)ce;
					//Check authen
					String req_aName=req.getAgentName();
					if(Authorization.getPassword().equals(req.getPassword()) && 
							Authorization.getUsername().equals(req_aName))
					{
						if(admin.deployAgent(req_aName))
						{
							response.setSuccess(true);				
							response.setReason("");
							javax.swing.tree.DefaultMutableTreeNode agentNode = admin.getGUI().addAgent(req_aName, true);
					  		NodeObject agentnodeObject = new NodeObject(req_aName, null, true);
					  		agentNode.setUserObject(agentnodeObject);
						}
						else
						{
							response.setSuccess(false);
							response.setReason("Agent exists");
						}
					}
					else
					{
						response.setSuccess(false);
						response.setReason("Fail Authentication");
					}
				} 
				if (ce instanceof ciamas.wsjade.wsdl2jade.onto.ServiceDeploymentRequest) 
				{
					ciamas.wsjade.wsdl2jade.onto.ServiceDeploymentRequest req = (ciamas.wsjade.wsdl2jade.onto.ServiceDeploymentRequest)ce;
					//Check authen
					if(Authorization.getPassword().equals(req.getPassword()) && 
							Authorization.getUsername().equals(req.getUsername()))
					{
						String req_sName = req.getServiceName();
						String req_aName = req.getAgentName();
						String req_uRI = req.getUri();
						
						//How to check if agent exist ?
						boolean newAgent = admin.deployAgent(req_aName);
						
						if(admin.activate(req.getServiceName(), req.getUri(), req.getAgentName(), true))
						{
							javax.swing.tree.DefaultMutableTreeNode agentNode =null;
							if(newAgent)
							{
								agentNode = admin.getGUI().addAgent(req_aName, true);
						  		NodeObject agentnodeObject = new NodeObject(req_aName, null, true);
						  		agentNode.setUserObject(agentnodeObject);
							}
							else
								agentNode = admin.getAgentNode(req_aName);
					  		
					  		javax.swing.tree.DefaultMutableTreeNode serviceNode = admin.getGUI().addService(req_sName, req_uRI, agentNode);  		
					  		NodeObject serviceNodeObject = new NodeObject(req_sName, null, true);
					  		serviceNode.setUserObject(serviceNodeObject);
					  		
					  		ASWSRecord record = new ASWSRecord(req_sName, req_uRI);
					  		CardinalMappingManager.addRecord(req_aName, record);
					  		
							response.setSuccess(true);
							response.setReason("");
						}
						else
						{
							response.setSuccess(false);
							response.setReason("Fail to deploy");
						}
							
					}
					else
					{
						response.setSuccess(false);
						response.setReason("Fail Authentication");
					}
				} 
		
				jade.content.onto.basic.Result result = new Result((Action)(manager.extractContent(msg)), response);
				manager.fillContent(resmsg , result);
				send(resmsg );
				
               } catch(Exception e) { 
               	//FIX ME: Return failure to client
               	log.debug("Exception occurred", e); }
            } else { block(); }
    	} 
    } 
} 




