/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.wsdl2jade.projection.owl;

import java.io.IOException;
import java.util.Vector;

import javax.xml.namespace.QName;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.symbolTable.SchemaUtils;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.symbolTable.TypeEntry;
import org.apache.axis.wsdl.toJava.Utils;
import org.w3c.dom.Node;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class OWLTypeWriter implements Generator {

	OWLComplexTypeWriter typeWriter = null;

	public OWLTypeWriter(OWLParser parser, TypeEntry type,
			SymbolTable symbolTable) {
		if (type.isReferenced() && !type.isOnlyLiteralReferenced()) {

			// Determine what sort of type this is and instantiate
			// the appropriate Writer.
			Node node = type.getNode();

			{

				// Generate the proper class for either "complex" or
				// "enumeration" types
				Vector v = Utils.getEnumerationBaseAndValues(node, symbolTable);
				if (v != null) {
					// typeWriter = getEnumTypeWriter(parser, type, v);
				} else {
					TypeEntry base = SchemaUtils
							.getComplexElementExtensionBase(node, symbolTable);
					if (base == null) {
						QName baseQName = SchemaUtils.getSimpleTypeBase(node);
						if (baseQName != null) {
							base = symbolTable.getType(baseQName);
						}
					}

					typeWriter = getBeanWriter(parser, type,
							SchemaUtils.getContainedElementDeclarations(node,
									symbolTable), base, SchemaUtils
									.getContainedAttributeTypes(node,
											symbolTable));
				}
			}

			// If the holder is needed (ie., something uses this type as an out
			// or inout
			// parameter), instantiate the holder writer.
			// if (holderIsNeeded(type)) {
			//    holderWriter = getHolderWriter(parser, type);
			//}
		}

	}

	public void generate() throws IOException {
		typeWriter.generate();
	}

	protected OWLComplexTypeWriter getBeanWriter(OWLParser owlParser,
			TypeEntry type, Vector elements, TypeEntry base, Vector attributes) {

		return new OWLComplexTypeWriter(owlParser, type, elements, base,
				attributes);
	}

}