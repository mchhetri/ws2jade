/*
 * Created on 2/12/2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package samples;

/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

import jade.content.*;
import jade.content.onto.*;
import jade.core.*;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import jade.util.leap.*;
import jade.content.onto.basic.*;
import jade.content.lang.*;
import jade.content.lang.sl.*;


import JadeGen.onto.Amazon.AmazonSearchPort.Request.KeywordSearchRequest;
import JadeGen.onto.reflect.com.amazon.soap.Details;



public class AmazonClientAgent extends Agent {

	// CREATE THE ONTOLOGY INSTANCE
        private ContentManager manager  = (ContentManager) getContentManager();
        private Codec codec = new SLCodec();
        private Ontology ontology = JadeGen.onto.Amazon.WebOntology.getInstance();

        protected void setup() {

		// REGISTER THE ONTOLOGY
		manager.registerLanguage(codec);
		manager.registerOntology(ontology);

		// ADD HANDLEINFORMBEHAVIOUR
		addBehaviour(new HandleInformBehaviour(this));

		// CREATE A REQUEST MESSAGE
		ACLMessage requestMsg = new ACLMessage(ACLMessage.REQUEST);
		
		requestMsg.setSender(getAID());

		// IDENTIFY THE WEB SERVICE AGENT 
		requestMsg.addReceiver(new AID("AmazonProvider", false)); 
		requestMsg.setLanguage(codec.getName());
		requestMsg.setOntology(ontology.getName());
	


		// INSTANTIATE THE CHOSEN AGENTACTION CLASS
		JadeGen.onto.reflect.com.amazon.soap.KeywordRequest keyword = new JadeGen.onto.reflect.com.amazon.soap.KeywordRequest();
		
		keyword.setKeyword("Java");
		keyword.setType("heavy");
		keyword.setDevtag("0B3BB4TCP1Y5236QJSR2");
		keyword.setMode("books");
		keyword.setTag("");
		keyword.setLocale("");
		keyword.setPage("1");
		keyword.setSort("");
		keyword.setVariations("");
		
		KeywordSearchRequest requestAction = 
		new KeywordSearchRequest();
		requestAction.setKeywordSearchRequest(keyword);
	
	   
		System.out.println("Client Agent search for java books");
	     try 
	     { 
			// SEND THE MESSAGE
			manager.fillContent( requestMsg, 
					new Action(new AID("AmazonClientAgent", false), requestAction));
					
			send(requestMsg);
		
	     } 
	     catch(Exception e) 
	     { e.printStackTrace(); }
	   }
	   
	   
     class HandleInformBehaviour extends CyclicBehaviour
     {
        public HandleInformBehaviour(Agent a) { super(a); }
        
        public void action() 
        {
            ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));
            if (msg != null) 
            {
             try 
             {
                ContentElement ce = manager.extractContent(msg);
					
				if (ce instanceof jade.content.onto.basic.Result) 
				{
					jade.content.onto.basic.Result result = (jade.content.onto.basic.Result)ce;
					System.out.println();
					System.out.print("Returned value: ");
					JadeGen.onto.Amazon.AmazonSearchPort.Response.KeywordSearchRequest	ret =
						
					(JadeGen.onto.Amazon.AmazonSearchPort.Response.KeywordSearchRequest)result.getValue();
					 JadeGen.onto.reflect.com.amazon.soap.ProductInfo info =ret.get_return();
					 
					 ArrayList detailList = info.getDetails();
					 for(int i=0;i<detailList.size();i++)
					 {
					 	Details d = (Details)detailList.get(i);
					 	System.out.println(d.getAsin());
					 	System.out.println(d.getProductName());
					 }
		
				} 
		
               } catch(Exception e) { e.printStackTrace(); }
            } else { block(); }
    	} 
    } 
} 



