/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.management.utils;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class OWLViewer extends JFrame implements ActionListener {

	JPanel displayPanel = new JPanel();

	JPanel buttonPanel = new JPanel();

	JEditorPane editPane = new JEditorPane();

	JButton button = new JButton("ok");

	public OWLViewer() {
		super("OWLViewer");
		init();
	}

	/**
	 *  
	 */
	public void init() {

		JScrollPane scrollPane = new JScrollPane(editPane);

		displayPanel.add(scrollPane);
		editPane.setPreferredSize(new Dimension(400, 300));
		buttonPanel.setLayout(new BorderLayout());
		buttonPanel.add(button, BorderLayout.CENTER);
		button.addActionListener(this);

		this.getContentPane().add(displayPanel, BorderLayout.CENTER);
		this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);

		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(WindowEvent winEvt) {
				dispose();
			}
		});
	}

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == button) {
			dispose();
		}
	}

	public void setText(String s) {
		editPane.setText(s);
	}

	public static void main(String[] s) {
		OWLViewer o = new OWLViewer();
		o.pack();
		o.show();
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		o.setLocation((screenSize.width - 400) / 2,
				(screenSize.height - 300) / 2);

	}

}