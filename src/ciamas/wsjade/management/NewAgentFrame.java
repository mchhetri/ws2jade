package ciamas.wsjade.management;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import org.apache.log4j.Logger;

/**
* This code was generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a
* for-profit company or business) then you should purchase
* a license - please visit www.cloudgarden.com for details.
*/
public class NewAgentFrame extends javax.swing.JFrame   implements
java.awt.event.ActionListener{

	private RemoteAgentManager mgr;
	private JButton cancelBtn;
	private JButton okBtn;
	private JTextField agentNameTf;
	private JLabel jLabel2;
	private JLabel jLabel1;
	private JPanel mainPanel;
	private Logger log = Logger.getLogger("WS2JADE");
	
	public NewAgentFrame() {
		initGUI();
	}

	/**
	* Initializes the GUI.
	* Auto-generated code - any changes you make will disappear.
	*/
	public void initGUI(){
		try {
			preInitGUI();
	
			mainPanel = new JPanel();
			jLabel1 = new JLabel();
			jLabel2 = new JLabel();
			agentNameTf = new JTextField();
			okBtn = new JButton();
			cancelBtn = new JButton();
	
			BorderLayout thisLayout = new BorderLayout();
			this.getContentPane().setLayout(thisLayout);
			thisLayout.setHgap(0);
			thisLayout.setVgap(0);
			this.setSize(new java.awt.Dimension(292,131));
	
			mainPanel.setLayout(null);
			mainPanel.setPreferredSize(new java.awt.Dimension(353,104));
			this.getContentPane().add(mainPanel, BorderLayout.CENTER);
	
			jLabel1.setText("New Agent");
			jLabel1.setPreferredSize(new java.awt.Dimension(67,25));
			jLabel1.setBounds(new java.awt.Rectangle(120,6,67,25));
			mainPanel.add(jLabel1);
	
			jLabel2.setText("Agent Name");
			jLabel2.setPreferredSize(new java.awt.Dimension(79,20));
			jLabel2.setBounds(new java.awt.Rectangle(4,43,79,20));
			mainPanel.add(jLabel2);
	
			agentNameTf.setPreferredSize(new java.awt.Dimension(174,20));
			agentNameTf.setBounds(new java.awt.Rectangle(91,41,174,20));
			mainPanel.add(agentNameTf);
	
			okBtn.setText("OK");
			okBtn.setPreferredSize(new java.awt.Dimension(81,20));
			okBtn.setBounds(new java.awt.Rectangle(95,72,81,20));
			mainPanel.add(okBtn);
	
			cancelBtn.setText("Cancel");
			cancelBtn.setPreferredSize(new java.awt.Dimension(79,20));
			cancelBtn.setBounds(new java.awt.Rectangle(183,72,79,20));
			mainPanel.add(cancelBtn);
	
			postInitGUI();
		} catch (Exception e) {
			//e.printStackTrace();
			log.debug("Exception occurred", e);
		}
	}
	/** Add your pre-init code in here 	*/
	public void preInitGUI(){
	}

	/** Add your post-init code in here 	*/
	public void postInitGUI(){
		okBtn.addActionListener(this);
		cancelBtn.addActionListener(this);
	}
	public void setManager(RemoteAgentManager manager)
	{
		mgr=manager;
	}
	/** Auto-generated main method */
	public static void main(String[] args){
		showGUI();
	}
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(!(mgr ==null))
			if(e.getSource()==okBtn)
			mgr.requestNewAgent(agentNameTf.getText().trim());		
		else
			System.out.println("Warning, manager is null !");
	
		this.setVisible(false);
		agentNameTf.setText("");

	}

	/**
	* This static method creates a new instance of this class and shows
	* it inside a new JFrame, (unless it is already a JFrame).
	*
	* It is a convenience method for showing the GUI, but it can be
	* copied and used as a basis for your own code.	*
	* It is auto-generated code - the body of this method will be
	* re-generated after any changes are made to the GUI.
	* However, if you delete this method it will not be re-created.	*/
	public static void showGUI(){
		try {
			NewAgentFrame inst = new NewAgentFrame();
			inst.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
			
		}
	}
}
