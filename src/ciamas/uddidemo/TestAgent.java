package ciamas.uddidemo;


import jade.core.*;

import api_v2.uddi_org.InquireMessagesLocator;
import api_v2.uddi_org.Name;
import org.apache.axis.client.Stub;
import api_v2.uddi_org.TModelInfo;
import api_v2.uddi_org.ArrayOfTModelInfo;
import api_v2.uddi_org.TModelList;
import api_v2.uddi_org.FindTModel;
import api_v2.uddi_org.InquireMessages;
import api_v2.uddi_org.InquireMessagesSoap;

public class TestAgent extends Agent {
    public TestAgent() {
        
        
    }

    

    public int search_Type = 3;
    public String search_Term = "Microsoft";

    public void setup() {
        try
        {
            System.setProperty("http.proxyHost", "wwwproxy.swin.edu.au");
            System.setProperty("http.proxyPort", "8000");

            InquireMessages locator = new InquireMessagesLocator();

            InquireMessagesSoap s =	locator.getInquireMessagesSoap();
            ((Stub)s).setTimeout(5*60*1000);

            Name[] names = {new Name("Microsoft")};

            //                FindService find = new FindService();
            //                FindBusiness find = new FindBusiness();
            FindTModel find = new FindTModel();

            find.setName("microsoft");
            find.setGeneric("2.0");
            find.setMaxRows(3);

            TModelList sList = s.findTModel(find);
            ArrayOfTModelInfo sLists = sList.getTModelInfos();
            TModelInfo[] info = sLists.getTModelInfo();
            for(int i=0;i< info.length;i++)
            {
                String name = info[i].getName();
                String tModel = info[1].getTModelKey();
                String owner = info[1].toString();
                System.out.println("Name " + name + "\ttModel Key" + tModel + "\nDetails " + owner);
            }
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }
}
