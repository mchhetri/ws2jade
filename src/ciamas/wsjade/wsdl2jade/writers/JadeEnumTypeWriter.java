/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import org.apache.axis.utils.JavaUtils;
import org.apache.axis.wsdl.symbolTable.TypeEntry;
import org.apache.axis.wsdl.toJava.Emitter;
import org.apache.axis.wsdl.toJava.JavaClassWriter;
import org.apache.axis.wsdl.toJava.Namespaces;
import org.apache.axis.wsdl.toJava.Utils;

import ciamas.wsjade.wsdl2jade.mapping.TypeMapping;
import ciamas.wsjade.wsdl2jade.utils.BeanUtils;
import ciamas.wsjade.wsdl2jade.utils.Common;

public class JadeEnumTypeWriter extends JavaClassWriter {

	private Vector elements;

	private String baseType;

	private TypeEntry type;

	public JadeEnumTypeWriter(Emitter e, TypeEntry type, Vector v) {
		super(e, type.getName(), "enumType");

		//Im not going to mess up with the namespace
		this.namespaces = new Namespaces(Common.getJadeOutputDir());
		this.packageName = Common.getFullReflectPkg()
				+ Utils.getJavaPackageName(type.getName());
		this.className = Utils.getJavaLocalName(type.getName());
		this.elements = v;
		this.type = type;

	}

	protected String getImplementsText() {
		return "implements jade.content.Concept";
	}

	protected void writeHeaderComments(PrintWriter pw) throws IOException {
		Common.printBanner(pw);

	}

	public static Vector getEnumValueIds(Vector bv) {
		boolean validJava = true; // Assume all enum values are valid ids
		// Walk the values looking for invalid ids
		for (int i = 1; i < bv.size() && validJava; i++) {
			String value = (String) bv.get(i);
			if (!JavaUtils.isJavaId(value))
				validJava = false;
		}
		// Build the vector of ids
		Vector ids = new Vector();
		for (int i = 1; i < bv.size(); i++) {
			// If any enum values are not valid java, then
			// all of the ids are of the form value<1..N>.
			if (!validJava) {
				ids.add("value" + i);
			} else {
				ids.add((String) bv.get(i));
			}
		}
		return ids;
	}

	protected void writeFileBody(java.io.PrintWriter printWriter)
			throws java.io.IOException {
		// The first index is the base type.
		// The base type could be a non-object, if so get the corresponding
		// Class.
		baseType = ((TypeEntry) elements.get(0)).getName();
		String baseClass = baseType;

		if (baseType.indexOf("int") == 0) {
			baseClass = "java.lang.Integer";
		} else if (baseType.indexOf("char") == 0) {
			baseClass = "java.lang.Character";
		} else if (baseType.indexOf("short") == 0) {
			baseClass = "java.lang.Short";
		} else if (baseType.indexOf("long") == 0) {
			baseClass = "java.lang.Long";
		} else if (baseType.indexOf("double") == 0) {
			baseClass = "java.lang.Double";
		} else if (baseType.indexOf("float") == 0) {
			baseClass = "java.lang.Float";
		} else if (baseType.indexOf("byte") == 0) {
			baseClass = "java.lang.Byte";
		}
		baseClass = TypeMapping.getJadeDataType(baseClass, false);
		// Create a list of ids
		Vector ids = getEnumValueIds(elements);

		// Create a list of the literal values.
		Vector values = new Vector();
		int i = 0;
		for (i = 1; i < elements.size(); i++) {
			String value = (String) elements.get(i);
			if (baseClass.equals("java.lang.String")) {
				value = "\"" + value + "\""; // Surround literal with double
				// quotes
			} else if (baseClass.equals("java.lang.Character")) {
				value = "'" + value + "'";
			} else if (baseClass.equals("java.lang.Float")) {
				if (!value.endsWith("F") && // Indicate float literal so javac
						!value.endsWith("f")) // doesn't complain about
					// precision.
					value += "F";
			} else if (baseClass.equals("java.lang.Long")) {
				if (!value.endsWith("L") && // Indicate float literal so javac
						!value.endsWith("l")) // doesn't complain about
					// precision.
					value += "L";
			} else if (baseClass.equals(baseType)) {
				// Construct baseClass object with literal string
				value = "new " + baseClass + "(\"" + value + "\")";
			}
			values.add(value);
		}
		printWriter.println(className +"Facet facet=null;");
		printWriter.println(baseClass +" enumEle;");
		printWriter.println("   public " + className + "(){");
		printWriter.println("   }");

		printWriter
				.println("   public "+baseClass+" getEnumEle(){return enumEle;}");
		printWriter.println("   public void setEnumEle("+baseClass+" enumIn){");
		printWriter.println("           this.enumEle = enumIn;");
		printWriter.println("    }");

	

		registerDetails();
	}

	private void registerDetails() {
		GenCache cache = GenCache.getInstance();

		//Add into GenCache the mapping name
		cache.record(packageName + "." + className, baseType,
				GenCache.ENUM_MAPPING);

		//Register onto for later print out
		(new BeanUtils(baseType, baseType, "enumEle"))
				.register(packageName + "." + className);
		
		OntSchemaTemp.getInstance().registerFacet(packageName+"."+className, 
				"enumEle", packageName+"."+className+"Facet");
	}

}