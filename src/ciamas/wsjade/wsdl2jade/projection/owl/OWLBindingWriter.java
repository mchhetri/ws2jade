/*
 * Created on 3/02/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ciamas.wsjade.wsdl2jade.projection.owl;

import java.io.IOException;

import javax.wsdl.Binding;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.PortTypeEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import ciamas.wsjade.wsdl2jade.writers.OntParser;
/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OWLBindingWriter implements Generator{

	protected Generator interfaceWriter = null;

	protected OntParser ontParser;

	protected Binding binding;

	protected SymbolTable symbolTable;

	public static String INTERFACE_NAME = "interface name";

	public OWLBindingWriter(OntParser ontParser, Binding binding,
			SymbolTable symbolTable) {
		this.ontParser = ontParser;
		this.binding = binding;
		this.symbolTable = symbolTable;
	}

	protected Generator getOWLInterfaceWriter(OntParser ontParser,
			PortTypeEntry ptEntry, BindingEntry bEntry, SymbolTable st) {
		return new OWLInterfaceWriter(ontParser, ptEntry, bEntry, st);
	
	}

	public void generate() throws IOException {
		setGenerators();
		if (interfaceWriter != null) {
			interfaceWriter.generate();
		}

	}

	protected void setGenerators() {
		BindingEntry bEntry = symbolTable.getBindingEntry(binding.getQName());

		// Interface writer
		PortTypeEntry ptEntry = symbolTable.getPortTypeEntry(binding
				.getPortType().getQName());
		if (ptEntry.isReferenced()) {
			interfaceWriter = getOWLInterfaceWriter(ontParser, ptEntry,
					bEntry, symbolTable);
		}

	}
}
