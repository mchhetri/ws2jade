/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import java.io.IOException;
import java.util.Vector;

import javax.xml.namespace.QName;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.symbolTable.SchemaUtils;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.symbolTable.TypeEntry;
import org.apache.axis.wsdl.toJava.Emitter;
import org.apache.axis.wsdl.toJava.JavaGeneratorFactory;
import org.apache.axis.wsdl.toJava.JavaWriter;
import org.apache.axis.wsdl.toJava.Utils;

import org.apache.axis.wsdl.toJava.JavaBeanFaultWriter;
import org.w3c.dom.Node;
import java.util.Collections;

public class JadeTypeWriter implements Generator {

	private Generator typeWriter1 = null;
	private Generator typeWriter2 = null;

	public JadeTypeWriter(Emitter emitter, TypeEntry type,
			SymbolTable symbolTable) {

		if (type.isReferenced() && !type.isOnlyLiteralReferenced()) {
			Node node = type.getNode();

			if (!type.getName().endsWith("[]")) {

				// Generate the proper class for either "complex" or
				// "enumeration" types
				Vector v = Utils.getEnumerationBaseAndValues(node, symbolTable);
				if (v != null) {
					typeWriter1 = getEnumTypeWriter(emitter, type, v);
					typeWriter2 = getEnumTypeFacetWriter(emitter, type, v);
				} else {
					TypeEntry base = SchemaUtils
							.getComplexElementExtensionBase(node, symbolTable);
					if (base == null) {
						QName baseQName = SchemaUtils.getSimpleTypeBase(node);
						if (baseQName != null) {
							base = symbolTable.getType(baseQName);
						}
					}

					/*typeWriter1 = getBeanWriter(emitter, type,
							SchemaUtils.getContainedElementDeclarations(node,
									symbolTable), base, SchemaUtils
									.getContainedAttributeTypes(node,
											symbolTable));*/
					typeWriter1 = getBeanWriter(emitter, type, base);
				}
			}

		}
	}

	public void generate() throws IOException {
		if (typeWriter1 != null) 
			typeWriter1.generate();
		
		if(typeWriter2 !=null)
			typeWriter2.generate();

	} // generate

	protected JavaWriter getEnumTypeWriter(Emitter emitter, TypeEntry type,
			Vector v) {

		JadeEnumTypeWriter jadeWriter = new JadeEnumTypeWriter(emitter, type, v);
		

		return jadeWriter;
	}
	protected JavaWriter getEnumTypeFacetWriter(Emitter emitter, TypeEntry type,
			Vector v) {

		JadeEnumTypeFacetWriter jadeWriter = new JadeEnumTypeFacetWriter(emitter, type, v);
		

		return jadeWriter;
	}

	protected JavaWriter getBeanWriter(Emitter emitter, TypeEntry type, TypeEntry base) {

		 Vector elements = type.getContainedElements();
	     Vector attributes = type.getContainedAttributes();
	        
	       
	        Boolean isComplexFault = (Boolean) type.getDynamicVar(
	                JavaGeneratorFactory.COMPLEX_TYPE_FAULT);

	        if ((isComplexFault != null) && isComplexFault.booleanValue()) {

	           return null;
	        }

	        JadeBeanWriter jadeBeanWriter = new JadeBeanWriter(emitter, type,
					elements, base, attributes, getBeanHelperWriter(emitter, type,
							elements, base, attributes,false));
			return jadeBeanWriter;
	}
	//FIX ME
	 protected JadeBeanHelperWriter getBeanHelperWriter(
            Emitter emitter, TypeEntry type, Vector elements, TypeEntry base,
            Vector attributes, boolean forException) {
        return new JadeBeanHelperWriter(
                emitter, type, elements, base, attributes,
                forException  ?  JavaBeanFaultWriter.RESERVED_PROPERTY_NAMES
                              :  Collections.EMPTY_SET);
    }

}

