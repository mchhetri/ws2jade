/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import java.io.IOException;

import javax.wsdl.Binding;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.PortTypeEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;

public class JadeBindingWriter implements Generator {

	protected Generator interfaceWriter = null;

	protected OntParser ontParser;

	protected Binding binding;

	protected SymbolTable symbolTable;

	public static String INTERFACE_NAME = "interface name";

	public JadeBindingWriter(OntParser ontParser, Binding binding,
			SymbolTable symbolTable) {
		this.ontParser = ontParser;
		this.binding = binding;
		this.symbolTable = symbolTable;
	}

	protected Generator getJadeInterfaceWriter(OntParser ontParser,
			PortTypeEntry ptEntry, BindingEntry bEntry, SymbolTable st) {
		return new JadeInterfaceWriter(ontParser, ptEntry, bEntry, st);
	}

	public void generate() throws IOException {
		setGenerators();
		if (interfaceWriter != null) {
			interfaceWriter.generate();
		}

	}

	protected void setGenerators() {
		BindingEntry bEntry = symbolTable.getBindingEntry(binding.getQName());

		// Interface writer
		PortTypeEntry ptEntry = symbolTable.getPortTypeEntry(binding
				.getPortType().getQName());
		if (ptEntry.isReferenced()) {
			interfaceWriter = getJadeInterfaceWriter(ontParser, ptEntry,
					bEntry, symbolTable);
		}

	}
} // class JadeBindingWriter
