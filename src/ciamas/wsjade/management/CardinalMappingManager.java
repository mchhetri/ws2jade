/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.management;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import ciamas.wsjade.management.utils.WSTableInfoModel;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class CardinalMappingManager {

	private static CardinalMappingManager me = null;

	private static Hashtable records = new Hashtable();

	//DataListener
	private static WSTableInfoModel wsTableModel = null;

	public static void addRecord(String agentName, ASWSRecord record) {
		if (records.get(agentName) == null)
			records.put(agentName, new ArrayList());
		ArrayList agentRecords = (ArrayList) records.get(agentName);
		agentRecords.add(record);

		//Notify data model
		wsTableModel.addData(new AGWSRecord(agentName, record.getWsURL()));

	}

	public static void delRecord(String agentName) {
		records.remove(agentName);
		wsTableModel.removeData(agentName);
	}

	public static void delRecord(String agentName, String serviceName,
			String wsURL) {

		ArrayList agentRecords = (ArrayList) records.get(agentName);
		for (int i = 0; i < agentRecords.size(); i++) {
			String service = ((ASWSRecord) agentRecords.get(i))
					.getAgentService();
			if (service.equals(serviceName)) {
				agentRecords.remove(i);
				wsTableModel.removeData(new AGWSRecord(agentName, wsURL));
				return;
			}
		}
	}

	public static ArrayList getAGWSList() {
		ArrayList ret = new ArrayList();
		Enumeration e = records.keys();
		while (e.hasMoreElements()) {
			String agentName = (String) e.nextElement();
			//Wrong, please fix the following
			ASWSRecord as_ws = (ASWSRecord) records.get(agentName);
			AGWSRecord ag_ws = new AGWSRecord(agentName, as_ws.getWsURL());
			ret.add(ag_ws);
		}

		return ret;
	}
	public static Hashtable getRecord()
	{
		return records;
	}

	/**
	 * @param wsTableModel
	 *            The wsTableModel to set.
	 */
	public static void setWsTableModel(WSTableInfoModel wsTableModel) {
		CardinalMappingManager.wsTableModel = wsTableModel;
	}
}