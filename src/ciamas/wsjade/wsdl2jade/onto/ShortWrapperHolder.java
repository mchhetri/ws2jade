/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.wsdl2jade.onto;

/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class ShortWrapperHolder  implements jade.content.Concept{

	private Short value;
	
	/**
	 * @return Returns the value.
	 */
	public Short getValue() {
		return value;
	}
	/**
	 * @param value The value to set.
	 */
	public void setValue(Short value) {
		this.value = value;
	}
}
