/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.management;

import java.util.Hashtable;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class Status {

	public static int JAVA_STUB_GENERATION_PARAM = 1;

	public static int JAVA_ONTO_GENERATION_PARAM = 2;

	public static int JAR_STUB_GENERATION_PARAM = 3;

	public static int JAR_ONTO_GENERATION_PARAM = 4;

	private static Hashtable actionStatusTable = new Hashtable();

	public static void registerActionStatus(String url) {

		actionStatusTable.put(url, new ActionStatus());
	}

	public static ActionStatus getActionStatus(String url) {
		return (ActionStatus) actionStatusTable.get(url);
	}

	public static void removeActionStatus(String url) {
		actionStatusTable.remove(url);
	}

	/**
	 * @return void Set statusParam of action on url to value of val
	 */
	public static void setStatus(String url, int statusParam, boolean val) {
		ActionStatus a = (ActionStatus) actionStatusTable.get(url);
		if (statusParam == Status.JAR_ONTO_GENERATION_PARAM)
			a.setJarOntoGeneration(val);
		else if (statusParam == Status.JAR_STUB_GENERATION_PARAM)
			a.setJarStubGeneration(val);
		else if (statusParam == Status.JAVA_ONTO_GENERATION_PARAM)
			a.setJavaOntoGeneration(val);
		else if (statusParam == Status.JAVA_STUB_GENERATION_PARAM)
			a.setJavaStubGeneration(val);

	}

	public static boolean getStatus(String url, int statusParam) {
		ActionStatus a = (ActionStatus) actionStatusTable.get(url);
		if (statusParam == Status.JAR_ONTO_GENERATION_PARAM)
			return a.isJarOntoGeneration();
		else if (statusParam == Status.JAR_STUB_GENERATION_PARAM)
			return a.isJarStubGeneration();
		else if (statusParam == Status.JAVA_ONTO_GENERATION_PARAM)
			return a.isJavaOntoGeneration();
		else if (statusParam == Status.JAVA_STUB_GENERATION_PARAM)
			return a.isJavaStubGeneration();
		return true;
	}

}