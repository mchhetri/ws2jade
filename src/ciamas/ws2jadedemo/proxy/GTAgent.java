/*
 * GTAgent.java
 *
 * Created on 31 January 2005, 10:58
 */

package ciamas.ws2jadedemo.proxy;

import jade.content.*;
import jade.content.onto.*;
import jade.core.*;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import jade.util.leap.*;
import jade.content.onto.basic.*;
import jade.content.lang.*;
import jade.content.lang.sl.*;
import java.io.*;

/**
 * This agent communicates with GlobalTransports web services. It does so by coordinating with a
 * CIAMAS WS2Jade WSAG via an automatically generated ontology.
 * @author 403515
 */
//TODO: Add log4j
public class GTAgent extends Agent {
    
    private ContentManager manager;
    private Codec codec;
    private Ontology ontology;
    private Ontology commonOntology;
    
    /** Creates a new instance of GTAgent */
    public GTAgent() {
        System.out.println("Initialising Agent...");
        manager = (ContentManager)getContentManager();
        codec = new SLCodec();
        ontology = JadeGen.onto.GTService.WebOntology.getInstance();
        commonOntology = ciamas.ws2jadedemo.ontology.CommonOntology.getInstance();
    }
    
    public void setup() {
        System.out.println("Agent " + getAID().getLocalName() + " is ready.");
        // Register the ontology
        manager.registerLanguage(codec);
        manager.registerOntology(ontology);
        manager.registerOntology(commonOntology);
        addBehaviour(new HandleDetailsRequest(this));
        addBehaviour(new HandleDeliveryInform(this));
    }
    
    class HandleDeliveryInform extends CyclicBehaviour {
        
        Agent a;
        MessageTemplate deliveryInformTemplate = MessageTemplate.and(
            MessageTemplate.MatchPerformative(ACLMessage.INFORM),
            MessageTemplate.MatchSender(new AID("GlobalTransportsProvider", AID.ISLOCALNAME)));
        
        public HandleDeliveryInform(Agent a) {
            this.a = a;
        }
        
        public void action() {
            ACLMessage deliveryInform = a.receive(deliveryInformTemplate);
            if (deliveryInform != null) {
                ContentElement content = null;
                try {
                    content = manager.extractContent(deliveryInform);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                if (content instanceof jade.content.onto.basic.Result) {
                    jade.content.onto.basic.Result result = (jade.content.onto.basic.Result)content;
                    if (result.getValue() instanceof JadeGen.onto.GTService.GlobalTransports.Response.BookDeliveryRequest) {
                        System.out.println("<GOT CONFIRMATION OF DELIVERY BOOKING FROM " + deliveryInform.getSender().getLocalName());
                        JadeGen.onto.GTService.GlobalTransports.Response.BookDeliveryRequest bdrResponse =
                            (JadeGen.onto.GTService.GlobalTransports.Response.BookDeliveryRequest)result.getValue();
                        JadeGen.onto.reflect.GlobalTransportsNS.Delivery del =
                            bdrResponse.getBookDeliveryRequestReturn();
                        // Convert to ciamas.ws2jadedemo.ontology.Delivery
                        ciamas.ws2jadedemo.ontology.Delivery deliv = new ciamas.ws2jadedemo.ontology.Delivery();
                        deliv.setCountry(del.getCountry());
                        deliv.setDeliveryDate(del.getDeliveryDate());
                        deliv.setDescription(del.getDescription());
                        deliv.setEmailAddress(del.getEmailAddress());
                        deliv.setPassword(del.getPassword());
                        deliv.setPostcode(del.getPostcode());
                        deliv.setPreferredDeliveryTimePeriod(del.getPreferredDeliveryTimePeriod());
                        deliv.setState(del.getState());
                        deliv.setStreetName(del.getStreetName());
                        deliv.setStreetNumber(del.getStreetNumber());
                        deliv.setSuburb(del.getSuburb());
                        // Send to AmazonAgent to forward to ClientAgent
                        ACLMessage sendInform = new ACLMessage(ACLMessage.INFORM);
                        sendInform.setSender(getAID());
                        sendInform.addReceiver(new AID("AmazonAgent", false));
                        sendInform.setLanguage(codec.getName());
                        sendInform.setOntology(commonOntology.getName());
                        try {
                            manager.fillContent(sendInform, new Action(getAID(), deliv));
                        }
                        catch (Exception e) { 
                            //System.out.println(e);
                            //e.printStackTrace(); 
                        }
                        System.out.println("<FORWARDING RESULTS TO AmazonAgent>");
                        send(sendInform);
                    }
                }
            }
            else {
                block();
            }
        }
        
    }
    
    class HandleDetailsRequest extends CyclicBehaviour {
        
        Agent a;
        MessageTemplate deliveryDetailsTemplate = MessageTemplate.and(
            MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
            MessageTemplate.MatchSender(new AID("AmazonAgent", AID.ISLOCALNAME)));
        
        public HandleDetailsRequest(Agent a) {
            super(a);
            this.a = a;
        }
        
        public void action() {
            ACLMessage deliveryDetails = a.receive(deliveryDetailsTemplate);
            if (deliveryDetails != null) {
                System.out.println("<RECEIVED DELIVERY BOOKING REQUEST FROM AmazonAgent>");
                System.out.println("<FORWARDING TO GlobalTransportsProvider>");
                ContentElement content = null;
                try {
                    content = manager.extractContent(deliveryDetails);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                if (content instanceof jade.content.onto.basic.Action) {
                    Concept action = ((Action)content).getAction();
                    ciamas.ws2jadedemo.ontology.Delivery delivery = 
                        (ciamas.ws2jadedemo.ontology.Delivery)action;
                    JadeGen.onto.GTService.GlobalTransports.Request.BookDeliveryRequest bdr =
                        new JadeGen.onto.GTService.GlobalTransports.Request.BookDeliveryRequest();
                    JadeGen.onto.reflect.GlobalTransportsNS.Delivery del =
                        new JadeGen.onto.reflect.GlobalTransportsNS.Delivery();
                    del.setCountry(delivery.getCountry());
                    del.setDeliveryDate(delivery.getDeliveryDate());
                    del.setDescription(delivery.getDescription());
                    del.setEmailAddress(delivery.getEmailAddress());
                    del.setPassword(delivery.getPassword());
                    del.setPostcode(delivery.getPostcode());
                    del.setPreferredDeliveryTimePeriod(delivery.getPreferredDeliveryTimePeriod());
                    del.setState(delivery.getState());
                    del.setStreetName(delivery.getStreetName());
                    del.setStreetNumber(delivery.getStreetNumber());
                    del.setSuburb(delivery.getSuburb());
                    bdr.setD(del);
                    ACLMessage sendRequest = new ACLMessage(ACLMessage.REQUEST);
                    sendRequest.setSender(getAID());
                    sendRequest.addReceiver(new AID("GlobalTransportsProvider", false));
                    sendRequest.setLanguage(codec.getName());
                    sendRequest.setOntology(ontology.getName());
                    try {
                        manager.fillContent(sendRequest, new Action(getAID(), bdr));
                    }
                    catch (Exception e) { 
                        //System.out.println(e);
                        //e.printStackTrace(); 
                    }
                    send(sendRequest);
                }
            }
            else {
                block();
            }
        }
        
    }
    
    public void takeDown() {
        System.out.println(getLocalName() + " is now shutting down.");
    }
    
}
