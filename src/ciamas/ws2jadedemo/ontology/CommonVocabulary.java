/*
 * CommonVocabulary.java
 *
 * Created on 31 January 2005, 16:07
 */

package ciamas.ws2jadedemo.ontology;

/**
 *
 * @author  403515
 */
public interface CommonVocabulary {
    
    public final static String SEARCH = "SEARCH";
    public final static String SEARCH_KEYWORD = "keyword";
    public final static String SEARCH_MODE = "mode";
    
    public final static String SEARCHRESULT = "SEARCHRESULT";
    public final static String SEARCHRESULT_PRODUCTNAME = "productName";
    public final static String SEARCHRESULT_ASIN = "asin";
    public final static String SEARCHRESULT_LISTPRICE = "listPrice";
    public final static String SEARCHRESULT_OURPRICE = "ourPrice";
    public final static String SEARCHRESULT_AVAILABILITY = "availability";
    public final static String SEARCHRESULT_MANUFACTURER = "manufacturer";
    public final static String SEARCHRESULT_URL = "url";
    public final static String SEARCHRESULT_CRCOMMENT = "crComment";
    public final static String SEARCHRESULT_CRRATING = "crRating";
    public final static String SEARCHRESULT_CRSUMMARY = "crSummary";
    public final static String SEARCHRESULT_QUANTITY = "quantity";
    
    public final static String SEARCHRESULTS = "SEARCHRESULTS";
    public final static String SEARCHRESULTS_RESULTS = "results";
    
    public final static String CARTITEM = "CARTITEM";
    public final static String CARTITEM_ITEMID = "itemId";
    public final static String CARTITEM_QUANTITY = "quantity";
    public final static String CARTITEM_HASCART = "hasCart";
    public final static String CARTITEM_HMAC = "HMAC";
    public final static String CARTITEM_CARTID = "cartId";
    
    public final static String CART = "CART";
    public final static String CART_ITEMS = "items";
    public final static String CART_HMAC = "HMAC";
    public final static String CART_CARTID = "cartId";
    public final static String CART_PURCHASEURL = "purchaseUrl";
    
    public final static String DELIVERY = "DELIVERY";
    public final static String DELIVERY_EMAILADDRESS = "emailAddress";
    public final static String DELIVERY_PASSWORD = "password";
    public final static String DELIVERY_STREETNUMBER = "streetNumber";
    public final static String DELIVERY_STREETNAME = "streetName";
    public final static String DELIVERY_SUBURB = "suburb";
    public final static String DELIVERY_POSTCODE = "postcode";
    public final static String DELIVERY_STATE = "state";
    public final static String DELIVERY_COUNTRY = "country";
    public final static String DELIVERY_DELIVERYDATE = "deliveryDate";
    public final static String DELIVERY_PREFERREDDELIVERYTIMEPERIOD = "preferredDeliveryTimePeriod";
    public final static String DELIVERY_DESCRIPTION = "description";
    
    public final static String PAYMENT = "PAYMENT";
    public final static String PAYMENT_EMAILADDRESS = "emailAddress";
    public final static String PAYMENT_PASSWORD = "password";
    public final static String PAYMENT_RECIPIENTEMAILADDRESS = "recipientEmailAddress";
    public final static String PAYMENT_AMOUNT = "amount";
    public final static String PAYMENT_CURRENCY = "currency";
    public final static String PAYMENT_NOTE = "note";
    
}
