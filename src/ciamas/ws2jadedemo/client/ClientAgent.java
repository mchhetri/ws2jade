/*
 * ClientAgent.java
 *
 * Created on 27 January 2005, 10:34
 */

package ciamas.ws2jadedemo.client;

import jade.content.*;
import jade.content.onto.*;
import jade.core.*;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import jade.util.leap.*;
import jade.content.onto.basic.*;
import jade.content.lang.*;
import jade.content.lang.sl.*;
import java.io.*;
import jade.gui.GuiAgent;
import jade.gui.GuiEvent;
import jade.domain.FIPANames;
import jade.proto.SimpleAchieveREInitiator;

/**
 * This GuiAgent is used to Test the composite web service functionality
 * offered by the proxy agents in this WS2Jade demo application.
 * @author 403515
 */
public class ClientAgent extends GuiAgent {
    
    private final static long TIMEOUT = 20000; // 10 seconds

    private ContentManager manager;
    private Codec codec;
    private Ontology commonOntology;
    
    private ArrayList search_results;
    private ciamas.ws2jadedemo.ontology.Cart shopping_cart;
    private String cart_contents;
    // The total price of all cart items
    private double total;
    
    // These constants are used by the Gui to post Events to the Agent
    public static final int EXIT = 1000;
    public static final int SEARCH = 1001;
    public static final int CHECKOUT = 1002;
    public static final int ADD_TO_CART = 1003;
    public static final int SELECT = 1004;
    public static final int GOTOCHECKOUT = 1005;
    
    transient protected ciamas.ws2jadedemo.gui.DemoGUI gui;
       
    /** Creates a new instance of ClientAgent */
    public ClientAgent() {
        System.out.println("Initialising Agent...");
        manager = (ContentManager)getContentManager();
        codec = new SLCodec();
        commonOntology = ciamas.ws2jadedemo.ontology.CommonOntology.getInstance();
    }
    
    public void setup() {
        Object[] args = getArguments();
        java.util.Properties props = null;
        if (args != null && args.length == 2) {
            props = System.getProperties();
            props.put("http.proxyHost", args[0]);
            System.out.println("Proxy Host: " + args[0]);
            props.put("http.proxyPort", args[1]);
            System.out.println("Proxy Port " + args[1]);
        }
        System.out.println("Agent " + getAID().getLocalName() + " is ready.");
        // Register the ontology
        manager.registerLanguage(codec);
        manager.registerOntology(commonOntology);
        gui = new ciamas.ws2jadedemo.gui.DemoGUI(this, props);
        gui.setVisible(true);
    }
    
    public void takeDown() {
        System.out.println(getLocalName() + " is now shutting down.");
    }
    
    class AddToCartRequestInitiator extends SimpleAchieveREInitiator {
        
        protected AddToCartRequestInitiator(Agent a, ACLMessage msg) {
            super(a, msg);
        }
        
        protected void handleAgree(ACLMessage msg) {
            System.out.println("AddToCartRequest: Received AGREE.");
        }
        
        protected void handleRefuse(ACLMessage msg) {
            System.out.println("AddToCartRequest: Received REFUSE.");
            // Set the gui cursor to Default
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    gui.setCursorDefault();
                }
            });
        }
        
        protected void handleInform(ACLMessage msg) {
            System.out.println("AddToCartRequest: Received INFORM.");
            ContentElement content = null;
            try {
                content = manager.extractContent(msg);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            if (content instanceof jade.content.onto.basic.Action) {
                String cartContents = ""; // For updating the gui
                Concept action = ((Action)content).getAction();
                shopping_cart = (ciamas.ws2jadedemo.ontology.Cart)action;
                System.out.println("CART ID: " + shopping_cart.getCartId());
                System.out.println("CART HMAC: " + shopping_cart.getHMAC());
                System.out.println("CART PURCHASE URL: " + shopping_cart.getPurchaseUrl());
                System.out.println("ITEMS: ");
                cartContents = cartContents + "Items:\n\n";
                double subtotal = 0.0;
                ciamas.ws2jadedemo.ontology.SearchResults cart_items = shopping_cart.getItems();
                for (int i=0; i<cart_items.getResults().size(); i++) {
                    ciamas.ws2jadedemo.ontology.SearchResult cart_item = 
                        (ciamas.ws2jadedemo.ontology.SearchResult)cart_items.getResults().get(i);
                    System.out.print("Asin: " + cart_item.getAsin());
                    cartContents = cartContents + cart_item.getProductName() + "\n";
                    cartContents = cartContents + cart_item.getAsin();
                    System.out.print(" @ " + cart_item.getOurPrice());
                    cartContents = cartContents + "    @    " + cart_item.getOurPrice();
                    System.out.println(" x " + cart_item.getQuantity());
                    cartContents = cartContents + "    X    " + cart_item.getQuantity() + "\n\n";
                    subtotal += (Integer.parseInt(cart_item.getQuantity())) * (Double.parseDouble(cart_item.getOurPrice().substring(1)));
                }
                subtotal = subtotal * 100;
                subtotal = (Math.rint(subtotal)) / 100;
                total = subtotal;
                System.out.println("SUBTOTAL: $" + subtotal);
                cartContents = cartContents + "SUBTOTAL: $" + subtotal;
                cartContents = cartContents + "\n\nPurchase URL:\n" + shopping_cart.getPurchaseUrl();
                cart_contents = cartContents;
                // Display the gui shopping cart dialog
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                     public void run() {
                         // Enable checkout button on gui
                         gui.checkoutButton.setEnabled(true);
                         gui.showUpdateCartDiaglog(cart_contents, "Shopping Cart");
                         gui.setCursorDefault();
                     }
                });
            }
        }
        
        protected void handleNotUnderstood(ACLMessage msg){
            System.out.println("AddToCartRequest: Received NOT_UNDERSTOOD.");
            // Set the gui cursor to Default
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    gui.setCursorDefault();
                }
            });
        }
        
    }
    
    class SearchRequestInitiator extends SimpleAchieveREInitiator {
        
        protected SearchRequestInitiator(Agent a, ACLMessage msg){
            super(a, msg);
        }
        
        protected void handleFailure(ACLMessage msg) {
            System.out.println("SearchRequest: TIMEOUT.");
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    gui.showErrorDialog("Search failed! (TIMEOUT or NO RESULTS FOUND)", "Search failed");
                }
            });
        }
        
        protected void handleAgree(ACLMessage msg) {
            System.out.println("SearchRequest: Received AGREE.");
        }
        
        protected void handleRefuse(ACLMessage msg) {
            System.out.println("SearchRequest: Received REFUSE.");
            // Set the gui cursor to Default
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    gui.setCursorDefault();
                }
            });
        }
        
        protected void handleInform(ACLMessage msg) {
            // Received the results, now let's update our gui
            System.out.println("SearchRequest: Received INFORM.");
            ContentElement content = null;
            try {
                content = manager.extractContent(msg);
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            if (content instanceof jade.content.onto.basic.Action) {
                Concept action = ((Action)content).getAction();
                ciamas.ws2jadedemo.ontology.SearchResults results = 
                    (ciamas.ws2jadedemo.ontology.SearchResults)action;
                search_results = results.getResults();
                final String[] productNames = new String[search_results.size()];
                for (int i=0; i<search_results.size(); i++) {
                    // Update the gui
                    ciamas.ws2jadedemo.ontology.SearchResult res = 
                        (ciamas.ws2jadedemo.ontology.SearchResult)search_results.get(i);
                    productNames[i] = res.getProductName();
                }
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        gui.resultsList.setListData(productNames);
                        gui.setCursorDefault();
                    }
                });
            }
        }
    }
    
    class HandleDeliveryInformBehaviour extends CyclicBehaviour {
        
        Agent a;
        MessageTemplate deliveryDetailsTemplate = MessageTemplate.and(
            MessageTemplate.MatchPerformative(ACLMessage.INFORM),
            MessageTemplate.MatchConversationId("delivery"));
        
        public HandleDeliveryInformBehaviour(Agent a) {
            this.a = a;
        }
        
        public void action() {
            // Got the Delivery confirmation
            ACLMessage deliveryInform = a.receive(deliveryDetailsTemplate);
            if (deliveryInform != null) {
                ContentElement content = null;
                try {
                    content = manager.extractContent(deliveryInform);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                if (content instanceof jade.content.onto.basic.Action) {
                    Concept action = ((Action)content).getAction();
                    if (action instanceof ciamas.ws2jadedemo.ontology.Delivery) {
                        System.out.println("Received Delivery confirmation.");
                        ciamas.ws2jadedemo.ontology.Delivery cnd =
                            (ciamas.ws2jadedemo.ontology.Delivery)action;
                        String deliveryReport = "CONFIRMED DELIVERY:\n\n";
                        deliveryReport = deliveryReport + "Email Address: " +  cnd.getEmailAddress();
                        deliveryReport = deliveryReport + "\nDelivery Date: " + cnd.getDeliveryDate();
                        deliveryReport = deliveryReport + "\nDescription: " + cnd.getDescription();
                        deliveryReport = deliveryReport + "\nPreferred Delivery Time Period: " + cnd.getPreferredDeliveryTimePeriod();
                        deliveryReport = deliveryReport + "\nAddress:\n" + cnd.getStreetNumber();
                        deliveryReport = deliveryReport + " " + cnd.getStreetName();
                        deliveryReport = deliveryReport + "\n" + cnd.getSuburb();
                        deliveryReport = deliveryReport + ", " + cnd.getPostcode();
                        deliveryReport = deliveryReport + "\n" + cnd.getState();
                        deliveryReport = deliveryReport + "\n" + cnd.getCountry();
                        final String dr = deliveryReport;
                        // Update gui
                        javax.swing.SwingUtilities.invokeLater(new Runnable() {
                             public void run() {
                                 gui.showUpdateCartDiaglog(dr, "Delivery Confirmed");
                                 gui.setCursorDefault();
                             }
                        });
                    }
                }
            }
            else {
                block();
            }
        }
        
    }
    
    class PaymentRequestInitiator extends SimpleAchieveREInitiator {

        protected PaymentRequestInitiator(Agent a, ACLMessage msg){
            super(a, msg);
        }

        protected void handleAgree(ACLMessage msg) {
            System.out.println("PaymentRequest: Received AGREE.");
        }

        protected void handleRefuse(ACLMessage msg) {
            System.out.println("PaymentRequest: Received REFUSE.");
            // Set the gui cursor to Default
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    gui.setCursorDefault();
                }
            });
        }

        protected void handleInform(ACLMessage msg) {
            System.out.println("PaymentRequest: Received INFORM.");
            if (msg.getContent() != null) {
                final String receipt = msg.getContent(); // For updating the gui
                // Display the gui receipt dialog
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                     public void run() {
                         // Enable checkout button on gui
                         gui.checkoutButton.setEnabled(true);
                         gui.showUpdateCartDiaglog(receipt, "Receipt - Payment Finished");
                     }
                });
            }
            else {
                // Display the gui receipt dialog
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                     public void run() {
                         // Enable checkout button on gui
                         gui.checkoutButton.setEnabled(true);
                         gui.showUpdateCartDiaglog("The payment was not processed. You may" +
                         "\nneed to create a PayFriend account.\nIt is also possible that the Recipient" +
                         "\nEmail Address you provided does not belong to\na PayFriend member.", "Error! - Payment Failed");
                         gui.setCursorDefault();
                     }
                });
            }
        }

        protected void handleNotUnderstood(ACLMessage msg){
            System.out.println("PaymentRequest: Received NOT_UNDERSTOOD.");
            // Set the gui cursor to Default
            javax.swing.SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    gui.setCursorDefault();
                }
            });
        }
               
    }
       
    protected void onGuiEvent(jade.gui.GuiEvent guiEvent) {
        switch(guiEvent.getType()) {
            case EXIT:
                System.out.println("<EXIT GuiEvent>");
                gui.dispose();
                gui = null;
                doDelete();
                System.exit(0);
                break;
            
            case SEARCH:
                // Set the gui cursor to Wait
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        gui.setCursorWait();
                    }
                });
                System.out.println("<SEARCH GuiEvent>");
                ciamas.ws2jadedemo.ontology.Search search = new ciamas.ws2jadedemo.ontology.Search();
                search.setKeyword((String)guiEvent.getParameter(0));
                System.out.println("KEYWORD: " + (String)guiEvent.getParameter(0));
                search.setMode((String)guiEvent.getParameter(1));
                System.out.println("SEARCH MODE: " + (String)guiEvent.getParameter(1));
                ACLMessage msg = new ACLMessage(ACLMessage.REQUEST);
                msg.setSender(getAID());
                msg.addReceiver(new AID("AmazonAgent", false));
                msg.setLanguage(codec.getName());
                msg.setOntology(commonOntology.getName());
                msg.setPerformative(ACLMessage.REQUEST);
                msg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
                msg.setReplyByDate(new java.util.Date(System.currentTimeMillis() + TIMEOUT)); 

                try {
                    manager.fillContent(msg, new Action(getAID(), search));
                }
                catch (Exception e) { 
                    System.out.println(e);
                    e.printStackTrace(); 
                }
                addBehaviour(new SearchRequestInitiator(this, msg));
                break;
                
            case SELECT:
                // Set the gui cursor to Wait
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        gui.setCursorWait();
                    }
                });
                System.out.println("<SELECT GuiEvent>");
                for (int i=0; i<search_results.size(); i++) {
                    final ciamas.ws2jadedemo.ontology.SearchResult res = 
                        (ciamas.ws2jadedemo.ontology.SearchResult)search_results.get(i);
                     if (res.getProductName().equals(guiEvent.getParameter(0))) {
                         // Update gui to display details for selected product
                         javax.swing.SwingUtilities.invokeLater(new Runnable() {
                             public void run() {
                                 gui.titleField.setText(res.getProductName());
                                 gui.ourPriceField.setText(res.getOurPrice());
                                 gui.listPriceField.setText(res.getListPrice());
                                 gui.detailsEditorPane.setText(
                                    "<HTML><STRONG>PRODUCT NAME:</STRONG><BR>" + res.getProductName()
                                    + "<BR><STRONG><BR>ASIN:</STRONG><BR>" + res.getAsin()
                                    + "<BR><STRONG><BR>AVAILABILITY:</STRONG><BR>" + res.getAvailability()
                                    + "<BR><STRONG><BR>MANUFACTURER:</STRONG><BR>" + res.getManufacturer()
                                    + "<BR><STRONG><BR>CUSTOMER REVIEW COMMENT:</STRONG><BR>" + res.getCrComment()
                                    + "<BR><STRONG><BR>CUSTOMER REVIEW SUMMARY:</STRONG><BR>" + res.getCrSummary()
                                    + "<BR><STRONG><BR>CUSTOMER REVIEW RATING:</STRONG><BR>" + res.getCrRating()
                                    + "</HTML>"
                                 );
                                 try {
                                     gui.imagePane.setText("<HTML><IMG SRC=\"" + res.getUrl() + "\"></IMG></HTML>");
                                 }
                                 catch (Exception e) {
                                     e.printStackTrace();
                                 }
                                 gui.titleField.setCaretPosition(0);
                                 gui.detailsEditorPane.setCaretPosition(0);
                                 // Enable add to cart button
                                 gui.addToCartButton.setEnabled(true);
                                 // Enable quantityField
                                 gui.quantityField.setEnabled(true);
                             }
                         });
                         javax.swing.SwingUtilities.invokeLater(new Runnable() {
                             public void run() {
                                 gui.setCursorDefault();
                             }
                         });
                         break;
                     }
                }
                break;
            
            case CHECKOUT:
                // Set the gui cursor to Wait
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        gui.setCursorWait();
                    }
                });
                System.out.println("<CHECKOUT GuiEvent>");
                System.out.println("TOTAL: $" + total);
                ciamas.ws2jadedemo.ontology.Payment payment = 
                    (ciamas.ws2jadedemo.ontology.Payment)guiEvent.getParameter(0);
                ciamas.ws2jadedemo.ontology.Delivery delivery = 
                    (ciamas.ws2jadedemo.ontology.Delivery)guiEvent.getParameter(1);
                // Send shipping details to AmazonAgent
                ACLMessage deliverymsg = new ACLMessage(ACLMessage.REQUEST);
                deliverymsg.setSender(getAID());
                deliverymsg.addReceiver(new AID("AmazonAgent", false));
                deliverymsg.setLanguage(codec.getName());
                deliverymsg.setOntology(commonOntology.getName());
                deliverymsg.setPerformative(ACLMessage.INFORM);
                try {
                    manager.fillContent(deliverymsg, new Action(getAID(), delivery));
                }
                catch (Exception e) { 
                    System.out.println(e);
                    e.printStackTrace(); 
                }
                send(deliverymsg);
                addBehaviour(new HandleDeliveryInformBehaviour(this));
                // Send payment request to PayFriendAgent
                ACLMessage checkoutmsg = new ACLMessage(ACLMessage.REQUEST);
                checkoutmsg.setSender(getAID());
                checkoutmsg.addReceiver(new AID("PayFriendAgent", false));
                checkoutmsg.setLanguage(codec.getName());
                checkoutmsg.setOntology(commonOntology.getName());
                checkoutmsg.setPerformative(ACLMessage.REQUEST);
                checkoutmsg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
                try {
                    manager.fillContent(checkoutmsg, new Action(getAID(), payment));
                }
                catch (Exception e) { 
                    System.out.println(e);
                    e.printStackTrace(); 
                }
                addBehaviour(new PaymentRequestInitiator(this, checkoutmsg));
                break;
                
            case GOTOCHECKOUT:
                System.out.println("<GOTOCHECKOUT GuiEvent>");
                 ciamas.ws2jadedemo.gui.CheckoutDialog cd = 
                    new ciamas.ws2jadedemo.gui.CheckoutDialog(gui, false, this);
                 cd.setAmount("" + total);
                 cd.show();
                 break;
            
            case ADD_TO_CART:
                // Set the gui cursor to Wait
                javax.swing.SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        gui.setCursorWait();
                    }
                });
                System.out.println("<ADD_TO_CART GuiEvent>");
                ACLMessage addtocartmsg = new ACLMessage(ACLMessage.REQUEST);
                addtocartmsg.setSender(getAID());
                addtocartmsg.addReceiver(new AID("AmazonAgent", false));
                addtocartmsg.setLanguage(codec.getName());
                addtocartmsg.setOntology(commonOntology.getName());
                addtocartmsg.setPerformative(ACLMessage.REQUEST);
                addtocartmsg.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
                ciamas.ws2jadedemo.ontology.CartItem cartItem = new ciamas.ws2jadedemo.ontology.CartItem();
                for (int i=0; i<search_results.size(); i++) {
                    ciamas.ws2jadedemo.ontology.SearchResult res = 
                        (ciamas.ws2jadedemo.ontology.SearchResult)search_results.get(i);
                    if (res.getProductName().equals((String)guiEvent.getParameter(0))) {
                        cartItem.setItemId(res.getAsin());
                        break;
                    }
                }
                cartItem.setQuantity((String)guiEvent.getParameter(1));
                if (shopping_cart == null) {
                    cartItem.setHasCart(false);
                }
                else {
                    cartItem.setHasCart(true);
                    cartItem.setHMAC(shopping_cart.getHMAC());
                    cartItem.setCartId(shopping_cart.getCartId());
                    System.out.println("Adding to cart...\nCART ID: " 
                        + shopping_cart.getCartId() + " CART HMAC: " + shopping_cart.getHMAC());
                }
                System.out.print("ADDING ITEM: " + (String)guiEvent.getParameter(0)+ " ");
                System.out.println("X " + guiEvent.getParameter(1));
                try {
                    manager.fillContent(addtocartmsg, new Action(getAID(), cartItem));
                }
                catch (Exception e) { 
                    System.out.println(e);
                    e.printStackTrace(); 
                }
                addBehaviour(new AddToCartRequestInitiator(this, addtocartmsg));
                break;
        }
    }
    
}
