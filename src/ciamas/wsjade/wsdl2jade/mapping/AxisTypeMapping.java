/*
 * Created on 20/02/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ciamas.wsjade.wsdl2jade.mapping;

/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

import org.apache.log4j.Logger;

public class AxisTypeMapping {

	//Date to Calendar
	protected static java.util.GregorianCalendar convert(java.util.Date d)
	{
		return (java.util.GregorianCalendar)JavaTypeConverter.convert(d, "java.util.GregorianCalendar");
	}
	//Calendar to Date
	protected  static java.util.Date convert(java.util.Calendar d)
	{
		return (java.util.Date)JavaTypeConverter.convert((java.util.GregorianCalendar)d, "java.util.Date");
	}
	//Ciamas Date to Axis Date
	protected static  org.apache.axis.types.Day convert(ciamas.wsjade.wsdl2jade.onto.Day d)
	{
		return (org.apache.axis.types.Day)JavaTypeConverter.convert(d, "org.apache.axis.types.Day");
	}
	//Axis Date to Ciamas Date
	protected  static ciamas.wsjade.wsdl2jade.onto.Day convert(org.apache.axis.types.Day d)
	{
		return (ciamas.wsjade.wsdl2jade.onto.Day)JavaTypeConverter.convert(d, "ciamas.wsjade.wsdl2jade.onto.Day");
	}
	//Ciamas Month to Axis Month
	protected  static org.apache.axis.types.Month convert(ciamas.wsjade.wsdl2jade.onto.Month m)
	{
		return (org.apache.axis.types.Month)JavaTypeConverter.convert(m,"org.apache.axis.types.Month");
	}
	//Axis Month to Ciamas Month
	protected  static ciamas.wsjade.wsdl2jade.onto.Month convert(org.apache.axis.types.Month m)
	{
		return (ciamas.wsjade.wsdl2jade.onto.Month)JavaTypeConverter.convert(m,"ciamas.wsjade.wsdl2jade.onto.Month");
	}
	//Ciamas MonthDay to Axis MonthDay
	protected  static org.apache.axis.types.MonthDay convert(ciamas.wsjade.wsdl2jade.onto.MonthDay m)
	{
		return (org.apache.axis.types.MonthDay)JavaTypeConverter.convert(m,"org.apache.axis.types.MonthDay");
	}
	//AxisMonthDay to Ciamas MonthDay
	protected  static ciamas.wsjade.wsdl2jade.onto.MonthDay convert(org.apache.axis.types.MonthDay m)
	{
		return (ciamas.wsjade.wsdl2jade.onto.MonthDay)JavaTypeConverter.convert(m,"ciamas.wsjade.wsdl2jade.onto.MonthDay");
	}
	//Ciamas Duration to Axis Duration
	protected  static org.apache.axis.types.Duration convert(ciamas.wsjade.wsdl2jade.onto.Duration d)
	{
		return (org.apache.axis.types.Duration)JavaTypeConverter.convert(d,"org.apache.axis.types.Duration");
		
	}
	//Axis Duration to Ciamas Duration
	protected  static ciamas.wsjade.wsdl2jade.onto.Duration convert(org.apache.axis.types.Duration d)
	{
		return (ciamas.wsjade.wsdl2jade.onto.Duration)JavaTypeConverter.convert(d,"ciamas.wsjade.wsdl2jade.onto.Duration");
		
	}
	
	//Unknown types
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(java.lang.Object d)
	{
		Logger.getLogger("WS2JADE").error("Object type has not mapping");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(java.util.HashMap d)
	{
		Logger.getLogger("WS2JADE").error("HashMap type has no mapping implemented");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(org.w3c.dom.Element d)
	{
		Logger.getLogger("WS2JADE").error("Element type has no mapping implemented");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(org.apache.axis.types.HexBinary d)
	{
		Logger.getLogger("WS2JADE").error("HexBinary type has no mapping implemented");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(org.apache.axis.types.Id d)
	{
		Logger.getLogger("WS2JADE").error("Id type has no mapping implemented");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(org.apache.axis.types.IDRef d)
	{
		Logger.getLogger("WS2JADE").error("IDRef type has no mapping implemented");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(org.apache.axis.types.IDRefs d)
	{
		Logger.getLogger("WS2JADE").error("IDRefs type has no mapping implemented");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(org.apache.axis.types.Language d)
	{
		Logger.getLogger("WS2JADE").error("org.apache.axis.types.Language type has no mapping implemented");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(org.apache.axis.types.NCName d)
	{
		Logger.getLogger("WS2JADE").error("NCName type has no mapping implemented");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(org.apache.axis.types.NMToken d)
	{
		Logger.getLogger("WS2JADE").error("NMToken type has no mapping implemented");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	protected  static ciamas.wsjade.wsdl2jade.onto.Unknown convert(org.apache.axis.message.MessageElement d)
	{
		Logger.getLogger("WS2JADE").error("Message Element type has no mapping implemented");
		return new ciamas.wsjade.wsdl2jade.onto.Unknown();
	}
	protected  static Object convert(ciamas.wsjade.wsdl2jade.onto.Unknown d)
	{
		Logger.getLogger("WS2JADE").error("No mapping encounters");
		return null;
	}
}
