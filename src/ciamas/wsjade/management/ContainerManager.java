/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.management;

import jade.BootProfileImpl;
import jade.core.Runtime;
import jade.wrapper.AgentController;
import jade.wrapper.AgentContainer;

import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Enumeration;

import java.util.Vector;
import org.apache.log4j.Logger;



/**
 * 
 * @author xnguyen
 */
public class ContainerManager {

	private static ContainerManager me = null;

	private AgentContainer container = null;

	private static Vector mapping = new Vector();
	private Logger log = Logger.getLogger("WS2JADE");

	/** Creates a new instance of ContainerManager */
	private ContainerManager() {
	}

	public static ContainerManager getInstance() {
		if (me == null) {
			me = new ContainerManager();
		}

		return me;
	}

	/** Create a new agent container to host WSAG */
	public boolean createAgentContainer(String[] args)
	{
		boolean successful = false;
		try {
			// create agent container
			Runtime rt = Runtime.instance();

			BootProfileImpl p = new BootProfileImpl(args);
				container = rt.createMainContainer(p);
		
			successful = true;
		} catch (Exception e) {
			log.warn("Fail to creat container");
			log.debug("Exception occurred", e);
			container = null;
		}

		return successful;
	}
	
	private synchronized void saveAgents()
	{
		log.info("Save deployment setting");
		  try {

		      FileOutputStream fout =  new FileOutputStream(".wsag");
		      PrintStream myOutput = new PrintStream(fout);
		      java.util.Hashtable records = CardinalMappingManager.getRecord();
		      Enumeration e = records.keys();
				while (e.hasMoreElements()) {
					String agentName = (String) e.nextElement();
					java.util.ArrayList list = (java.util.ArrayList)records.get(agentName);
					for(int i=0;i< list.size();i++)
					{
						ASWSRecord as_ws = (ASWSRecord) list.get(i);
						String serviceName = as_ws.getAgentService();
						String wsURI = as_ws.getWsURL();
						AGWSRecord ag_ws = new AGWSRecord(agentName, as_ws.getWsURL());
						myOutput.println(agentName + "|" + serviceName +"|" + wsURI);
					}
				}
				fout.close();
				
		  }
		  catch(Exception e){log.debug("Exception occurred", e);}

		
	}
	
	
	public boolean createAgentContainer(String platformName, String host, int port,
			boolean isMainContainer) {
		boolean successful = false;
		if (container != null)
			return true;

		String[] agent_containerArgs = { "host:" + host, "-container", "-gui" };
		//String[]
		// main_containerArgs={"mtp:jade.mtp.http.MessageTransportProtocol",
		// "-gui"};
		String[] main_containerArgs = { "-gui", "name:"+platformName };
		BootProfileImpl p = null;

		try {
			// create agent container
			Runtime rt = Runtime.instance();

			if (isMainContainer) {
				p = new BootProfileImpl(main_containerArgs);
				container = rt.createMainContainer(p);
			} else {
				p = new BootProfileImpl(agent_containerArgs);
				container = rt.createAgentContainer(p);
			}
		
			successful = true;
		} catch (Exception e) {
			container = null;
			log.warn("Fail to start the container" + " on " + host
			 + " port " + port + '.');
			successful = false;
		}
		
		return successful;

	}

	public boolean shutdownContainer() {
		boolean successful = false;
		saveAgents();
		if (container == null)
			return true;

		try {
			
			container.kill();
			successful = true;
		} catch (Exception e) {
			//Debug
			//e.printStackTrace();
			log.warn("Fail to shutdown container properly");
			log.debug("Exception occurred", e);
		}
		return successful;
	}

	/** create a new WSAG */
	public AgentController createNewAgent(java.lang.String nickname,
			java.lang.String className, java.lang.Object[] args) {
		AgentController agent = null;
		try {
			agent = container.createNewAgent(nickname, className, args);
		} catch (Exception e) {
			log.info("Fail to creat new agent");
			log.debug("Exception occurred", e);
			//e.printStackTrace();
		}

		return agent;

	}

	public AgentController registerAgent(ciamas.wsjade.wsdl2jade.utils.WSAgent agent,
			String nickName) {
		
		if (container == null) {
			log.info("Agent container is not created.");
			return null;
		}

		AgentController agentController = null;
		try {
			agentController = container.acceptNewAgent(nickName, agent);
			if (agentController == null) {
				log.info("Fail to register new agent");
			}
			agentController.start();
		} catch (Exception e) {
			//e.printStackTrace();
			log.debug("Exception occurred", e);
		}

		return (AgentController) agentController;

	}
	public AgentController registerManagerAgent()
	{
		if (container == null) {
			log.info("Agent container is not created.");
			return null;
		}

		AgentController agentController = null;
		try {
			agentController = container.acceptNewAgent("WSManager", new ciamas.wsjade.management.AgentManager());
			if (agentController == null) {
				log.info("Fail to register new agent");
			}
			agentController.start();
		} catch (Exception e) {
			//e.printStackTrace();
			log.debug("Exception occurred", e);
		}

		return (AgentController) agentController;
	}

	public void removeAgent(String nickname) {
		try {
			AgentController agentController = container.getAgent(nickname);
			agentController.kill();
		} catch (Exception e) {
			log.info("Fail to remove agent " + nickname);
			log.debug("Exception occurred", e);
		}
	}

	public boolean putObject(String agentName, Object obj) {
		boolean success = true;
		try {
			AgentController foundAgent = (AgentController) container.getAgent(agentName);
			foundAgent.putO2AObject(obj, AgentController.ASYNC);
			
		} catch (Exception e) {
			//e.printStackTrace();
			log.debug("Exception occurred", e);
			log.warn("Fail to send agent object");
			success = false;
		}
		return success;
	}
	

}