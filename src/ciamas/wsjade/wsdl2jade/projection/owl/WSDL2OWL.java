/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.wsdl2jade.projection.owl;

import java.util.List;

import org.apache.axis.utils.CLArgsParser;
import org.apache.axis.utils.CLOption;
import org.apache.axis.utils.Messages;
import org.apache.axis.wsdl.gen.Parser;
import org.apache.axis.wsdl.gen.WSDL2;
import org.apache.log4j.Logger;



/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class WSDL2OWL extends WSDL2 {

	protected Parser createParser() {
		return new OWLParser();

	}

	public void run(String[] args) {
		
		// Parse the arguments
		CLArgsParser argsParser = new CLArgsParser(args, options);

		// Print parser errors, if any
		if (null != argsParser.getErrorString()) {
			System.err.println(Messages.getMessage("error01", argsParser
					.getErrorString()));
			printUsage();
		}

		// Get a list of parsed options
		List clOptions = argsParser.getArguments();
		int size = clOptions.size();

		try {
			// Parse the options and configure the emitter as appropriate.
			for (int i = 0; i < size; i++) {
				parseOption((CLOption) clOptions.get(i));
			}

			// validate argument combinations
			//
			validateOptions();

			parser.run(wsdlURI);

		} catch (Throwable t) {
			Logger.getLogger("WS2JADE").debug("Exception occurred", t);
			//t.printStackTrace();

		}
	} // run

	public static void main(String args[]) {
		WSDL2OWL wsdl2OWL = new WSDL2OWL();

		wsdl2OWL.run(args);

		System.out.println(Temp.getInstance().getText());
	}

}