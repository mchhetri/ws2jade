/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import jade.content.schema.ObjectSchema;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Vector;

import org.apache.log4j.Logger;

import ciamas.wsjade.wsdl2jade.mapping.TypeMapping;
import ciamas.wsjade.wsdl2jade.utils.Common;


public class OntSchemaTemp {

	private static OntSchemaTemp me = null;

	private Vector vocabularies = new Vector();

	private Vector ontConstructs = new Vector();
	
	private Vector facets = new Vector();

	private OntSchemaTemp() {
	}

	public void clear() {
		vocabularies.clear();
		ontConstructs.clear();
		facets.clear();
	}

	public static OntSchemaTemp getInstance() {
		if (me == null)
			me = new OntSchemaTemp();
		return me;
	}

	public void registerFacet(String baseCls, String field,String facetCls)
	{
		
		for(int i = 0;i< facets.size();i++)
		{
			Restriction re = (Restriction)facets.elementAt(i);
			if(re.getBaseCls().equals(baseCls)&& re.getField().equals(field) &&
					re.getFacetCls().equals(facetCls))
				return;
		}
		facets.add(new Restriction(baseCls, field, facetCls));
		
		
	}
	/* Register a new construct */
	//clName: Name of the Vocabulary (composite)
	//partName: Name of the referred part in this vocabulary
	//partType: The referred part jax-rpc class
	//partClass: The referred part jade class
	public void register(String partName, String partType, String partClass,
			String clName) {
		//Note that we need both partType and partClass here as some case
		// partType is List

		clName = clName.replace(File.separatorChar, '.');

		//Each vocabulary has only one part with certain name
		if (hasOntConstruct(partName, clName))
			return;

		//Vocabulary is made basing on its class name
		String eleTerm = makeTerm(clName);
		String eleJadeType = TypeMapping.getJadeOntology(clName);
		String eleOntSchema = TypeMapping.getJadeSchemaType(eleJadeType);

		Vocabulary ele = new Vocabulary(eleTerm, eleJadeType, eleOntSchema);
		//Check if a new vocabulary
		if (!hasVocabulary(clName))
			vocabularies.addElement(ele);

		String partTerm = makeTerm(partClass);
		String partJadeType = TypeMapping.getJadeOntology(partClass);
		String partOntSchema = TypeMapping.getJadeSchemaType(partJadeType);
		Vocabulary part = new Vocabulary(partTerm, partJadeType, partOntSchema);

		OntConstruct ontConstruct = new OntConstruct(ele, part);
		ontConstruct.setPartName(partName);
		//Check if a List had been substitued for Jade Type
		if (partType.equals("jade.util.leap.ArrayList"))
			ontConstruct.setRelation(0, ObjectSchema.UNLIMITED, null);
		else
			ontConstruct.setRelation(0, 0, null);

		ontConstructs.addElement(ontConstruct);

	}

	public boolean hasVocabulary(String clName) {
		for (int i = 0; i < vocabularies.size(); i++) {
			Vocabulary vv = (Vocabulary) vocabularies.elementAt(i);
			if (vv.getTermClass().equals(clName))
				return true;

		}
		return false;
	}

	/* Check if a construct has been added */
	private boolean hasOntConstruct(String partName, String clName) {
		for (int i = 0; i < ontConstructs.size(); i++) {
			OntConstruct ontConstruct = (OntConstruct) ontConstructs
					.elementAt(i);
			String pName = ontConstruct.getPartVocabulary().getTerm();
			String cName = ontConstruct.getEleVocabulary().getTermClass();
			if (partName.equals(pName) && clName.equals(cName))
				return true;
		}

		return false;
	}

	//Make up a vocabulary term for a class
	public String makeTerm(String clName) {
		String term = clName;
		if(TypeMapping.isUnknown(clName))
			return "\"Unknow Concept\"";
		if(TypeMapping.isAxisPrimitive(clName))
		{
			int startPos = clName.lastIndexOf('.');
			return "Axis " + clName.substring(startPos+1);
		}
		//Get rid of the long package name of Action/Concept
		String servicePck = Common.getJadeGenRootPkg() + ".onto."
				+ Common.getServiceName() + ".";
		int index = clName.indexOf(servicePck);
		if (index == 0)
			term = clName.substring(servicePck.length());

		String reflectPck = Common.getJadeGenRootPkg() + ".onto.reflect.";
		index = term.indexOf(reflectPck);
		if (index == 0)
			term = term.substring(reflectPck.length());

		
		term = term.toUpperCase();
		term = term.replace('.', '_');

		return term;

	}

	public void sort() {
		//Sort the parts Vector before printing out
	}

	public void generate() {
		printVocFile();
		printOntFile();

	}

	private void printVocFile() {
		try {

			String pck = Common.getJadeGenRootPkg() + ".onto."
					+ Common.getServiceName();
			String pckDir = pck.replace('.', File.separatorChar);
			String outputDir = Common.getJadeOutputDir() + File.separatorChar
					+ pckDir;

			File file = new File(outputDir + File.separatorChar
					+ "WebVocabulary.java");
			File parent = new File(file.getParent());
			if (!parent.exists())
				parent.mkdirs();

			PrintWriter out = new PrintWriter(new FileWriter(file));

			//Print the header
			Common.printBanner(out);
			out.println();
			out.println("package " + pck + ";");
			out.println();
			out.println();
			out.println("/**");
			out
					.println("* Vocabulary containing constants used by the WebOntology.");
			out.println("* @author Xuan Thang Nguyen");
			out.println("*/");

			out.println("public interface WebVocabulary {");

			out.println();
			out.println();

			for (int i = 0; i < vocabularies.size(); i++) {
				Vocabulary vv = (Vocabulary) vocabularies.elementAt(i);
				String eleSchema = vv.getSchemaType();
				String eleClass = vv.getTermClass();
				String term = vv.getTerm();

				String vol = eleClass.replace('.', '_');

				out.println("public static final String " + term + " = \""
						+ vol + "\";");
			}

			//Print the footer
			out.println("}");

			out.close();
		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
		}

	}

	private void printOntFile() {
		try {

			String pck = Common.getJadeGenRootPkg() + ".onto."
					+ Common.getServiceName();
			String pckDir = pck.replace('.', File.separatorChar);
			String outputDir = Common.getJadeOutputDir() + File.separatorChar
					+ pckDir;

			File file = new File(outputDir + File.separatorChar
					+ "WebOntology.java");
			File parent = new File(file.getParent());
			if (!parent.exists())
				parent.mkdirs();

			PrintWriter out = new PrintWriter(new FileWriter(file));

			//Print the header
			Common.printBanner(out);
			out.println();
			out.println("package " + pck + ";");
			out.println();
			out.println();
			out.println("import jade.content.onto.*;");
			out.println("import jade.content.schema.*;");
			out.println();
			out.println();

			out.println("public class WebOntology extends Ontology implements "
					+ pck + ".WebVocabulary{");

			out.println("\tpublic static final String ONTOLOGY_NAME = \"" + pck
					+ "-ontology" + "\";");
			out.println();

			out.println("\t// The singleton instance of this ontology");
			out
					.println("\tprivate static Ontology theInstance = new WebOntology(BasicOntology.getInstance());");
			out.println();

			out.println("\tpublic static Ontology getInstance() {");
			out.println("\t\treturn theInstance;");
			out.println("\t}");

			out.println();
			out.println("\t/*** Constructor ***/");
			out.println("\tprivate WebOntology(Ontology base) {");
			out.println("\t\tsuper(ONTOLOGY_NAME, base);");

			out.println("\t\ttry {");
			out.println();

			//Print the body
			printParts(out);

			//Print the footer
			out.println("\t\t}");
			out.println("\t\tcatch (OntologyException oe) {");
			out.println("\t\t\toe.printStackTrace();");
			out.println("\t\t}");
			out.println("\t}");
			out.println("}");

			out.close();
		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
		}

	}

	
	private void printParts(PrintWriter out) {

		for (int i = 0; i < vocabularies.size(); i++) {
			Vocabulary vv = (Vocabulary) vocabularies.elementAt(i);
			String eleSchema = vv.getSchemaType();
			String eleClass = vv.getTermClass();
			String term = vv.getTerm();

			out.println("\t\t\tadd(" + "new " + eleSchema + "(" + term + "),");
			out.println("\t\t\t\t\t" + eleClass + ".class);");

		}

		for (int i = 0; i < vocabularies.size(); i++) {
			Vocabulary vv = (Vocabulary) vocabularies.elementAt(i);
			String eleSchema = vv.getSchemaType();
			String eleClass = vv.getTermClass();
			String term = vv.getTerm();

			String shortEleClass = eleClass
					.substring(eleClass.lastIndexOf('.') + 1);

			String s = "\t\t\t" + eleSchema + " ele" + i + " = (" + eleSchema
					+ ") " + "getSchema(" + term + ");";
			out.println();

			out.println(s);
			printItem(term, out, i);
			printFacet("ele" + i,eleClass, out );

		}

	}
	
	public void printFacet(String eleName, String eleCls,PrintWriter out)
	{
		for(int i=0;i< facets.size();i++)
		{
			Restriction r = (Restriction)facets.get(i);
			String eName = r.getBaseCls();
			if(eName.equals(eleCls))
			{
				String field = r.getField();
				String facetCls = r.getFacetCls();
				out.println("\t\t\t"+eleName+".addFacet(\"" + field +
							"\", new "+ facetCls +"());");
				
			}
			
		}
	}

	public void printItem(String eleTerm, PrintWriter out, int num) {
		for (int i = 0; i < ontConstructs.size(); i++) {
			OntConstruct construct = (OntConstruct) ontConstructs.elementAt(i);
			String eleSchema = construct.getEleVocabulary().getSchemaType();
			String eleClass = construct.getEleVocabulary().getTermClass();
			String term = construct.getEleVocabulary().getTerm();
			if (!term.equals(eleTerm))
				continue;

			String partSchema = construct.getPartVocabulary().getSchemaType();
			String partClass = construct.getPartVocabulary().getTermClass();

			int minCard = construct.getCardMin();
			int maxCard = construct.getCardMax();
			String partName = construct.getPartName();

			//If partClass is still java type, then we have to use its
			// vocabulary
			if (TypeMapping.isCompositeOnto(partClass))
				partClass = construct.getPartVocabulary().getTerm();

			String shortEleClass = eleClass
					.substring(eleClass.lastIndexOf('.') + 1);
			//String shortPartClass =
			// partClass.replaceFirst("jade.content.onto.", "");
			
			String s = "\t\t\tele" + num + ".add(\"" + partName + "\", ("
					+ partSchema + ") getSchema(" + partClass + ")";

			if (maxCard != 0)
				s = s + ",0, ObjectSchema.UNLIMITED";
			else
				s=s+", ObjectSchema.OPTIONAL";

			s = s + ");";

			out.println(s);

		}

	}

	//This class represents the Facet
	public class Restriction{
		private String baseCls;
		private String field;
		private String facetCls;
		
		public Restriction(String b, String fl, String f)
		{
			baseCls =b;
			field =fl;
			facetCls =f;
		}
		
		
		/**
		 * @return Returns the baseCls.
		 */
		public String getBaseCls() {
			return baseCls;
		}
		/**
		 * @param baseCls The baseCls to set.
		 */
		public void setBaseCls(String baseCls) {
			this.baseCls = baseCls;
		}
		/**
		 * @return Returns the facetCls.
		 */
		public String getFacetCls() {
			return facetCls;
		}
		/**
		 * @param facetCls The facetCls to set.
		 */
		public void setFacetCls(String facetCls) {
			this.facetCls = facetCls;
		}
		/**
		 * @return Returns the field.
		 */
		public String getField() {
			return field;
		}
		/**
		 * @param field The field to set.
		 */
		public void setField(String field) {
			this.field = field;
		}
	}
	//This class represents a Vocabulary in JADE onto language
	public class Vocabulary {

		private String term; //The vocabulary name itself

		private String termClass; //Java class represents this term

		private String schemaType; //Type of schema e.g Concept, Simple

		private boolean added;

		public Vocabulary(String t, String c, String s) {
			this.term = t;
			this.termClass = c;
			this.schemaType = s;
			added = false;

		}

		public String getTerm() {
			return term;
		}

		public String getTermClass() {
			return termClass;
		}

		public String getSchemaType() {
			return schemaType;
		}

		public boolean isAdded() {
			return added;
		}

		public void add() {
			added = true;
		}
	}

	//This class represents a Vocabulary with one part of it
	public class OntConstruct {

		private Vocabulary eleVocabulary; //The vocabulary

		private Vocabulary partVocabulary; //One of the made-up part

		private String aggType; //Type of aggregration

		private int cardMin; //Min cardinality

		private int cardMax; //Max cardinality

		private String partName; //Name of this part in the vocabulary

		public OntConstruct(Vocabulary v, Vocabulary p) {
			this.eleVocabulary = v;
			this.partVocabulary = p;
		}

		public void setRelation(int cardMin, int cardMax, String aggType) {
			this.cardMin = cardMin;
			this.cardMax = cardMax;
			this.aggType = aggType;
		}

		public void setPartName(String s) {
			partName = s;
		}

		public String getPartName() {
			return partName;
		}

		public Vocabulary getEleVocabulary() {
			return eleVocabulary;
		}

		public Vocabulary getPartVocabulary() {
			return partVocabulary;
		}

		public int getCardMin() {
			return cardMin;
		}

		public int getCardMax() {
			return cardMax;
		}

	}

}