package ciamas.wsjade.management;

import javax.swing.JPanel;
import javax.swing.JButton;

import javax.swing.JLabel;

import org.apache.log4j.Logger;

import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.Dimension;
import java.awt.Toolkit;



/**
* This code was generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a
* for-profit company or business) then you should purchase
* a license - please visit www.cloudgarden.com for details.
*/
public class RemoteAgentManagerGUI extends javax.swing.JFrame  implements
java.awt.event.ActionListener{

	
	private JLabel jLabel1;
	private JButton authenBtn;
	private JButton serviceBtn;
	private JButton agentBtn;
	private JPanel mainPanel;
	private RemoteAgentManager mgr = null;
	NewAgentFrame ainst = new NewAgentFrame();
	NewServiceFrame sinst = new NewServiceFrame();
	AuthenDetails auinst = new AuthenDetails();
	
	public RemoteAgentManagerGUI() {
		initGUI();
	}
	public void setManager(RemoteAgentManager manager)
	{
		mgr = manager;
		ainst.setManager(manager);
		sinst.setManager(manager);
		auinst.setManager(manager);
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==authenBtn)
		{
			showAuthenGUI();
			
		}
		else if(e.getSource()==agentBtn)
		{
			showAgentGUI();
		}
		else if(e.getSource()==serviceBtn)
		{
			showServiceGUI();
		}

	}
	
	/**
	* Initializes the GUI.
	* Auto-generated code - any changes you make will disappear.
	*/
	public void initGUI(){
		try {
			preInitGUI();
	
			mainPanel = new JPanel();
			jLabel1 = new JLabel();
			agentBtn = new JButton();
			serviceBtn = new JButton();
			authenBtn = new JButton();
	
			BorderLayout thisLayout = new BorderLayout();
			this.getContentPane().setLayout(thisLayout);
			thisLayout.setHgap(0);
			thisLayout.setVgap(0);
			this.setSize(new java.awt.Dimension(373,104));
	
			FlowLayout mainPanelLayout = new FlowLayout();
			mainPanel.setLayout(mainPanelLayout);
			mainPanelLayout.setAlignment(FlowLayout.CENTER);
			mainPanelLayout.setHgap(5);
			mainPanelLayout.setVgap(5);
			mainPanel.setPreferredSize(new java.awt.Dimension(366,66));
			this.getContentPane().add(mainPanel, BorderLayout.CENTER);
	
			jLabel1.setText("                     Remote Web Service Agent Manager");
			jLabel1.setVisible(true);
			jLabel1.setPreferredSize(new java.awt.Dimension(332,18));
			mainPanel.add(jLabel1);
	
			agentBtn.setText("New Agent");
			agentBtn.setPreferredSize(new java.awt.Dimension(95,26));
			mainPanel.add(agentBtn);
	
			serviceBtn.setText("New Service");
			serviceBtn.setPreferredSize(new java.awt.Dimension(105,26));
			mainPanel.add(serviceBtn);
	
			authenBtn.setText("Authen Details");
			authenBtn.setPreferredSize(new java.awt.Dimension(116,26));
			mainPanel.add(authenBtn);
	
			postInitGUI();
		} catch (Exception e) {
			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
			//e.printStackTrace();
		}
	}
	/** Add your pre-init code in here 	*/
	public void preInitGUI(){
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		setLocation((screenSize.width - 400) / 2, (screenSize.height - 300) / 2);
		sinst.setLocation((screenSize.width - 400) / 2 +20, (screenSize.height - 300) / 2+90);
		ainst.setLocation((screenSize.width - 400) / 2+50, (screenSize.height - 300) / 2+90);
		auinst.setLocation((screenSize.width - 400) / 2+60, (screenSize.height - 300) / 2+90);
	}

	/** Add your post-init code in here 	*/
	public void postInitGUI(){
		agentBtn.addActionListener(this);
		serviceBtn.addActionListener(this);
		authenBtn.addActionListener(this);
	}



	
	public void showAuthenGUI(){
		try {
			//auinst.pack();
			auinst.setVisible(true);
		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
		}
	}
	public void showAgentGUI(){
		try {
			//ainst.pack();
			ainst.setVisible(true);
		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
		}
	}
	public void showServiceGUI(){
		try {
		//sinst.pack();
			
			sinst.setVisible(true);
		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
		}
	}
}
