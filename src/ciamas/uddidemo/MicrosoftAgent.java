package ciamas.uddidemo;

import jade.content.*;
import jade.content.onto.*;
import jade.core.*;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import jade.util.leap.*;
import jade.content.onto.basic.*;
import jade.content.lang.*;
import jade.content.lang.sl.*;
import JadeGen.onto.reflect.api_v2.uddi_org.*;

import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.DefaultListModel;

import org.apache.log4j.Logger;

/**
 * <p>UDDI Agent</p>
 *
 * <p>Description:  UDDI proxy Agent</p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: CIAMAS, Swinburne University</p>
 *
 * @author Mohan Buruwal Chhetri
 *         
 * @version 1.0
 */
public class MicrosoftAgent extends Agent {
    public MicrosoftAgent() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // CREATE THE ONTOLOGY INSTANCE
    private ContentManager manager  = (ContentManager) getContentManager();
    private Codec codec = new SLCodec();
    private Ontology ontology = JadeGen.onto.ServiceInquiry.WebOntology.getInstance();
    private Ontology adminOntology = ciamas.wsjade.wsdl2jade.onto.WebAdminOntology.getInstance();
    
    public int search_Type = 0;
    public String search_Term = "";
    //private ACLMessage requestMsg;
    private WSClientGUI _client;
    private ArrayList tempResult = null;
    private String wsdlURL = null;

    public void setup() {
       // REGISTER THE ONTOLOGY
       manager.registerLanguage(codec);
       manager.registerOntology(ontology);
       manager.registerOntology(adminOntology);

       // ADD HANDLEINFORMBEHAVIOUR
       addBehaviour(new HandleInformBehaviour(this));

      
       // Create the interface
       try {
           UIManager.setLookAndFeel(
            "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
           _client = new WSClientGUI(this);
       }
       catch (Exception e) {
           e.printStackTrace();
       }
    }
    private ACLMessage createReqMessage()
    {
    	//    	 CREATE A REQUEST MESSAGE
    	ACLMessage requestMsg = new ACLMessage(ACLMessage.REQUEST);

        requestMsg.setSender(getAID());

        // IDENTIFY THE WEB SERVICE AGENT
        requestMsg.addReceiver(new AID("MSUDDIAgent", false));
        requestMsg.setLanguage(codec.getName());
        requestMsg.setOntology(ontology.getName());
        return requestMsg;
    }
    public void requestNewService(String uri, String aName, String sName, String uname, String pword)
    {
    	//CREATE A REQUEST MESSAGE
		ACLMessage requestMsg = new ACLMessage(ACLMessage.REQUEST); 		
		requestMsg.setSender(getAID());

		// IDENTIFY THE WEB SERVICE AGENT 
		requestMsg.addReceiver(new AID("WSManager", false)); 
		requestMsg.setLanguage(codec.getName());
		requestMsg.setOntology(adminOntology.getName());

		ciamas.wsjade.wsdl2jade.onto.ServiceDeploymentRequest requestAction = 
		new ciamas.wsjade.wsdl2jade.onto.ServiceDeploymentRequest();
		requestAction.setAgentName(aName);
		requestAction.setServiceName(sName);
		requestAction.setUri(uri);
		requestAction.setUsername(uname);
		requestAction.setPassword(pword);

	     try 
	     { 
			// SEND THE MESSAGE
			manager.fillContent( requestMsg, 
					new Action(new AID("MicrosoftAgent", false), requestAction));
					
			send(requestMsg);
		
	     } 
	     catch(Exception e) 
	     {
	     	Logger.getLogger("WS2JADE").debug("Exception occurred", e);
	     	//e.printStackTrace(); 
	     	}
    }
    

    protected void start_Search() {
       if(this.search_Type==1) {
           JadeGen.onto.reflect.api_v2.uddi_org.FindService fi_Service = this.serviceAction();
           this.sendMessage(createReqMessage(), 1, fi_Service);
       }
       else if(this.search_Type==2) {
           JadeGen.onto.reflect.api_v2.uddi_org.FindBusiness fi_Service = this.providerAction();
           this.sendMessage(createReqMessage(), 2, fi_Service);
       }
       else if(this.search_Type==3) {
           JadeGen.onto.reflect.api_v2.uddi_org.FindTModel fi_Service = this.tModelAction();
           this.sendMessage(createReqMessage(), 3, fi_Service);
       }
   }
    protected void start_QueryTModel(String tName)
    {
    	System.out.println("Query information for TModel name="+tName);
    	String tKey = null;
    	if(tempResult == null)
    		return;
    	
    	for(int i=0;i<tempResult.size();i++)
    	{
    		TModelInfo t_Inf = (TModelInfo) tempResult.
            get(i);
	    	 if(t_Inf.getName().equals(tName))
	    	 {
	    	 	tKey = t_Inf.getTModelKey();
	    	 	break;
	    	 }		  	 	
    	}
    	if(tKey!=null)
    		sendQueryTModel(tKey);
    	
    }

    private void sendQueryTModel(String tKey)
    {
    	GetTModelDetail tModelDetail = new GetTModelDetail();
    	tModelDetail.setGeneric("2.0");
    	ArrayList arrayKeys = new ArrayList();
    	arrayKeys.add(tKey);
    	tModelDetail.setTModelKey(arrayKeys);
    	//Now create a message content
    	JadeGen.onto.ServiceInquiry.InquireMessagesSoap.Request.GetTModelDetail
		getTModelDetail = new JadeGen.onto.ServiceInquiry.InquireMessagesSoap.Request.GetTModelDetail();
    	getTModelDetail.setGtmd(tModelDetail);
    	//Fill content and send off the request message
    	 try {
            // SEND THE MESSAGE
    	 	ACLMessage requestMsg=createReqMessage();
            manager.fillContent(requestMsg,
                                new Action(new AID("MicrosoftAgent", false),
                                           getTModelDetail));
            send(requestMsg);
    	 } catch (Exception e) {
          e.printStackTrace();
      	}
    	
    	
    	
    	
    }
   private void sendMessage(ACLMessage requestMsg, int search_Type, Object search_Args) {
       if(search_Type==1) {
           JadeGen.onto.reflect.api_v2.uddi_org.FindService fi_Service =
                   (JadeGen.onto.reflect.api_v2.uddi_org.FindService) search_Args;
           JadeGen.onto.ServiceInquiry.InquireMessagesSoap.Request.FindService f_Service
                   = new JadeGen.onto.ServiceInquiry.InquireMessagesSoap.Request.FindService();
           f_Service.setFs(fi_Service);
           try {
                 // SEND THE MESSAGE
                 manager.fillContent(requestMsg,
                                     new Action(new AID("MicrosoftAgent", false),
                                                f_Service));
                 send(requestMsg);
           } catch (Exception e) {
               e.printStackTrace();
           }
       }
       else if(search_Type==2) {
           JadeGen.onto.reflect.api_v2.uddi_org.FindBusiness fi_Service =
                              (JadeGen.onto.reflect.api_v2.uddi_org.FindBusiness) search_Args;
                      JadeGen.onto.ServiceInquiry.InquireMessagesSoap.Request.FindBusiness f_Service
                              = new JadeGen.onto.ServiceInquiry.InquireMessagesSoap.Request.FindBusiness();
                      f_Service.setFbus(fi_Service);
                      try {
                            // SEND THE MESSAGE
                            manager.fillContent(requestMsg,
                                                new Action(new AID("MicrosoftAgent", false),
                                                           f_Service));
                            send(requestMsg);
                      } catch (Exception e) {
                          e.printStackTrace();
           }
       }
       else if(search_Type==3) {
           JadeGen.onto.reflect.api_v2.uddi_org.FindTModel fi_Service =
                              (JadeGen.onto.reflect.api_v2.uddi_org.FindTModel) search_Args;
                      JadeGen.onto.ServiceInquiry.InquireMessagesSoap.Request.FindTModel f_Service
                              = new JadeGen.onto.ServiceInquiry.InquireMessagesSoap.Request.FindTModel();
                      f_Service.setFtm(fi_Service);
                      System.out.println(f_Service.getFtm().toString());
                      //System.out.println("hlhlhlkhj " + f_Service.getFtm().getName());
                      try {
                            // SEND THE MESSAGE
                            manager.fillContent(requestMsg,
                                                new Action(new AID("MicrosoftAgent", false),
                                                           f_Service));
                            send(requestMsg);
                      } catch (Exception e) {
                          e.printStackTrace();
           }
       }
     
   }

   // Method to find service information based of service name
   public JadeGen.onto.reflect.api_v2.uddi_org.FindService serviceAction() {
       JadeGen.onto.reflect.api_v2.uddi_org.FindService fi_Service =
            new JadeGen.onto.reflect.api_v2.uddi_org.FindService();

       // INSTANTIATE THE CHOSEN AGENTACTION CLASS

       // Add the name of service to search for
       JadeGen.onto.reflect.api_v2.uddi_org.Name _name = new Name();
       _name.set_value(this.search_Term);

       jade.util.leap.ArrayList name_List = new ArrayList();
       name_List.add(_name);
       fi_Service.setName(name_List);

       // set generic
       fi_Service.setGeneric("2.0");

       // set max rows
       fi_Service.setMaxRows(5);
       return fi_Service;
   }

   // Method to find list of providers of web services
   public JadeGen.onto.reflect.api_v2.uddi_org.FindBusiness providerAction() {
       JadeGen.onto.reflect.api_v2.uddi_org.FindBusiness fi_Business =
            new JadeGen.onto.reflect.api_v2.uddi_org.FindBusiness();

       JadeGen.onto.reflect.api_v2.uddi_org.Name _name = new Name();
       _name.set_value(this.search_Term);
       System.out.println("Search term " + search_Term);

       jade.util.leap.ArrayList name_List = new ArrayList();
       name_List.add(_name);
       fi_Business.setName(name_List);
       fi_Business.setGeneric("2.0");
       fi_Business.setMaxRows(5);
       return fi_Business;
   }

   // Method to find list of tModels of web services
   public JadeGen.onto.reflect.api_v2.uddi_org.FindTModel tModelAction() {
       JadeGen.onto.reflect.api_v2.uddi_org.FindTModel fi_tModel =
            new JadeGen.onto.reflect.api_v2.uddi_org.FindTModel();

       fi_tModel.setName(this.search_Term);
       fi_tModel.setGeneric("2.0");
       fi_tModel.setMaxRows(5);
       return fi_tModel;
   }

    private void jbInit() throws Exception {
    }

    class HandleInformBehaviour extends CyclicBehaviour
   {
       public HandleInformBehaviour(Agent a) { super(a); }
       

       public void action()
       {
           ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));
           if (msg != null)
           {
               try
               {
                   ContentElement ce = manager.extractContent(msg);

                   if (ce instanceof jade.content.onto.basic.Result)
                   {
                       jade.content.onto.basic.Result result = (jade.content.onto.basic.Result)ce;
                       if(search_Type == 1) {
                           JadeGen.onto.ServiceInquiry.InquireMessagesSoap.
                                   Response.FindService ret =
                                           (JadeGen.onto.ServiceInquiry.
                                            InquireMessagesSoap.Response.
                                            FindService) result.getValue();
                           ServiceList s_List = ret.getFindServiceResult();
                           JadeGen.onto.reflect.api_v2.uddi_org.ArrayOfServiceInfo s_Info = s_List.getServiceInfos();
                           ArrayList info = s_Info.getServiceInfo();
                           System.out.println("Result ");
                           _client.result_List.setModel(new DefaultListModel());
                           for(int i = 0; i < info.size(); i++) {
                               ServiceInfo s = (ServiceInfo) info.get(i);
                               Name n = (Name) s.getName().get(0);
                               ( (DefaultListModel)_client.result_List.getModel()).addElement(
                                       n.get_value());
                               // Display all details about the service
                               System.out.println("Name: ");
                               for(int j = 0; j < s.getName().size(); j++) {
                                   Name _name = (Name) s.getName().get(j);
                                   System.out.println(_name);
                               }
                               System.out.println("Service Key");
                               System.out.println(s.getServiceKey());
                           }
                           System.out.println(s_List.getGeneric());
                       }
                       else if(search_Type == 2) {
                           JadeGen.onto.ServiceInquiry.InquireMessagesSoap.
                                   Response.FindBusiness ret =
                                           (JadeGen.onto.ServiceInquiry.
                                            InquireMessagesSoap.Response.
                                            FindBusiness) result.getValue();
                           BusinessList s_List = ret.getFindBusinessResult();
                           JadeGen.onto.reflect.api_v2.uddi_org.ArrayOfBusinessInfo b_Info = s_List.getBusinessInfos();
                           ArrayList info = b_Info.getBusinessInfo();
                           if(info==null) {
                               System.out.println("No results");
                               ((DefaultListModel) _client.result_List.
                                getModel()).addElement(
                                        "0 record(s) found");
                           }
                           else {
                               for (int i = 0; i < info.size(); i++) {
                                   BusinessInfo b_Inf = (BusinessInfo) info.get(
                                           i);
                                   ((DefaultListModel) _client.result_List.
                                    getModel()).addElement(
                                            b_Inf.getName().get(0).toString());
                                   System.out.println(b_Inf.getBusinessKey());
                                   if(b_Inf.getDescription()!=null) {
                                       System.out.println(b_Inf.getDescription().toString());
                                   }
                                   if(b_Inf.getName()!=null) {
                                       System.out.println(b_Inf.getName().
                                               toString());
                                   }
                                   if(b_Inf.getServiceInfos()!=null) {
                                       System.out.println(b_Inf.getServiceInfos().
                                               toString());
                                   }
                               }
                           }
                       }
                       else if(search_Type == 3) {
                           JadeGen.onto.ServiceInquiry.InquireMessagesSoap.
                                   Response.FindTModel ret =
                                           (JadeGen.onto.ServiceInquiry.
                                            InquireMessagesSoap.Response.
                                            FindTModel) result.getValue();
                           TModelList s_List = ret.getFindTModelResult();
                           
                           JadeGen.onto.reflect.api_v2.uddi_org.ArrayOfTModelInfo t_Info = s_List.getTModelInfos();
                           
                           ArrayList model_List = t_Info.getTModelInfo();
                           tempResult = model_List;
                           if(model_List==null) {
                               ((DefaultListModel) _client.result_List.
                                getModel()).addElement(
                                        "No results found");

                           }
                           else {
                               _client.result_List.setModel(new
                                       DefaultListModel());
                               for (int i = 0; i < model_List.size(); i++) {
                                   TModelInfo t_Inf = (TModelInfo) model_List.
                                           get(i);
                                   ((DefaultListModel) _client.result_List.
                                    getModel()).addElement(
                                            t_Inf.getName());
                               }
                           }
                       }
                       else if(search_Type == 4)
                       {
                       	
                        JadeGen.onto.ServiceInquiry.InquireMessagesSoap.
                        Response.GetTModelDetail ret =
                                (JadeGen.onto.ServiceInquiry.InquireMessagesSoap.
                                        Response.GetTModelDetail) result.getValue();
		                TModelDetail t_List = ret.getGetTModelDetailResult();
		                ArrayList t_Models = t_List.getTModel();
		                TModel t_Model = (TModel)t_Models.get(0);
		                TModelView tModelView = new TModelView();
		                
		                tModelView.setModelCatCb(t_Model.getCategoryBag());
		                tModelView.setModelDescTb(t_Model.getDescription());
		                wsdlURL=tModelView.setModelDocTb(t_Model.getOverviewDoc());
		                tModelView.setModelIdenTb(t_Model.getIdentifierBag());
		                tModelView.setModelKeyTb(t_Model.getTModelKey());
		                tModelView.setModelName(t_Model.getName());
		                tModelView.setModelOwnerTb(t_Model.getOperator());
						tModelView.setMSAgent((MicrosoftAgent)myAgent);
						
						tModelView.displayGUI();
              
                       }
                       else if(search_Type ==5)
                       {
                       	ciamas.wsjade.wsdl2jade.onto.DeploymentResponse	ret =
    						
    					(ciamas.wsjade.wsdl2jade.onto.DeploymentResponse)result.getValue();
    					System.out.println(ret.getSuccess());
    					String retMsg = null;
    					if(ret.getSuccess())
    						retMsg ="Deployment Successful!";
    					else
    						retMsg ="Deployment Failed";
    						
    					JOptionPane.showMessageDialog(new javax.swing.JFrame(), retMsg);
    					 
    					
                       }
              _client.search_ProgressBar.setIndeterminate(false);
        }
    }catch(Exception e)
    {
        e.printStackTrace();
    }
} else {
    block(); }
}
}
}
