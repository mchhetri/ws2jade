/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import javax.wsdl.Message;
import javax.wsdl.Operation;
import javax.wsdl.PortType;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.PortTypeEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.toJava.JavaBindingWriter;
import org.apache.axis.wsdl.toJava.Utils;

import ciamas.wsjade.wsdl2jade.utils.Common;

public class JadeInterfaceWriter implements Generator {
	protected PortType portType;

	protected OntParser ontParser;

	protected SymbolTable symbolTable;

	protected BindingEntry bEntry;

	protected Vector writers = new Vector();

	protected JadeInterfaceWriter(OntParser parser, PortTypeEntry ptEntry,
			BindingEntry bEntry, SymbolTable symbolTable) {

		this.portType = ptEntry.getPortType();
		this.ontParser = parser;
		this.symbolTable = symbolTable;
		this.bEntry = bEntry;

	}

	public void generate() throws IOException {
		setGenerators();

		for (int i = 0; i < writers.size(); i++) {
			if (writers.elementAt(i) instanceof JadeActionWriter)
				((JadeActionWriter) writers.elementAt(i)).generate();
			else if (writers.elementAt(i) instanceof JadeConceptWriter)
				((JadeConceptWriter) writers.elementAt(i)).generate();

		}

	}

	protected void setGenerators() {
		Iterator operations = portType.getOperations().iterator();
		while (operations.hasNext()) {
			Operation operation = (Operation) operations.next();
			createWriters(operation);
		}

	}

	private void createWriters(Operation operation) {
		Message inputMsg = operation.getInput().getMessage();
		String inputName = inputMsg.getQName().toString();

		Message outputMsg = null;
		String outputName = null;
		if(operation.getOutput()!=null)
		{
			outputMsg=operation.getOutput().getMessage();
			outputName=outputMsg.getQName().toString();
		}

		GenCache genCache = GenCache.getInstance();

		genCache.record((String) bEntry
				.getDynamicVar(JavaBindingWriter.INTERFACE_NAME), Utils
				.xmlNameToJava(operation.getName()), Common.getInstance()
				.getJavaName(portType), bEntry.getParameters(operation));

		if (!genCache.existing(GenCache.IN_TYPE, inputName)) {

			genCache.record(GenCache.IN_TYPE, inputName);
		}

			writers.addElement(new JadeActionWriter(ontParser, operation,
					portType, bEntry, symbolTable));

		
        if(outputName !=null)
		if (!genCache.existing(GenCache.OUT_TYPE, outputName)) {
			genCache.record(GenCache.OUT_TYPE, outputName);
		}

			writers.addElement(new JadeConceptWriter(ontParser, operation,
					portType, bEntry, symbolTable));
		

	}

}