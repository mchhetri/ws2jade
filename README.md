# README #

WS2JADE software has been developed by researchers at the Intelligent Agent Technology Group (IAT) located at the School of Software and Electrical Engineering, Faculty of Science, Engineering and Technology, Swinburne University of Technology, Melbourne, Australia. It is a toolkit that enables Jade agents to access and use Web services. There are two main components in WS2JADE: ontology generator and service deployment manager. Ontology Generator generates necessary ontologies for Jade agents to use a Web service. Service Deployment Manager provides facilities to deploy and control Web serives as agent services at runtime. It is the first step toward automation of Web service invocations by Agent software. OpenNet@Swin is a sample of our WS2JADE at work. This work was part of research work done by Dr. Xuan Thang Nguyen and [Prof. Ryszard Kowalczyk](http://www.ict.swin.edu.au/centres/success/iat/tiki-index.php?page=rkowalczyk). 

**This project is no longer under active development.**

**Warning:** Some versions of java 1.5 are not compatible (due to axis 1.2.1) and may fail to parse certain xml data types. java 1.4.* is best recommended 

Please look at the short guide for usage. 
* Fixes on DF service registration
* Ontology handler for Axis native types
* Remote deployment capabilities

### Disclaimers & Copyright ###

The WS2JADE v1.5 is free software, you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License as published by the Free Software Foundation; either version 2 of the License, or any later version. The library is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more details. NO WARRANTY. 

We would appreciate information on WS2JADE use and modifications, as well as acknowledgements of IAT in projects and publications involving WS2JADE. 

### Related Software ###

[WSAI-AgentCities](http://wsai.sourceforge.net/) - This software is a tool for exposing Agent services to Web service environment. It is complementary to WS2JADE.

### Who do I talk to? ###

* mchhetri 'at' swin.edu.au (I do not maintain the code)