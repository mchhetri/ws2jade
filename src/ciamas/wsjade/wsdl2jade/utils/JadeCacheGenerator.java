/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.utils;

import java.io.IOException;
import java.util.Iterator;
import java.util.Map;

import javax.wsdl.Binding;
import javax.wsdl.Port;
import javax.wsdl.Service;

import org.apache.axis.utils.JavaUtils;
import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.PortTypeEntry;
import org.apache.axis.wsdl.symbolTable.ServiceEntry;
import org.apache.axis.wsdl.symbolTable.SymTabEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.toJava.Utils;

import ciamas.wsjade.wsdl2jade.writers.GenCache;

/**
 * This generator does caching.
 */
public class JadeCacheGenerator implements Generator {

	public static int JAX_SERVICE_STUB = 0;

	public static int JAX_BINDING_STUB = 1;

	public static int JAX_PORT_STUB = 2;

	private String cacheString = null;

	private int cacheType = -1;

	private SymTabEntry entry = null;

	private SymbolTable symbolTable = null;

	public JadeCacheGenerator() {
		super();
	}

	public JadeCacheGenerator(int type, String s, SymTabEntry e, SymbolTable t) {
		super();
		cacheType = type;
		cacheString = s;
		entry = e;
		symbolTable = t;
	}

	public void setCacheType(int i) {
		cacheType = i;
	}

	public int getCacheType() {
		return cacheType;
	}

	public void setEntry(SymTabEntry e) {
		entry = e;
	}

	public SymTabEntry getEntry() {
		return entry;
	}

	public void setCacheString(String s) {
		cacheString = s;
	}

	public String getCacheString() {
		return cacheString;
	}

	public void generate() throws IOException {
		GenCache genCache = GenCache.getInstance();
		if (cacheType == JAX_SERVICE_STUB) {
			genCache.setStubServiceClass(cacheString);
			//System.out.println("service stub: " + cacheString);
			ServiceEntry sEntry = (ServiceEntry) entry;
			Service service = sEntry.getService();

			// get ports
			Map portMap = service.getPorts();
			Iterator portIterator = portMap.values().iterator();

			// write a get method for each of the ports with a SOAP binding
			while (portIterator.hasNext()) {
				Port p = (Port) portIterator.next();
				Binding binding = p.getBinding();
				if (binding == null)
					continue;

				BindingEntry bEntry = symbolTable.getBindingEntry(binding
						.getQName());
				if (bEntry == null)
					continue;

				PortTypeEntry ptEntry = symbolTable.getPortTypeEntry(binding
						.getPortType().getQName());
				if (ptEntry == null)
					continue;

				// If this isn't an SOAP binding, skip
				if (bEntry.getBindingType() != BindingEntry.TYPE_SOAP) {
					continue;
				}

				String portName = p.getName();
				if (!JavaUtils.isJavaId(portName)) {
					portName = Utils.xmlNameToJavaClass(portName);
				}

				genCache.record(portName, binding.getPortType().getQName()
						.getLocalPart());
			}

		} else if (cacheType == JAX_PORT_STUB) {
			//genCache.addStubPortClass(cacheString);
			//System.out.println("port stub: " + cacheString);
		}

	} // generate

}