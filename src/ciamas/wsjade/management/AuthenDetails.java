package ciamas.wsjade.management;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JPasswordField;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import org.apache.log4j.Logger;

/**
* This code was generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a
* for-profit company or business) then you should purchase
* a license - please visit www.cloudgarden.com for details.
*/
public class AuthenDetails extends javax.swing.JFrame   implements
java.awt.event.ActionListener{

	private RemoteAgentManager mgr;
	private JButton cancelBtn;
	private JPasswordField passwordTf;
	private JButton okBtn;
	private JLabel jLabel3;
	private JTextField usernameTf;
	private JLabel jLabel2;
	private JLabel jLabel1;
	private JPanel mainPanel;
	
	private Logger log = Logger.getLogger("WS2JADE");
	public AuthenDetails() {
		initGUI();
	}

	/**
	* Initializes the GUI.
	* Auto-generated code - any changes you make will disappear.
	*/
	public void initGUI(){
		
		try {
			preInitGUI();
	
			mainPanel = new JPanel();
			jLabel1 = new JLabel();
			jLabel2 = new JLabel();
			usernameTf = new JTextField();
			jLabel3 = new JLabel();
			okBtn = new JButton();
			passwordTf = new JPasswordField();
			cancelBtn = new JButton();
	
			BorderLayout thisLayout = new BorderLayout();
			this.getContentPane().setLayout(thisLayout);
			thisLayout.setHgap(0);
			thisLayout.setVgap(0);
			this.setSize(new java.awt.Dimension(263,142));
	
			mainPanel.setLayout(null);
			mainPanel.setPreferredSize(new java.awt.Dimension(280,116));
			this.getContentPane().add(mainPanel, BorderLayout.CENTER);
	
			jLabel1.setText("Authentication Details");
			jLabel1.setPreferredSize(new java.awt.Dimension(124,16));
			jLabel1.setBounds(new java.awt.Rectangle(90,5,124,16));
			mainPanel.add(jLabel1);
	
			jLabel2.setText("User name");
			jLabel2.setPreferredSize(new java.awt.Dimension(77,19));
			jLabel2.setBounds(new java.awt.Rectangle(10,31,77,19));
			mainPanel.add(jLabel2);
	
			usernameTf.setPreferredSize(new java.awt.Dimension(156,19));
			usernameTf.setBounds(new java.awt.Rectangle(87,31,156,19));
			mainPanel.add(usernameTf);
	
			jLabel3.setText("Password");
			jLabel3.setPreferredSize(new java.awt.Dimension(60,20));
			jLabel3.setBounds(new java.awt.Rectangle(9,60,60,20));
			mainPanel.add(jLabel3);
	
			okBtn.setText("OK");
			okBtn.setPreferredSize(new java.awt.Dimension(74,20));
			okBtn.setBounds(new java.awt.Rectangle(84,89,74,20));
			mainPanel.add(okBtn);
	
			passwordTf.setPreferredSize(new java.awt.Dimension(155,20));
			passwordTf.setBounds(new java.awt.Rectangle(87,62,155,20));
			mainPanel.add(passwordTf);
	
			cancelBtn.setText("Cancel");
			cancelBtn.setPreferredSize(new java.awt.Dimension(76,20));
			cancelBtn.setBounds(new java.awt.Rectangle(161,90,76,20));
			mainPanel.add(cancelBtn);
	
			postInitGUI();
		} catch (Exception e) {
			log.debug(e.getStackTrace());
		}
	}
	/** Add your pre-init code in here 	*/
	public void preInitGUI(){
	}

	/** Add your post-init code in here 	*/
	public void postInitGUI(){
		okBtn.addActionListener(this);
		cancelBtn.addActionListener(this);
	}
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource()==okBtn)
			mgr.setAuthen(usernameTf.getText(), passwordTf.getText());
		
			this.setVisible(false);
		

	}
	public void setManager(RemoteAgentManager manager)
	{
		mgr=manager ;
	}
	
	/** Auto-generated main method */
	public static void main(String[] args){
		AuthenDetails inst = new AuthenDetails();
		inst.showGUI();
	}

	/**
	* This static method creates a new instance of this class and shows
	* it inside a new JFrame, (unless it is already a JFrame).
	*
	* It is a convenience method for showing the GUI, but it can be
	* copied and used as a basis for your own code.	*
	* It is auto-generated code - the body of this method will be
	* re-generated after any changes are made to the GUI.
	* However, if you delete this method it will not be re-created.	*/
	public void showGUI(){
		try {
			setVisible(true);
		} catch (Exception e) {
			log.debug(e.getStackTrace());
			
		}
	}
}
