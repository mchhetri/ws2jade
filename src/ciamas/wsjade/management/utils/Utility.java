/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.management.utils;

import java.io.*;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import org.apache.log4j.Logger;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class Utility {

	public static int ENV_SETTING_COMMAND = 0;

	public static int SCRIPT_EXTENSTION = 1;

	public static int ENV_VALUE_SEPARATOR = 2;

	public static int ENV_VAR_PREFIX = 3;
	public static int LINUX_OS =0;
	public static int WINDOW_OS =1;

	public static void main(String[] args) {
		System.out.println(Utility.getLibDir());
	}

	public static String generateClsEnv() {
		String dir = getLibDir();
		String cls_env = new String();
		//FIX ME: Loop and read files from lib folder
		cls_env = dir + "wsdl4j-1.5.1.jar" + File.pathSeparatorChar + dir
				+ "axis.jar" + File.pathSeparatorChar + dir + "axis-ant.jar"
				+ File.pathSeparatorChar + dir + "Base64.jar.jar"
				+ File.pathSeparatorChar + dir + "commons-discovery-0.2.jar"
				+ File.pathSeparatorChar + dir + "commons-logging-1.0.4.jar"
				+ File.pathSeparatorChar + dir + "iiop.jar"
				+ File.pathSeparatorChar + dir + "jade.jar"
				+ File.pathSeparatorChar + dir + "jadeTools.jar"
				+ File.pathSeparatorChar + dir + "jaxrpc.jar"
				+ File.pathSeparatorChar + dir + "ws2jade-1.2.1.jar"
				+ File.pathSeparatorChar + dir + "log4j-1.2.8.jar"
				+ File.pathSeparatorChar + dir + "saaj.jar";

		return cls_env;
	}

	public static String getLibDir() {
		//URL url = Utility.class.getResource("resources/splash.png");
		//String fName = url.getFile().substring(1);
		//String folderName = fName
		//		.substring(
		//				0,
		//				fName
		//						.lastIndexOf("ciamas/wsjade/management/utils/resources/splash.png"));
		//return folderName.replace('/', File.separatorChar) + "lib"
		//		+ File.separatorChar;
		return ".." + File.separatorChar +"lib" + File.separatorChar;
	}

	public static boolean validFile(String s) {
		boolean result = false;
		File file = new File(s);
		if (!file.exists()) {
			try {
				if (file.mkdir())
					result = true;
			} catch (SecurityException e) {
				Logger log =Logger.getLogger("WS2JADE");
				log.error("Cannot create "+s);
				log.debug("Exception occurred", e);
				
				result = false;
			}
		} else if (!file.canWrite())
			result = false;
		else
			result = true;

		return result;
	}

	public static int getOSType()
	{
		String OS = System.getProperty("os.name").toLowerCase();
		
		if ((OS.indexOf("windows 9") > -1) ||
			(OS.indexOf("nt") > -1) || 
			(OS.indexOf("windows 2000") > -1)
				|| (OS.indexOf("windows xp") > -1)) {

		return WINDOW_OS;
		}
		else 
		{
			// we assume Unix
			return LINUX_OS;
		}	

	}
	
	public static String getBourneShell()
	{
		try
		{
		Runtime rt = Runtime.getRuntime();
		Process proc = Runtime.getRuntime().exec("which sh");
		InputStream stdin = proc.getInputStream();
        InputStreamReader isr = new InputStreamReader(stdin);
        BufferedReader br = new BufferedReader(isr);
        String line = null;
        
        while ( (line = br.readLine()) != null)
            return line;
		}
		catch(Exception e)
		{
			//e.printStackTrace();
			Logger log =Logger.getLogger("WS2JADE");
			log.debug("Exception occurred", e);
			log.info("Fail to locate sh shell");
		}
		return null;


	}
	public static String getSystemCommand(int s) {
	
		String command = null;
		// System.out.println(OS);
		if (getOSType() == WINDOW_OS) {

			if (s == ENV_SETTING_COMMAND)
				command = "set";
			else if (s == ENV_VALUE_SEPARATOR)
				command = ";";
			else if (s == ENV_VAR_PREFIX)
				command = "%";
			else if (s == SCRIPT_EXTENSTION)
				command = ".bat";
		} else {
			// we assume Unix
			if (s == ENV_SETTING_COMMAND)
				command = "export";
			else if (s == ENV_VALUE_SEPARATOR)
				command = ":";
			else if (s == ENV_VAR_PREFIX)
				command = "$";
			else if (s == SCRIPT_EXTENSTION)
				command = ".sh";
		}
		return command;

	}

	public static String getPkgRoot(String dir, String excludedStr) {
		String ret = new String();
		File f = new File(dir);
		String[] l = f.list();
		for (int i = 0; i < l.length; i++) {
			//System.out.println(l[i]);
			if (!(l[i].equals("JadeGen")))
				if (l[i].indexOf('.') < 0)
					ret = ret + l[i] + " ";
		}

		return ret;
	}

	private static final Class[] parameters = new Class[] { URL.class };

	public static void addFile(String s) throws IOException {
		File f = new File(s);
		addFile(f);
	}//end method

	public static void addFile(File f) throws IOException {
		addURL(f.toURL());
	}//end method

	//FIX ME: Hack to add new lib, can be dangerous
	public static void addURL(URL u) throws IOException {
		URLClassLoader sysloader = (URLClassLoader) ClassLoader
				.getSystemClassLoader();
		Class sysclass = URLClassLoader.class;
		try {
			Method method = sysclass.getDeclaredMethod("addURL", parameters);
			method.setAccessible(true);
			method.invoke(sysloader, new Object[] { u });
		} catch (Throwable t) {
			//t.printStackTrace();
			Logger.getLogger("WS2JADE").debug(String.valueOf(t));
			throw new IOException(
					"Error, could not add URL to system classloader");
		}//end try catch
	}//end method

}