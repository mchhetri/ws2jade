/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.wsdl2jade.onto;

import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.ConceptSchema;
import jade.content.schema.PrimitiveSchema;
/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class WSOntology extends Ontology {
	public static final String ONTOLOGY_NAME = "ciamas.ws-ontology";

	// The singleton instance of this ontology
	private static Ontology theInstance = new WSOntology(BasicOntology
			.getInstance());

	public static Ontology getInstance() {
		return theInstance;
	}

	/** * Constructor ** */
	private WSOntology(Ontology base) {
		super(ONTOLOGY_NAME, base);
		try {

			//Add Error Concept
			add(new ConceptSchema("WS Error"),
					ciamas.wsjade.wsdl2jade.onto.Error.class);

			ConceptSchema error = (ConceptSchema) getSchema("WS Error");
			error.add("errorMsg",
				(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			
			//			Add Day Concept
			add(new ConceptSchema("Axis Day"),
					ciamas.wsjade.wsdl2jade.onto.Day.class);

			ConceptSchema day = (ConceptSchema) getSchema("Axis Day");
			day.add("day",
				(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.INTEGER));
			day.add("timezone",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			
			add(new ConceptSchema("Axis Month"),
					ciamas.wsjade.wsdl2jade.onto.Day.class);

			ConceptSchema month = (ConceptSchema) getSchema("Axis Month");
			month.add("month",
				(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.INTEGER));
			month.add("timezone",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			
			add(new ConceptSchema("Axis MonthDay"),
					ciamas.wsjade.wsdl2jade.onto.Day.class);

			ConceptSchema monthday = (ConceptSchema) getSchema("Axis MonthDay");
			monthday.add("month",
				(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.INTEGER));
			monthday.add("day",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.INTEGER));
			monthday.add("timezone",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
	
			add(new ConceptSchema("Axis Duration"),
					ciamas.wsjade.wsdl2jade.onto.Day.class);

			ConceptSchema dur = (ConceptSchema) getSchema("Axis Duration");
			dur.add("months",
				(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.INTEGER));
			dur.add("days",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.INTEGER));
			dur.add("hours",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.INTEGER));
			dur.add("years",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.INTEGER));
			dur.add("minutes",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.INTEGER));
			dur.add("seconds",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.FLOAT));
			dur.add("isNegative",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.BOOLEAN));

			add(new ConceptSchema("Unknow Concept"),
					ciamas.wsjade.wsdl2jade.onto.Unknown.class);
			//TO DO:	Add other concepts
			
			
		} catch (OntologyException oe) {
			oe.printStackTrace();
		}
	}

}
