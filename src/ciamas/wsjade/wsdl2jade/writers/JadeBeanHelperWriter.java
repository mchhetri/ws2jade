/*
 * Created on 16/06/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ciamas.wsjade.wsdl2jade.writers;

/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
import org.apache.axis.wsdl.toJava.*;
import org.apache.axis.wsdl.symbolTable.SchemaUtils;
import org.apache.axis.wsdl.symbolTable.TypeEntry;
import java.util.*;

public class JadeBeanHelperWriter extends JavaBeanHelperWriter{
	
	public Set reservedPropNames;
	protected JadeBeanHelperWriter(Emitter emitter, TypeEntry type,
            Vector elements, TypeEntry extendType,
            Vector attributes, Set reservedPropNames) {

         super(emitter, type,elements, extendType, attributes, reservedPropNames);

	this.type = type;
	this.elements = elements;
	this.attributes = attributes;
	this.extendType = extendType;
	this.reservedPropNames = reservedPropNames;
	
	
	if ((null != extendType)
	&& (null
	!= SchemaUtils.getComplexElementRestrictionBase(
	 type.getNode(), emitter.getSymbolTable()))) {
	this.canSearchParents = false;
	} else {
	this.canSearchParents = true;
	}
	}    

}
