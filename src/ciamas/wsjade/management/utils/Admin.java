/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.management.utils;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;

import javax.swing.tree.DefaultMutableTreeNode;

import org.apache.log4j.Logger;

import ciamas.wsjade.management.Authorization;
import ciamas.wsjade.management.ContainerManager;
import ciamas.wsjade.management.OntoManager;

/**
 * 
 * @author xnguyen
 */

public class Admin {

	private ContainerManager cmanager = null;

	private Config config = null;

	private OntoManager omanager = null;
	
	public static String OUTDIR_KEY = "-outdir";
	public static String USERNAME_KEY ="-username";
	public static String PASSWORD_KEY="-password";
	private static Admin me = null;
	private AdminGUI myGUI =null;
	
	private Admin() {
		cmanager = ContainerManager.getInstance();
		omanager = OntoManager.getInstance();

	}

	public static Admin getInstance()
	{
		if(me ==null)
			me = new Admin();
		return me;
	}
	public void setConfig(Config config) {
		this.config = config;
	}

	public boolean shutdownJadeContainer() {
		return cmanager.shutdownContainer();

	}

	//Refresh the agent lists
	public boolean refresh() {
		return true;
	}

	public boolean deployAgent(String name) {
		ciamas.wsjade.wsdl2jade.utils.WSAgent agent = new ciamas.wsjade.wsdl2jade.utils.WSAgent();

		if(cmanager.registerAgent(agent, name)!=null)
			return true;
		else 
			return false;
	}
	public boolean deployAgentManager()
	{
		if(cmanager.registerManagerAgent()!=null)		
			return true;
		else
			return false;
	}

	public boolean undeployAgent(String name) {
		cmanager.removeAgent(name);
		return true;
	}

	public DefaultMutableTreeNode getAgentNode(String agNodeName)
	{
		return myGUI.getAgentNode(agNodeName);
	}
	public boolean activate(String serviceName, String serviceURI,
			String agentName, boolean remote) {
		String ontoCls = null;
		try {

			omanager.setBases(config.getStubOutDir(), config.getJadeOutDir());
			ontoCls = omanager.deployOnto(serviceURI, agentName, serviceName, remote);
		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred",e);
		}

		if (ontoCls == null) {
			Logger.getLogger("WS2JADE").error("Error:  Cannot locate onto class");
			return false;
		}

		//Create new Object to send to the agent
		ciamas.wsjade.wsdl2jade.utils.AddOntAction addOntAction = new ciamas.wsjade.wsdl2jade.utils.AddOntAction();
		addOntAction.setOntClassName(ontoCls);
		addOntAction.setServiceName(serviceName);

		return cmanager.putObject(agentName, addOntAction);
	}

	public boolean remove(String serviceName, String agentName) {
		return true;
	}

	public boolean startupJadeContainerWithGUI(String platformName) {
		if (config == null) {
			config = new Config();
		}
		String jServer = config.getJadeSName();
		int jPort = config.getJadePort();
		boolean isMain = config.isMainContainer();
		cmanager.createAgentContainer(platformName,jServer, jPort, isMain);
		Admin.getInstance().deployAgentManager();
		return true;

	}
	public boolean startupJadeContainerWithoutGUI(String[] args)
	{
		return cmanager.createAgentContainer(args);
		
	}
	private void prepareConfig(String[] args)
	{
		config = new Config();
		 for (int i = 0; i < args.length; i++) {
		 	if(args[i].equals(OUTDIR_KEY))
		 	{
		 		config.setJadeOutDir(args[i+1]);
		 		config.setStubOutDir(args[i+1]);
		 	}
		 	else if(args[i].equals(USERNAME_KEY))
		 		Authorization.setUsername(args[i+1]);
		 	else if(args[i].equals(PASSWORD_KEY))
		 		Authorization.setPassword(args[i+1]);
		 }
	}
	private String[] prepareArgs(String[] args) {
		//Adopted from jade.Boot code
        boolean printUsageInfo = false;
        
        if ((args == null) || (args.length == 0)) {
            // printUsageInfo = true;
        } else {
            boolean isNew = false;
            boolean likely = false;
            for (int i = 0; i < args.length; i++) {
               
                if (args[i].startsWith("agents:")) {
                    isNew = true;
                } else
                if (args[i].startsWith("-") && likely) {
                    isNew = true;
                } 
            }

            if (isNew) {
                return args;
            }
        }

        int n = 0;
        boolean endCommand =
            false;    // true when there are no more options on the command line
        java.util.Vector results = new  java.util.Vector();
        Logger log = Logger.getLogger("WS2JADE");
        log.info("Start up the container");
        while ((n < args.length) &&!endCommand) {
            String theArg = args[n];

          if (theArg.equalsIgnoreCase("-host")) {
                if (++n == args.length) {
                    log.error("Missing host name ");

                    printUsageInfo = true;
                } else {
                    results.add("host:" + args[n]);
                }
            }else if (theArg.equalsIgnoreCase("-owner")) {
                if (++n == args.length) {

					// "owner:password" not provided on command line
					results.add("owner:" + ":");

                } else {
                    results.add("owner:" + args[n]);
                }
            } else if (theArg.equalsIgnoreCase("-name")) {
                if (++n == args.length) {
                    log.error("Missing platform name");

                    printUsageInfo = true;
                } else {
                    results.add("name:" + args[n]);
                }
            } else if (theArg.equalsIgnoreCase("-imtp")) {
                if (++n == args.length) {
                    log.error("Missing IMTP class");

                    printUsageInfo = true;
                } else {
                    results.add("imtp:" + args[n]);
                }
            } else if (theArg.equalsIgnoreCase("-port")) {
                if (++n == args.length) {
                    log.error("Missing port number");

                    printUsageInfo = true;
                } else {
                    try {
                        Integer.parseInt(args[n]);
                    } catch (NumberFormatException nfe) {
                        System.err.println("Wrong int for the port number");

                        printUsageInfo = true;
                    }

                    results.add("port:" + args[n]);
                }
            } else if (theArg.equalsIgnoreCase("-container")) {
                results.add(theArg);
	    } else if (theArg.equalsIgnoreCase("-backupmain")) {
		results.add(theArg);
            } else if (theArg.equalsIgnoreCase("-gui")) {
                results.add(theArg);
            
            } else if (theArg.equalsIgnoreCase("-help")
                       || theArg.equalsIgnoreCase("-h")) {
                results.add("-help");
            } else if (theArg.equalsIgnoreCase("-nomtp")) {
                results.add(theArg);
            } else if(theArg.equalsIgnoreCase("-nomobility")){
                results.add(theArg);
            } else if (theArg.equalsIgnoreCase("-mtp")) {
                if (++n == args.length) {
                    System.err.println("Missing mtp specifiers");

                    printUsageInfo = true;
                } else {
                    results.add("mtp:" + args[n]);
                }
            } else if (theArg.equalsIgnoreCase("-aclcodec")) {
                if (++n == args.length) {
                    System.err.println("Missing aclcodec specifiers");

                    printUsageInfo = true;
                } else {
                    results.add("aclcodec:" + args[n]);
                }
            } else if (theArg.startsWith("-") && n+1 < args.length) {
            	// Generic option
            	results.add(theArg.substring(1)+":"+args[++n]);
            } else {
                endCommand = true;    //no more options on the command line
            }

            n++;    // go to the next argument
        }    // end of while

        // all options, but the list of Agents, have been parsed
        if (endCommand) {    // parse the list of agents, now
            --n;    // go to the previous argument

            StringBuffer sb = new StringBuffer();

            for (int i = n; i < args.length; i++) {
                sb.append(args[i] + " ");
            }

            results.add("agents:" + sb.toString());
        }

        if (printUsageInfo) {
            results.add("-help");
        }

        String[] newArgs = new String[results.size()];

        for (int i = 0; i < newArgs.length; i++) {
            newArgs[i] = (String) results.elementAt(i);
        }

        return newArgs;
    }

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args)
	{
		boolean withGui = false;
		Admin admin = Admin.getInstance();
		admin.prepareConfig(args);
		String[] arg = admin.prepareArgs(args);
		
		 for (int i = 0; i < args.length; i++) {
		 	if(args[i].equals("-gui"))
		 	{
		 		runWithGUI();
		 		withGui =true;
		 		break;
		 	}
		 	
		 }
		 if(!withGui)
		 	runWithoutGUI(arg);
	}
	public AdminGUI getGUI()
	{
		return myGUI;
	}
	public void constructGUI()
	{
	
		myGUI = new AdminGUI();

		myGUI.setSize(420, 345);
		myGUI.setAdmin(this);

		Dimension d = null;
		// size of what we're positioning against
		Point p = null;

		d = Toolkit.getDefaultToolkit().getScreenSize();
		p = new Point();

		double centreX = p.getX() + d.getWidth() / 2;
		double centreY = p.getY() + d.getHeight() / 2;
		myGUI.getSize(d);
		p.setLocation(centreX - d.getWidth() / 2, centreY - d.getHeight() / 2);
		if (p.getX() < 0)
			p.setLocation(0, p.getY());
		if (p.getY() < 0)
			p.setLocation(p.getX(), 0);

		myGUI.setLocation(p);
		myGUI.setResizable(false);
		
		String displayText="	WS2JADE v1.0 start up\n" +
		"	Open Source, under LGPL restriction\n"+
		"	http://www.it.swin.edu.au/centres/ciamas/ws2jade\n";
		System.out.println(displayText);
		
	}
	public void setVisibility(boolean b)
	{
		if(b)
			myGUI.show();
		else
			myGUI.setVisible(false);
		
	}
	public static void runWithoutGUI(String[] args)
	{
		Admin admin = Admin.getInstance();
		//admin.prepareConfig(args);
		//String[] arg = admin.prepareArgs(args);
		admin.constructGUI();
		admin.setVisibility(false);
		admin.startupJadeContainerWithoutGUI(args);
		admin.deployAgentManager();
		
	}
	public static void runWithGUI() {

		Splash s = new Splash();
		s.pack();
		s.show();

		Admin admin = Admin.getInstance();
	
		try
		{
			Thread.currentThread().sleep(2000);
		}
		catch(Exception e){Logger.
			getLogger("WS2JADE").debug("Exception occurred", e);}
		s.dispose();
		admin.constructGUI();
		admin.setVisibility(true);
		//We only deploy agents when JADE starts up
		//admin.deployAgentManager();
	

	}
	

}