package ciamas.wsjade.management;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import javax.swing.JButton;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;

/**
* This code was generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a
* for-profit company or business) then you should purchase
* a license - please visit www.cloudgarden.com for details.
*/
//TODO: Log4j 
public class NewServiceFrame extends javax.swing.JFrame  implements
java.awt.event.ActionListener{

	private JButton cancelBtn;
	private JLabel jLabel1;
	private JTextField serviceNameTf;
	private JButton okBtn;
	private JTextField agentNameTf;
	private JTextField uriTf;
	private JLabel agentNameLbl;
	private JLabel sNameLbl;
	private JLabel uriLbl;
	private JPanel mainPanel;
	private RemoteAgentManager mgr;
	public NewServiceFrame() {
		initGUI();
	}

	/**
	* Initializes the GUI.
	* Auto-generated code - any changes you make will disappear.
	*/
	public void initGUI(){
		try {
			preInitGUI();
	
			mainPanel = new JPanel();
			uriLbl = new JLabel();
			sNameLbl = new JLabel();
			agentNameLbl = new JLabel();
			uriTf = new JTextField();
			agentNameTf = new JTextField();
			okBtn = new JButton();
			serviceNameTf = new JTextField();
			jLabel1 = new JLabel();
			cancelBtn = new JButton();
	
			BorderLayout thisLayout = new BorderLayout();
			this.getContentPane().setLayout(thisLayout);
			thisLayout.setHgap(0);
			thisLayout.setVgap(0);
			this.setSize(new java.awt.Dimension(323,187));
	
			mainPanel.setLayout(null);
			mainPanel.setPreferredSize(new java.awt.Dimension(368,194));
			this.getContentPane().add(mainPanel, BorderLayout.CENTER);
	
			uriLbl.setText("Service URI");
			uriLbl.setPreferredSize(new java.awt.Dimension(71,24));
			uriLbl.setBounds(new java.awt.Rectangle(14,26,71,24));
			mainPanel.add(uriLbl);
	
			sNameLbl.setText("Service Name");
			sNameLbl.setPreferredSize(new java.awt.Dimension(81,20));
			sNameLbl.setBounds(new java.awt.Rectangle(11,61,81,20));
			mainPanel.add(sNameLbl);
	
			agentNameLbl.setText("Agent Name");
			agentNameLbl.setPreferredSize(new java.awt.Dimension(85,20));
			agentNameLbl.setBounds(new java.awt.Rectangle(18,91,85,20));
			mainPanel.add(agentNameLbl);
	
			uriTf.setPreferredSize(new java.awt.Dimension(195,21));
			uriTf.setBounds(new java.awt.Rectangle(101,28,195,21));
			mainPanel.add(uriTf);
	
			agentNameTf.setPreferredSize(new java.awt.Dimension(194,22));
			agentNameTf.setBounds(new java.awt.Rectangle(105,89,194,22));
			mainPanel.add(agentNameTf);
	
			okBtn.setText("OK");
			okBtn.setPreferredSize(new java.awt.Dimension(83,20));
			okBtn.setBounds(new java.awt.Rectangle(103,126,83,20));
			mainPanel.add(okBtn);
	
			serviceNameTf.setPreferredSize(new java.awt.Dimension(194,20));
			serviceNameTf.setBounds(new java.awt.Rectangle(102,57,194,20));
			mainPanel.add(serviceNameTf);
	
			jLabel1.setText("Add New Service");
			jLabel1.setPreferredSize(new java.awt.Dimension(141,20));
			jLabel1.setBounds(new java.awt.Rectangle(151,2,141,20));
			mainPanel.add(jLabel1);
	
			cancelBtn.setText("Cancel");
			cancelBtn.setPreferredSize(new java.awt.Dimension(88,20));
			cancelBtn.setBounds(new java.awt.Rectangle(201,127,88,20));
			mainPanel.add(cancelBtn);
	
			postInitGUI();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/** Add your pre-init code in here 	*/
	public void preInitGUI(){
	}

	/** Add your post-init code in here 	*/
	public void postInitGUI(){
		okBtn.addActionListener(this);
		cancelBtn.addActionListener(this);
	}
	public void setManager(RemoteAgentManager manager)
	{
		mgr=manager ;
	}

	/** Auto-generated main method */
	public static void main(String[] args){
		showGUI();
	}

	/**
	* This static method creates a new instance of this class and shows
	* it inside a new JFrame, (unless it is already a JFrame).
	*
	* It is a convenience method for showing the GUI, but it can be
	* copied and used as a basis for your own code.	*
	* It is auto-generated code - the body of this method will be
	* re-generated after any changes are made to the GUI.
	* However, if you delete this method it will not be re-created.	*/
	public static void showGUI(){
		try {
			NewServiceFrame inst = new NewServiceFrame();
			inst.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub
		if(e.getSource() == okBtn)
			mgr.requestNewService(uriTf.getText().trim(),
					agentNameTf.getText().trim(), serviceNameTf.getText().trim());

		else if(e.getSource() == cancelBtn)
		{
			uriTf.setText("");
			agentNameTf.setText("");
			serviceNameTf.setText("");
		}
		this.setVisible(false);

	}
}
