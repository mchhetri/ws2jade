/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.management;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class ASWSRecord {

	private String wsURL = null;

	private String agentService = null;

	public ASWSRecord(String a, String w) {
		wsURL = w;
		agentService = a;
	}

	/**
	 * @return Returns the agentService.
	 */
	public String getAgentService() {
		return agentService;
	}

	/**
	 * @param agentService
	 *            The agentService to set.
	 */
	public void setAgentService(String agentService) {
		this.agentService = agentService;
	}

	/**
	 * @return Returns the wsURL.
	 */
	public String getWsURL() {
		return wsURL;
	}

	/**
	 * @param wsURL
	 *            The wsURL to set.
	 */
	public void setWsURL(String wsURL) {
		this.wsURL = wsURL;
	}
}