/*
 * Created on 4/02/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ciamas.wsjade.management;

import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.basic.Action;
import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

import javax.swing.JOptionPane;
import org.apache.log4j.Logger;
/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class RemoteAgentManager  extends Agent {

	// CREATE THE ONTOLOGY INSTANCE
	 private ContentManager manager  = (ContentManager) getContentManager();
     private Codec codec = new SLCodec();
     private Ontology ontology = ciamas.wsjade.wsdl2jade.onto.WebAdminOntology.getInstance();
     private ciamas.wsjade.management.utils.Admin admin = ciamas.wsjade.management.utils.Admin.getInstance();
	 private String uname = null;
	 private String pword=null;
	 private RemoteAgentManagerGUI gui = new RemoteAgentManagerGUI();
        protected void setup() {

		// REGISTER THE ONTOLOGY
		manager.registerLanguage(codec);
		manager.registerOntology(ontology);

		// ADD HANDLEINFORMBEHAVIOUR
		addBehaviour(new HandleInformBehaviour(this));
		gui.setManager(this);
		showGUI();
	   }
        public void showGUI(){
    		try {
    			
    			gui.setVisible(true);
    		} catch (Exception e) {
    			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
    			//e.printStackTrace();
    		}
    	}
	   protected void setAuthen(String u, String p)
	   {
	   	uname = u;
	   	pword =p;
	   }
        protected void requestNewAgent(String aName)
        {
        	// CREATE A REQUEST MESSAGE
    		ACLMessage requestMsg = new ACLMessage(ACLMessage.REQUEST); 		
    		requestMsg.setSender(getAID());

    		// IDENTIFY THE WEB SERVICE AGENT 
    		requestMsg.addReceiver(new AID("WSManager", false)); 
    		requestMsg.setLanguage(codec.getName());
    		requestMsg.setOntology(ontology.getName());
  
    		ciamas.wsjade.wsdl2jade.onto.AgentDeploymentRequest requestAction = 
    		new ciamas.wsjade.wsdl2jade.onto.AgentDeploymentRequest();
    		requestAction.setAgentName(aName);
    		requestAction.setUsername(uname);
    		requestAction.setPassword(pword);
   
    	     try 
    	     { 
    			// SEND THE MESSAGE
    			manager.fillContent( requestMsg, 
    					new Action(new AID("RemoteAgentManager", false), requestAction));
    					
    			send(requestMsg);
    		
    	     } 
    	     catch(Exception e) 
    	     { Logger.getLogger("WS2JADE").debug("Exception occurred", e);
    	     //e.printStackTrace(); }
    	     }
        	
        }
        protected void requestNewService(String uri, String aName, String sName)
        {
        	//        	 CREATE A REQUEST MESSAGE
    		ACLMessage requestMsg = new ACLMessage(ACLMessage.REQUEST); 		
    		requestMsg.setSender(getAID());

    		// IDENTIFY THE WEB SERVICE AGENT 
    		requestMsg.addReceiver(new AID("WSManager", false)); 
    		requestMsg.setLanguage(codec.getName());
    		requestMsg.setOntology(ontology.getName());
  
    		ciamas.wsjade.wsdl2jade.onto.ServiceDeploymentRequest requestAction = 
    		new ciamas.wsjade.wsdl2jade.onto.ServiceDeploymentRequest();
    		requestAction.setAgentName(aName);
    		requestAction.setServiceName(sName);
    		requestAction.setUri(uri);
    		requestAction.setUsername(uname);
    		requestAction.setPassword(pword);
   
    	     try 
    	     { 
    			// SEND THE MESSAGE
    			manager.fillContent( requestMsg, 
    					new Action(new AID("RemoteAgentManager", false), requestAction));
    					
    			send(requestMsg);
    		
    	     } 
    	     catch(Exception e) 
    	     {
    	     	Logger.getLogger("WS2JADE").debug("Exception occurred", e);
    	     	//e.printStackTrace(); 
    	     	}
        }
     class HandleInformBehaviour extends CyclicBehaviour
     {
        public HandleInformBehaviour(Agent a) { super(a); }
        
        public void action() 
        {
            ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));
            if (msg != null) 
            {
             try 
             {
                ContentElement ce = manager.extractContent(msg);
					
				if (ce instanceof jade.content.onto.basic.Result) 
				{
					jade.content.onto.basic.Result result = (jade.content.onto.basic.Result)ce;
					System.out.println();
					System.out.print("Returned value: ");
					ciamas.wsjade.wsdl2jade.onto.DeploymentResponse	ret =
						
					(ciamas.wsjade.wsdl2jade.onto.DeploymentResponse)result.getValue();
					System.out.println(ret.getSuccess());
					String retMsg = null;
					if(ret.getSuccess())
						retMsg ="Deployment Successful!";
					else
						retMsg ="Deployment Failed";
						
					JOptionPane.showMessageDialog(new javax.swing.JFrame(), retMsg);
					 
					
		
				} 
		
               } catch(Exception e) {
               	Logger.getLogger("WS2JADE").debug("Exception occurred", e);
               	//e.printStackTrace();
               	}
            } else { block(); }
    	} 
    } 
} 




