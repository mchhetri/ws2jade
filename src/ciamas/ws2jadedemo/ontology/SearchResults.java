/*
 * SearchResults.java
 *
 * Created on 2 February 2005, 13:09
 */

package ciamas.ws2jadedemo.ontology;

import jade.content.*;
import jade.util.leap.ArrayList;

/**
 *
 * @author  403515
 */
public class SearchResults implements Concept {
    
    private ArrayList results = null;
    
    /** Creates a new instance of SearchResults */
    public SearchResults() {
    }
    
    public void setResults(ArrayList results) {
        this.results = results;
    }
    
    public ArrayList getResults() {
        return results;
    }
    
}
