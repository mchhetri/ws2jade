/*
 * Search.java
 *
 * Created on 31 January 2005, 15:49
 */

package ciamas.ws2jadedemo.ontology;

import jade.content.*;

/**
 *
 * @author  403515
 */
public class Search implements Concept {
    
    private String keyword = null;
    private String mode = "music";
    
    /** Creates a new instance of Search */
    public Search() {
    }
    
    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }
    
    public String getKeyword() {
        return keyword;
    }
    
    public void setMode(String mode) {
        this.mode = mode;
    }
    
    public String getMode() {
        return mode;
    }
    
}
