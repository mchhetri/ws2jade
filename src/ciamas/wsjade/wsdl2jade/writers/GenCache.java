/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Vector;

import org.apache.axis.wsdl.symbolTable.Parameter;
import org.apache.axis.wsdl.symbolTable.Parameters;
import org.apache.axis.wsdl.toJava.Utils;

import ciamas.wsjade.management.Status;
import ciamas.wsjade.management.utils.Utility;
import ciamas.wsjade.wsdl2jade.mapping.TypeMapping;
import ciamas.wsjade.wsdl2jade.utils.BeanUtils;
import ciamas.wsjade.wsdl2jade.utils.Common;
import ciamas.wsjade.wsdl2jade.utils.PrintWriterUtils;
import org.apache.log4j.Logger;
//Multi-purpose class to cache different unrelated things

public class GenCache {
	public static int IN_TYPE = 0;

	public static int OUT_TYPE = 1;

	//For all special mapping, only enum at this stage
	public static int ENUM_MAPPING = 0;
	
	public static String ExecFilePrefix ="exec";

	//For converter obj name in printing file
	private static String inObj = "inObj";

	private static String outObj = "outObj";

	private OntParser ontParser = null;

	private static GenCache me = null;

	private Vector cache = new Vector();

	//Hashmap of normal class and attributes/types
	private HashMap map = new HashMap();

	//Hashmap of special class mapping - only Enumeration at this stage
	private HashMap enumMap = new HashMap();

	//Hashmap of operation and attributes/types
	private HashMap optMap = new HashMap();

	//Hashmap of portName/porttypeName
	private HashMap portMap = new HashMap();

	//Cache of WSDL2JAVA generated service
	private String stubServiceClass = null;

	//Singleton pattern
	private GenCache() {
	}

	public static GenCache getInstance() {
		if (me == null)
			me = new GenCache();
		return me;
	}

	//Clear all cache data 
	public void clear() {
		enumMap.clear();
		map.clear();
		optMap.clear();
		portMap.clear();
		cache.clear();
	}

	//Get and set the WSDL2JAVA stub service class
	public String getStubServiceClass() {
		return this.stubServiceClass;
	}

	public void setStubServiceClass(String sName) {
		this.stubServiceClass = sName;
	}

	private void generateStubLocatorMethod(PrintWriterUtils out) {
		out.setTabNum(1);
		out.println("public static String getStubServiceClass()");
		out.println("{");
		out.setTabNum(2);
		out.println("return \"" + stubServiceClass + "\";");
		out.setTabNum(1);
		out.println("}");

	}

	public void setParser(OntParser ps) {
		this.ontParser = ps;
	}

	public OntParser getParser() {
		return ontParser;
	}

	//Check if a message is already generated with certain type
	public boolean existing(int t, String msg) {
		for (int i = 0; i < cache.size(); i++) {
			MessageCache msgCache = (MessageCache) cache.elementAt(i);
			if ((msgCache.getType() == t)
					&& (msgCache.getMessage().equals(msg)))
				return true;
		}
		return false;
	}

	public void record(int t, String msg) {
		MessageCache msgCache = new MessageCache(t, msg);
		cache.addElement(msgCache);

	}

	public void record(String jadeType, String axisType, int specialMappingCode) {
		if (specialMappingCode == ENUM_MAPPING) {
			enumMap.put(jadeType, axisType);

		}
	}

	public void record(String portName, String porttypeName) {
		Vector v = null;
		for (Iterator it = portMap.keySet().iterator(); it.hasNext();) {
			String ptName = (String) it.next();
			if (ptName.equals(porttypeName)) {
				if (portMap.get(ptName) != null)
					v = (Vector) portMap.get(ptName);
				break;

			}
		}
		if (v == null)
			v = new Vector();
		v.addElement(portName);

		portMap.put(porttypeName, v);

	}

	public void record(String portClsName, String optName, String pkg,
			Parameters params) {
		Vector value = null;
		//boolean hasCls = false;
		for (Iterator it = optMap.keySet().iterator(); it.hasNext();) {
			String pCls = (String) it.next();
			if (pCls.equals(portClsName)) {
				if (optMap.get(pCls) != null)
					value = (Vector) optMap.get(pCls);
				break;

			}
		}
		if (value == null)
			value = new Vector();

		//Note that some other bindings may be missing in this cache
		//since the proxy agent does not have to care about which binding to
		// choose from
		value.addElement(new OperationCache(optName, pkg, params));
		optMap.put(portClsName, value);

	}

	public String getPortCls(String portName) {
		for (Iterator it = optMap.keySet().iterator(); it.hasNext();) {
			String pCls = (String) it.next();
			if (pCls.endsWith("." + portName)) {
				return pCls;

			}
		}
		return null;

	}

	public void record(String cls, String xmlName, String javaName,
			String javaType, String jadeType) {
		Vector value = null;
		//boolean hasCls = false;
		for (Iterator it = map.keySet().iterator(); it.hasNext();) {
			String clName = (String) it.next();
			if (cls.equals(clName)) {
				value = (Vector) map.get(cls);
				//hasCls = true;
				break;

			}
		}
		if (value == null)
			value = new Vector();

		value
				.addElement(new NameMapping(xmlName, javaName, javaType,
						jadeType));
		map.put(cls, value);
	}

	public String getJavaName(String cls, String xmlName) {
		Vector v = (Vector) map.get(cls);
		if (v == null)
			return null;
		else {
			for (int i = 0; i < v.size(); i++) {
				NameMapping n = (NameMapping) v.elementAt(i);
				if (n.getXmlName().equals(xmlName)) {
					return n.getJavaName();
				}
			}
		}
		return null;
	}

	public Vector getMappingForClass(String cls) {
		Vector v = (Vector) map.get(cls);
		return v;
	}

	public String getXmlName(String cls, String javaName) {
		Vector v = (Vector) map.get(cls);
		if (v == null)
			return null;
		else {
			for (int i = 0; i < v.size(); i++) {
				NameMapping n = (NameMapping) v.elementAt(i);
				if (n.getJavaName().equals(javaName)) {
					return n.getXmlName();
				}
			}
		}
		return null;

	}

	public void generate() {
		try {
			generateTypeConverter();
			generateCompileFile();
		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
		}

	}

	public void generateCompileFile() throws Exception {
		//Only do this after the java generation has finished !
		boolean code_generation_finished = Status.getStatus(Common
				.getServiceURL(), Status.JAR_STUB_GENERATION_PARAM);
		/*
		 * while(!code_generation_finished) {
		 * 
		 * System.out.println("Waiting for stub code to finish!");
		 * code_generation_finished =
		 * Status.getStatus(Common.getServiceURL(),Status.JAR_STUB_GENERATION_PARAM); }
		 */
		String mappingDir = Common.getJadeOutputDir() + File.separatorChar
				+ Common.getJadeGenRootPkg() + File.separatorChar + "mapping"
				+ File.separatorChar + Common.getServiceName()
				+ File.separatorChar;
		mappingDir = (new File(mappingDir)).getCanonicalPath();

		String mappingFiles = mappingDir + File.separatorChar + "*.java";
		String serviceStubFile = getStubServiceClass().replace('.',
				File.separatorChar);
		
		String ontoDir =Common.getJadeOutputDir() + File.separatorChar
		+ Common.getJadeGenRootPkg() + File.separatorChar + "onto"
		+ File.separatorChar + Common.getServiceName()
		+ File.separatorChar;
        ontoDir = (new File(ontoDir)).getCanonicalPath();
        String ontoFiles =ontoDir + File.separatorChar + "*.java";

		String serviceStubDir = Common.getJaxOutputDir();
		//System.out.println(serviceStubDir);
		serviceStubFile = (new File(serviceStubDir)).getCanonicalPath()
				+ File.separatorChar + serviceStubFile + "Locator.java";
		
		//Way around to get special character % or $
		char envPrefix = Utility.getSystemCommand(Utility.ENV_VAR_PREFIX).charAt(0);
		try {

			String jadeCls = (new File(Common.getJadeOutputDir()))
					.getCanonicalPath();
			String jaxCls = (new File(Common.getJaxOutputDir()))
					.getCanonicalPath();

			//File file = new File(jadeCls + File.separatorChar
			//		+ Common.getServiceName()
			File file = new File("." + File.separatorChar + ExecFilePrefix
					+ Utility.getSystemCommand(Utility.SCRIPT_EXTENSTION));
			File parent = new File(file.getParent());
			if (!parent.exists())
				parent.mkdirs();
			PrintWriterUtils out = new PrintWriterUtils(new FileWriter(file, false));
			
			String clsPath="";
			if(Utility.getOSType() == Utility.WINDOW_OS)
			{
				out.println("@echo off");
				clsPath = "set CLASSPATH=" + envPrefix + "CLASSPATH"
				+ envPrefix + File.pathSeparatorChar
				+ Utility.generateClsEnv() + File.pathSeparatorChar
				+ jadeCls;
			}
			else
			{
				out.println("#!"+ Utility.getBourneShell());
				out.println();
				out.println("set +v");
				clsPath ="export CLASSPATH=$CLASSPATH" +File.pathSeparatorChar
				+ Utility.generateClsEnv() + File.pathSeparatorChar
				+ jadeCls;
			}

			
			if (!jadeCls.equals(jaxCls))
				clsPath = clsPath + File.pathSeparatorChar + jaxCls;

			//clsPath = clsPath + ";";
			out.println(clsPath);

			out.println("javac " + mappingFiles);
			out.println("javac " + ontoFiles);
			out.println("javac " + serviceStubFile);

			//Create jar file
			out.println("cd " + jadeCls);
			out.println("jar cvf " + "Onto.jar " + Common.getJadeGenRootPkg());

			out.println("cd " + jaxCls);

			out.println("jar cvf " + "Stub.jar "
					+ Utility.getPkgRoot(jaxCls, Common.getJadeGenRootPkg()));

			out.close();

		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
		}
	}

	//From here forward, it's getting complex, may break down in next version
	public void generateTypeConverter() {
		String pck = Common.getJadeGenRootPkg() + ".mapping."
				+ Common.getServiceName();
		String pckDir = pck.replace('.', File.separatorChar);
		try {
			String outDir = Common.getJadeOutputDir();

			String outputDir = outDir + File.separatorChar + pckDir;

			File file = new File(outputDir + File.separatorChar
					+ "JavaTypeMapping.java");
			File parent = new File(file.getParent());
			if (!parent.exists())
				parent.mkdirs();
			PrintWriterUtils out = new PrintWriterUtils(new FileWriter(file));

			Common.printBanner(out);
			out.println();
			out.println("package " + pck + ";");
			out.println();
			out.println("import jade.util.leap.ArrayList;");
			out.println("import java.rmi.RemoteException;");
			out.println("import java.util.Vector;");
			out.println("import ciamas.wsjade.wsdl2jade.mapping.TypeMapping;");
			out.println("import ciamas.wsjade.wsdl2jade.mapping.JavaTypeConverter;");
		
			out.println();
			out.println("public class JavaTypeMapping extends ciamas.wsjade.wsdl2jade.mapping.AxisTypeMapping");
			out.println("{");
			out.println();

			generateStubLocatorMethod(out);
			generateAnyPortMethod(out);
			generateTypeConverter(out);
			generateEnumTypeConverter(out);
			generateOperationCoverter(out);
			generateArrayList2JadeArrayList(out);
			generateVector2JadeArrayList(out);
			generateJadeArrayList2Vector(out);
			generateJadeArrayList2ArrayList(out);
			out.setTabNum(0);
			out.println("}");

			out.close();

		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
		}

	}

	private void generateAnyPortMethod(PrintWriterUtils out) throws Exception {
		out.setTabNum(1);
		out.println("public static String getAnyPortName(String pTypeName)");
		out.println("{");
		out.setTabNum(2);
		Vector v = new Vector();
		for (Iterator it = portMap.keySet().iterator(); it.hasNext();) {
			String ptName = (String) it.next();
			out.println("if(pTypeName.equals(\"" + ptName + "\"))");

			if (portMap.get(ptName) != null)
				v = (Vector) portMap.get(ptName);
			if (v.size() > 0)
				out.println("return \"" + v.elementAt(0) + "\";");

		}
		out.println("return null;");
		out.setTabNum(1);
		out.println("}");

	}

	//This method generate signature and enclosed brackets for a certain invoke
	//method inside the generated file
	private void generateOperationCoverter(PrintWriterUtils out)
			throws Exception {
		Vector value = null;
		Vector done = new Vector();

		for (Iterator it = optMap.keySet().iterator(); it.hasNext();) {
			String pClsName = (String) it.next();
			value = (Vector) optMap.get(pClsName);
			for (int i = 0; i < value.size(); i++) {
				OperationCache oper = (OperationCache) value.elementAt(i);
				String optName = oper.getOptName();
				Parameters params = oper.getParams();

				//Find the name of Concept and Action
				String actionCls = oper.getJadePkg() + ".Request." + BeanUtils.upperFirstLetter(optName);
				String conceptCls = oper.getJadePkg() + ".Response." + BeanUtils.upperFirstLetter(optName);

				boolean repeat = false;
				for(int l=0;l<done.size();l++)
					if(conceptCls.equals(done.elementAt(l)))
					{
						repeat = true;
						break;
					}
				if(repeat)
					continue;
				done.addElement(conceptCls);
				//Print out the header of this method
				out.setTabNum(1);
				out.println("public static " + conceptCls + " invoke("
						+ pClsName + "  portObj, " + actionCls + " " + inObj
						+ ") throws java.rmi.RemoteException");
				out.println("{");
				out.setTabNum(2);
				//out.println(conceptCls + " outObj = new " + conceptCls
				// +"();");
				writeOperationConverter(out, optName, outObj, "portObj", inObj,
						params, conceptCls);
				out.setTabNum(1);
				out.println("}");

			}

		}

	}

	//This method find out the appropriate mode of the parameters inside the
	// method
	//to assign them appropriately to Concept or Action objects
	private void writeOperationConverter(PrintWriterUtils out, String optName,
			String conceptObj, String portObj, String actionObj,
			Parameters parms, String conceptCls) throws Exception {
		out.setTabNum(2);

		String ret = null;
		String retType = null;
		String retJadeType = null;
		Vector inputParams = new Vector();
		boolean finished = false;

		for (int i = 0; parms != null && i < parms.list.size(); ++i) {
			Parameter p = (Parameter) parms.list.get(i);
			String varName = Utils.xmlNameToJava(p.getName());
			varName = BeanUtils.lowerFirstLetter(varName);

			String jaxType = Utils.getParameterTypeName(p);
			String jaxHolderType = Utils.holder(p.getType(),
					ontParser);
			String jadeType = TypeMapping.getJadeDataType(jaxType, false);

			if (p.getMode() == Parameter.IN) {
				inputParams.addElement(varName);
				//Jade2Jax
				writeOperationConverter(out, varName, jadeType, jaxType, false, false); 

			} else if (p.getMode() == Parameter.INOUT) {
				inputParams.addElement(varName + "Holder");
				//Jade2Jax
				writeOperationConverter(out, varName, jadeType, jaxType, false, false); 
				out.println(jaxHolderType + " " + varName + "Holder= new "
						+ jaxHolderType + "(" + varName + ");");

			} else {
				inputParams.addElement(varName + "Holder");
				out.println(jaxHolderType + " " + varName + "Holder= new "
						+ jaxHolderType + "();");

			}
		}
		//We now construct a Concept object for return
		out.println();
		out.println("//Create a new response ");
		out.println(conceptCls + " " + conceptObj + "=new " + conceptCls
				+ "();");

		String methodInvoke = "";
		if (parms != null && parms.returnParam != null) {
			Parameter p = parms.returnParam;
			retType = Utils.getParameterTypeName(p);
			retJadeType = TypeMapping.getJadeDataType(retType, false);

			ret = Utils.xmlNameToJava(p.getName());
			ret = BeanUtils.lowerFirstLetter(ret);

			methodInvoke = retType + " " + ret + " = ";

		}

		//Printout the invocation
		methodInvoke = methodInvoke + "portObj." + optName + "(";
		int k = 0;
		for (k = 0; k < inputParams.size() - 1; k++) {
			methodInvoke = methodInvoke + inputParams.elementAt(k) + ", ";
		}
		if (inputParams.size() > 0)
			methodInvoke = methodInvoke + inputParams.elementAt(k) + ");";
		else
			methodInvoke = methodInvoke + ");";

		out.println();
		out.println("//Do invocation on the stub");
		out.println(methodInvoke);

		if (ret != null) {
			//Convert the return type into jade type store in jade_ret
			//writeInOutConverting(out, ret, "jade_"+ ret, retType,
			// retJadeType);

			if (!retType.equals(retJadeType)) {

				writeOperationConverter(out, ret, "jade_" + ret, retType,
						retJadeType, true, true); //From Jax to Jade
				out.println(outObj + ".set" + BeanUtils.upperFirstLetter(ret)
						+ "(jade_" + ret + ");");
			} else
				out.println(outObj + ".set" + BeanUtils.upperFirstLetter(ret)
						+ "(" + ret + ");");

		}

		for (int i = 0; parms != null && i < parms.list.size(); ++i) {

			Parameter p = (Parameter) parms.list.get(i);

			if (p.getMode() != Parameter.IN) {

				String varName = Utils.xmlNameToJava(p.getName());
				varName = BeanUtils.lowerFirstLetter(varName);

				String jaxType = Utils.getParameterTypeName(p);
				String jaxHolderType = Utils.holder( p
						.getType(), ontParser);
				String jadeType = TypeMapping.getJadeDataType(jaxType, false);

				if (!jaxType.equals(jadeType)) {

					out.println(jaxType + " jade_" + varName + "=" + varName
							+ "Holder.value;");
					writeOperationConverter(out, "jade_" + varName, "axis_"+varName,
							jaxType, jadeType, true, true); 
					out.println(outObj + ".set"
							+ BeanUtils.upperFirstLetter(varName) + "("
							+ "axis_"+varName + ");");
				} else
					out.println(outObj + ".set"
							+ BeanUtils.upperFirstLetter(varName) + "("
							+ varName + "Holder.value);");

			}

		}
		out.println("return " + outObj + ";");

	}

	//This method is at the lowest level to convert between JAX and JADE type
	// inside a
	//generated invoke method. Data being processed here is the message parts
	private void writeOperationConverter(PrintWriterUtils out, String inName,
			String outName, String inType, String outType, boolean b, boolean invocationBody)
			throws IOException {
//		JavaBean standard use isVar instead of getVar for boolean type
		// parameters
		String get = null;
		Logger log = Logger.getLogger("WS2JADE");
		//Jax2Jade
		if (b && (inType.endsWith(".Boolean") || inType.equals("boolean"))) 
		{
			get = ".is";
		} else
			get = ".get";

		String getMethodStr = inObj + get + BeanUtils.upperFirstLetter(outName);
	
		
		out.setTabNum(2);
		if (inType.equals(outType)) //In type, out type are the same
		{
			out.println(outType + " " + outName + " = " + getMethodStr + "();");
		} 
		else if (TypeMapping.isPrimitive(inType)) 
		{
			if (!TypeMapping.isPrimitive(outType)) 
			{
				log.info(inType + " to " + outType);
				log.error("Rules violated: "+inType+"->" +outType);
				return;
			}

			out.println(outType + " " + outName + "=(" + outType + ")"
					+ getMethodStr + "();");

		} 
		else if (TypeMapping.isPrimitiveObject(inType)) 
		{
			out.println(outType + " " + outName + "=JavaTypeConverter.convert("
					+ getMethodStr + "(),\"" + outType + "\");");

		} 
		else if (TypeMapping.isArray(inType)) 
		{

			String baseInType = inType.substring(0, inType.indexOf("["));
			String baseOutType = TypeMapping.getJadeDataType(baseInType, false);
			out.println(outType + " " + outName + " = new " + outType + "();");
			if(!invocationBody)
				out.println(inType + " " + inName + "=" + getMethodStr + "();");
			out.println("for(int i=0;i<" + inName + ".length;i++)");
			out.println("{");
			out.setTabNum(3);
			if (TypeMapping.isPrimitive(baseInType)) 
			{
				String wrapperOutType = TypeMapping.getWrapperName(baseOutType);
				out.println(wrapperOutType + " ele = new " + wrapperOutType
						+ "((" + baseOutType + ")" + inName + "[i]);");
				if(outType.equals("jade.util.leap.ArrayList"))
					out.println(outName + ".add(ele);");
				else
					log.error("Rules violated: "+baseInType+"->"+baseOutType);

			}
			else
			{

				out.println(baseInType + " eleIn = " + inName + "[i];");
				writeInOutConverting(out, "ele", baseInType, baseOutType);
				if(outType.equals("jade.util.leap.ArrayList"))
					out.println(outName + ".add(eleOut);");
				else
					log.error("Rules violated: "+baseInType+"->"+baseOutType);
			}
			out.setTabNum(2);
			out.println("}");

		}

		else if (inType.equals("jade.util.leap.ArrayList") && TypeMapping.isArray(outType)) 
		{
			
			String baseOutType = outType.substring(0, outType.indexOf("["));
			String baseInType  =TypeMapping.getJadeDataType(baseOutType, false);
			
		
			if(!invocationBody)
				out.println(inType + " " + inName + "=" + getMethodStr + "();");
			out.println("if(" + inName + "!=null){");
			
			out.println(outType +" "+outName + "= new " + baseOutType + "[" + inName
					+ ".size()];");
			
			out.println("for(int i=0;i<" + inName + ".size();i++)");
			out.println("{");
			out.setTabNum(3);
			if (TypeMapping.isPrimitive(baseInType)) 
			{
				String wrapperInType = TypeMapping.getWrapperName(baseInType);
				out.println(wrapperInType + " ele = (" + wrapperInType + ")"
						+ inName + ".get(i);");
				
					out.println(baseOutType + " value = (" + baseOutType + ")ele."
						+ baseInType + "Value();");
				
					out.println(outName + "[i]=value");
				

			} 
			else 
			{
				out.println(baseInType + " eleIn = ");
				out.println("		(" + baseInType + ")" + inName + ".get(i);");
				
				writeInOutConverting(out, "ele", baseInType, baseOutType);
				out.println(outName + "[i] = eleOut;");
				
			}
			out.setTabNum(2);
			out.println("}");
			out.println("}");

		} 
		else if (inType.equals("jade.util.leap.ArrayList") && (!TypeMapping.isArray(outType))) 
		{
			//	Here is where the complication come, we may have input and output
			//are collections and an inspection of the class type is required
			//out.println("//"+inType + " " + inName + "In =" + getMethodStr + "();");
			if(outType.equals("java.util.Vector"))
				out.println(outType + " " + outName + "= JadeArrayList2Vector(" + inName + " );");
			else if(outType.equals("java.util.ArrayList"))
				out.println(outType + " " + outName + "= JadeArrayList2ArrayList(" + inName + " );");
			else
				out.println("Rules violated :"+inType+"->"+outType);
					
		}
		else if(inType.equals("java.util.Vector"))
		{
			//out.println(inType + " " + inName + "In =" + getMethodStr + "();");
			if(outType.equals("jade.util.leap.ArrayList"))
			out.println(outType + " " + outName + "= Vector2JadeArrayList(" + inName + " );");
			else
				out.println("Rules violated :"+inType+"->"+outType);
			
		}
		else if(inType.equals("java.util.ArrayList"))
		{
			//out.println("//"+inType + " " + inName + "="+ getMethodStr + "();");
			if(outType.equals("jade.util.leap.ArrayList"))
			out.println(outType + " " + outName + "= ArrayList2JadeArrayList(" + inName + " );");
			else
				out.println("Rules violated :"+inType+"->"+outType);
			
		}
		else //Must be a generated type
		{
			if(!invocationBody)
				out.println(inType + " " + outName + "In =" + getMethodStr + "();");
			if(inType.equals("ciamas.wsjade.wsdl2jade.onto.Unknown"))
				out.println(outType + " " + outName + "= new " +outType + "();");
			else
			out.println(outType + " " + outName + "= convert(" +inName + " );");

		}
		
	}

	//JAX-JADE WSDL message part conversion
	private void writeOperationConverter(PrintWriterUtils out, String varName,
			String inType, String outType, boolean b, boolean invocationBody) throws IOException {
		writeOperationConverter(out, varName +"In",
				varName, inType, outType, b, invocationBody)	;
	}

	private void generateEnumTypeConverter(PrintWriterUtils out)
			throws IOException {
		for (Iterator it = enumMap.keySet().iterator(); it.hasNext();) {
			String jadeCls = (String) it.next();
			String axisCls = TypeMapping.getComplexJaxType(jadeCls);
			String axisBaseCls = (String) enumMap.get(jadeCls);
			String jadeBaseCls = TypeMapping
					.getJadeDataType(axisBaseCls, false);

			//Jade to Axis
			out.setTabNum(1);
			out.println("public static " + axisCls + " convert(" + jadeCls
					+ " " + inObj + ")");
			out.println("{");
			out.setTabNum(2);
			//out.println("if(" + inObj + "==null)");
			//out.println("return new "+ axisCls +"();");
			out.println("return " + axisCls + ".fromValue("+inObj+".getEnumEle());");
			out.setTabNum(1);
			out.println("}");

			//Axis to Jade
			out.setTabNum(1);
			out.println("public static " + jadeCls + " convert(" + axisCls
					+ " " + inObj + ")");
			out.println("{");
			out.setTabNum(2);
			out.println(jadeCls+" outObj=new "+jadeCls+"();");
			out.println("outObj.setEnumEle("+ inObj + ".getValue());");
			out.println("return outObj;");
			out.setTabNum(1);
			out.println("}");

		}

	}

	private void generateVector2JadeArrayList(PrintWriterUtils out) throws IOException {
		
		out.setTabNum(1);
		out.println("//Vector<->ArrayList");
		out.println("public static jade.util.leap.ArrayList Vector2JadeArrayList(Vector cIn)");
		out.println("{");
		out.setTabNum(2);
		out.println("jade.util.leap.ArrayList cOut = new jade.util.leap.ArrayList();");
		out.println("Object eleIn = null;");
		out.println("for(int i=0;i<cIn.size();i++)");
		out.println("{");
		out.setTabNum(3);
		out.println("eleIn = cIn.elementAt(i);");
		
		out.println("String eleInCls = eleIn.getClass().getName();");
		out.println("String eleOutCls = TypeMapping.getJadeDataType(eleInCls, false);");

		out.println("if(TypeMapping.isPrimitiveObject(eleInCls))");
		out.println("{");
		out.println("	cOut.add(JavaTypeConverter.convert(eleIn, eleOutCls));");
		out.println("}");
		out.println("else if(eleInCls.equals(\"java.lang.String\"))");
		out.println("{");
		out.println("	cOut.add(eleIn);");
		out.println("}");
		out.println("else if(eleInCls.equals(\"java.util.ArrayList\"))");
		out.println("{");
	out.println("	cOut.add(ArrayList2JadeArrayList((java.util.ArrayList)eleIn));");
		out.println("}");
		out.println("else if(eleInCls.equals(\"java.util.Vector\"))");
		out.println("{");
		out.println("	cOut.add(Vector2JadeArrayList((java.util.Vector)eleIn));");
		out.println("}");
		for (Iterator it = map.keySet().iterator(); it.hasNext();) 
		{
			String jadeCls = (String) it.next();
			String jaxCls = TypeMapping.getComplexJaxType(jadeCls);
			if (jaxCls == null)
				continue;
				
			out.println("else if(eleIn instanceof "+jaxCls+")");
			out.println("{");
			out.setTabNum(4);
			out.println("cOut.add(convert(("+jaxCls+")eleIn));");
			out.setTabNum(3);
			out.println("}");
			
			
		}
		
		out.setTabNum(2);
		out.println("}");
		out.println("return cOut;");
		out.setTabNum(1);
		out.println("}");		
		
	}
	
	private void generateArrayList2JadeArrayList(PrintWriterUtils out) throws IOException {
		
		out.setTabNum(1);
		out.println("//ArrayList<->ArrayList");
		out.println("public static jade.util.leap.ArrayList ArrayList2JadeArrayList(java.util.ArrayList cIn)");
		out.println("{");
		out.setTabNum(2);
		out.println("jade.util.leap.ArrayList cOut = new jade.util.leap.ArrayList();");
		out.println("java.util.Vector vIn = new java.util.Vector(cIn);");
		out.println("return Vector2JadeArrayList(vIn);");
		out.setTabNum(1);
		out.println("}");		
		
	}
	private void generateJadeArrayList2Vector(PrintWriterUtils out) throws IOException {
		
		out.setTabNum(1);
		out.println("//JadeArrayList<->Vector");
		out.println("public static java.util.Vector JadeArrayList2Vector(jade.util.leap.ArrayList cIn)");
		out.println("{");
		out.setTabNum(2);
		out.println("return new java.util.Vector(JadeArrayList2ArrayList(cIn));");
		
		out.setTabNum(1);
		out.println("}");		
		
	}
	
	private void generateJadeArrayList2ArrayList(PrintWriterUtils out) throws IOException {
		
		out.setTabNum(1);
		out.println("//JadeArrayList<->ArrayList");
		out.println("public static java.util.ArrayList JadeArrayList2ArrayList(jade.util.leap.ArrayList cIn)");
		out.println("{");
		out.setTabNum(2);
		out.println("java.util.ArrayList cOut = new java.util.ArrayList();");
		out.println("Object eleIn = null;");
		out.println("for(int i=0;i<cIn.size();i++)");
		out.println("{");
		out.setTabNum(3);
		out.println("eleIn = cIn.get(i);");
		
		out.println("String eleInCls = eleIn.getClass().getName();");
		out.println("if(TypeMapping.isPrimitiveObject(eleInCls))");
		out.println("{");
		out.println("	cOut.add(eleIn);");
		out.println("}");
		out.println("else if(eleInCls.equals(\"java.lang.String\"))");
		out.println("{");
		out.println("	cOut.add(eleIn);");
		out.println("}");
		out.println("else if(eleInCls.equals(\"java.util.ArrayList\"))");
		out.println("{");
		out.println("	cOut.add(ArrayList2JadeArrayList((java.util.ArrayList)eleIn));");
		out.println("}");
		out.println("else if(eleInCls.equals(\"java.util.Vector\"))");
		out.println("{");
		out.println("	cOut.add(Vector2JadeArrayList((java.util.Vector)eleIn));");
		out.println("}");
		
		
		for (Iterator it = map.keySet().iterator(); it.hasNext();) 
		{
			String jadeCls = (String) it.next();
			String jaxCls = TypeMapping.getComplexJaxType(jadeCls);
			if (jaxCls == null)
				continue;
				
			out.println("else if(eleIn instanceof "+jadeCls+")");
			out.println("{");
			out.setTabNum(4);
			out.println("cOut.add(convert(("+jadeCls+")eleIn));");
			out.setTabNum(3);
			out.println("}");
			
		}
		
		out.setTabNum(2);
		out.println("}");
		out.println("return cOut;");
		out.setTabNum(1);
		out.println("}");		
		
	}
	
	private void generateTypeConverter(PrintWriterUtils out) throws IOException {
		Vector value = null;

		for (Iterator it = map.keySet().iterator(); it.hasNext();) {
			String jadeCls = (String) it.next();
			String jaxCls = TypeMapping.getComplexJaxType(jadeCls);
			if (jaxCls == null)
				continue;

			//Write out a conversion method from jadeCls to jaxCls
			out.setTabNum(1);
			out.println("public static " + jaxCls + " convert(" + jadeCls + " "
					+ inObj + ")");
			out.println("{");
			out.setTabNum(2);
			
			out.println("if("+inObj+"==null)");
			out.println(" return null;");
			out.println(jaxCls + " " + outObj + "= new " + jaxCls + "();");
			
			value = (Vector) map.get(jadeCls);
			for (int i = 0; i < value.size(); i++) {
				NameMapping aMapping = (NameMapping) value.elementAt(i);
				String varName = aMapping.getJavaName();
				String inType = aMapping.getJadeType();
				String outType = aMapping.getJavaType();
				//Jade2Jax
				writeTypeConverter(out, varName, inType, outType, false); 
				

			}
			out.println("return " + outObj + ";");
			out.setTabNum(1);
			out.println("}");

			//Jax2Jade

			out.println("public static " + jadeCls + " convert(" + jaxCls + " "
					+ inObj + ")");
			out.println("{");
			out.setTabNum(2);
			out.println("if("+ inObj + "==null)");
			out.println("return null;");
			out.println(jadeCls + " " + outObj + "= new " + jadeCls + "();");
			
			for (int i = 0; i < value.size(); i++) {
				NameMapping aMapping = (NameMapping) value.elementAt(i);
				String varName = aMapping.getJavaName();
				String inType = aMapping.getJavaType();
				String outType = aMapping.getJadeType();
				writeTypeConverter(out, varName, inType, outType, true); 
				

			}
			out.println("return " + outObj + ";");
			out.setTabNum(1);
			out.println("}");

		}

		//value.addElement(new NameMapping(xmlName, javaName, javaType,
		// jadeType));
		//map.put(cls, value);
	}

	//Only call this method when inType is primitive wrapper or generated type
	private void writeInOutConverting(PrintWriterUtils out, String varName,
			String inType, String outType) throws IOException {
		writeInOutConverting(out, varName + "In", varName + "Out", inType,
				outType);

	}

	private void writeInOutConverting(PrintWriterUtils out, String inName,
			String outName, String inType, String outType) throws IOException {
		out.setTabNum(3);
		Logger log = Logger.getLogger("WS2JADE");
		if (inType.equals(outType)) {
			out.println(outType + " " + outName + " = " + inName + ";");
		}

		else if (TypeMapping.isPrimitiveObject(inType)) {

			out.println(outType + " " + outName
					+ " = JavaTypeConverter.convert(" + inName + "," + outType
					+ ");");

		}
		else if(inType.equals("ciamas.wsjade.wsdl2jade.onto.Unknown"))
		{
			log.warn("Warning: No mapping is available for " + inType+
					"\n         Possible failure during execution " );
			out.println(outType + " " + outName + " = new " + outType
					+ " ();");
		}
		else //Must be a generated type
		{
			if(inType.equals("ciamas.wsjade.wsdl2jade.onto.Unknown"))
				out.println(outType + " " + outName + " = new " + outType
						+ "();");
			else			
			out.println(outType + " " + outName + " = convert(" + inName
					+ " );");

		}

	}

	//Write the method for type conversion between JAX-JADE WSDL type data

	private void writeTypeConverter(PrintWriterUtils out, String varName,
			String inType, String outType, boolean b) throws IOException {
		//JavaBean standard use isVar instead of getVar for boolean type
		// parameters
		String get = null;
		Logger log = Logger.getLogger("WS2JADE");
		//Jax2Jade
		if (b && (inType.endsWith(".Boolean") || inType.equals("boolean"))) 
		{

			get = ".is";
		} else
			get = ".get";

		String getMethodStr = inObj + get + BeanUtils.upperFirstLetter(varName);
		String setMethodStr = outObj + ".set"
				+ BeanUtils.upperFirstLetter(varName);
		String varIn = varName + "In";
		String varOut = varName + "Out";

		out.setTabNum(2);
		if (inType.equals(outType)) //In type, out type are the same
		{
			out.println(setMethodStr + "(" + getMethodStr + "());");
		} 
		else if (TypeMapping.isPrimitive(inType)) 
		{
			if (!TypeMapping.isPrimitive(outType)) 
			{
				log.info(inType + " to " + outType);
				log.error("Error: Primive types should be mapped to primitives");
				return;
			}

			out.println(setMethodStr + "((" + outType + ")" + getMethodStr
					+ "());");

		} 
		else if (TypeMapping.isPrimitiveObject(inType)) {
			out.println(inType + " " + varIn + " =" + getMethodStr + "();");
			out.println(outType + " " + varOut + "=JavaTypeConverter.convert("
					+ varIn + "," + outType + ");");

			out.println(setMethodStr + "(" + varOut + ");");
		}
		else if (TypeMapping.isArray(inType)) 
		{
			if (!outType.equals("jade.util.leap.ArrayList")) {

				log.info(inType + " to " + outType);
				log.error(inType + " must be mapped to ArrayList");
				return;
			}
			String baseInType = inType.substring(0, inType.indexOf("["));
			String baseOutType = TypeMapping.getJadeDataType(baseInType, false);
			
			//System.out.println("Base out type="+ baseOutType);
			//System.out.println("Base in type="+ baseInType);
			out.println(inType + " " + varIn + "=" + getMethodStr + "();");
			
			out.println("ArrayList " + varOut + "=new ArrayList();");
			out.println("if("+ varIn + "!=null)");
			out.println("for(int i=0;i<" + varIn + ".length;i++)");
			out.println("{");
			out.setTabNum(3);
			if (TypeMapping.isPrimitive(baseInType)) {
				String wrapperOutType = TypeMapping.getWrapperName(baseOutType);
				out.println(wrapperOutType + " ele = new " + wrapperOutType
						+ "((" + baseOutType + ")" + varIn + "[i]);");

				out.println(varOut + ".add(ele);");

			} else {

				out.println(baseInType + " eleIn = " + varIn + "[i];");
				writeInOutConverting(out, "ele", baseInType, baseOutType);

				out.println(varOut + ".add(eleOut);");
			}
			out.setTabNum(2);
			out.println("}");

			out.println(setMethodStr + "(" + varOut + ");");

		} 
		else if (inType.equals("jade.util.leap.ArrayList")) 
		{
			out.println(inType + " " + varName + "In =" + getMethodStr + "();");
			if(outType.equals("java.util.Vector"))
			out.println(outType + " " + varName + "Out=JadeArrayList2Vector(" + varName
					+ "In );");
			else if(outType.equals("java.util.ArrayList"))
				out.println(outType + " " + varName + "Out=JadeArrayList2ArrayList(" + varName
						+ "In );");
			else if(TypeMapping.isArray(outType))
			{
				String baseOutType = outType.substring(0, outType.indexOf("["));
				String baseInType = TypeMapping.getJadeDataType(baseOutType, false);
				out.println(outType +" "+ varName +"Out= new "+baseOutType+"["+varName +"In.size()];");
				out.println("for(int i=0;i<"+varName+"In.size();i++)");
				out.println("{");
						
				if(TypeMapping.isPrimitive(baseOutType))
				{
					String wrapperInType = TypeMapping.getWrapperName(baseInType);
					out.println(wrapperInType +" inEle="+"("+wrapperInType+")"+ varName+"In.get(i);");
					out.println(varName+"Out[i]=inEle."+ baseOutType+"Value();" );
				}
				else if(baseOutType.equals(baseInType))
				{
					out.println(baseInType +" inEle="+"("+baseInType+")"+ varName+"In.get(i);");
					out.println(varName+"Out[i]=inEle;");
				}
				else
				{
					out.println(baseInType +" inEle="+"("+baseInType+")"+ varName+"In.get(i);");
					if(baseInType.equals("ciamas.wsjade.wsdl2jade.onto.Unknown"))
						out.println(varName+"Out[i]=new "+baseOutType+"();" );
					else
					out.println(varName+"Out[i]=convert(inEle);" );
				}
				
				out.println("}");
				
				
			}
			out.println(setMethodStr + "(" + varName + "Out);");
		}
		else if (inType.equals("java.util.Vector")) 
		{
			out.println(inType + " " + varName + "In =" + getMethodStr + "();");
			if(outType.equals("jade.util.leap.ArrayList"))
				out.println(outType + " " + varName + "Out=Vector2JadeArrayList(" + varName
						+ "In );");
			else
			{
				log.error("Rules violated: "+inType+"->"+outType);
				return;
			}
			
			out.println(setMethodStr + "(" + varName + "Out);");
			
		} 
		else //Must be a generated type
		{
			out.println(inType + " " + varName + "In =" + getMethodStr + "();");
			if(inType.equals("ciamas.wsjade.wsdl2jade.onto.Unknown"))
				out.println(outType + " " + varName + "Out=new " + outType
						+ "();");
			else
			out.println(outType + " " + varName + "Out=convert(" + varName
					+ "In );");
			out.println(setMethodStr + "(" + varName + "Out);");

		}
	}

	

	public class MessageCache {
		private int type;

		private String message;

		public MessageCache(int t, String msg) {
			type = t;
			message = msg;
		}

		public int getType() {
			return type;
		}

		public String getMessage() {
			return message;
		}

	}

	//This cache information is used for mapping between Action/Concept and
	// operation
	public class OperationCache {
		private Parameters params;

		private String optName;

		private String jadeBasePkg;

		private String portName;

		public OperationCache(String sOpt, String sPkg, Parameters p) {
			params = p;
			optName = sOpt;
			jadeBasePkg = sPkg;

		}

		public Parameters getParams() {
			return params;
		}

		public String getOptName() {
			return optName;
		}

		public String getJadePkg() {
			return jadeBasePkg;
		}

	}

	//This cache information is used for mapping between JAX stub types and
	// JADE types
	public class NameMapping {

		//For each NameMapping, xmlName and javaName is unique
		private String xmlName;

		private String javaName;

		private String javaType;

		private String jadeType;

		public NameMapping(String xName, String jName, String javaType,
				String jadeType) {
			this.xmlName = xName;
			this.javaName = jName;
			this.javaType = javaType;
			this.jadeType = jadeType;
		}

		public String getXmlName() {
			return this.xmlName;
		}

		public void setXmlName(String x) {
			this.xmlName = x;
		}

		public String getJavaName() {
			return this.javaName;
		}

		public void setJavaName(String j) {
			this.javaName = j;
		}

		public String getJavaType() {
			return this.javaType;
		}

		public void setJavaType(String s) {
			this.javaType = s;
		}

		public void setJadeType(String s) {
			this.jadeType = s;
		}

		public String getJadeType() {
			return this.jadeType;
		}

	}

}