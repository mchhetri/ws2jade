/*
 * Created on 3/02/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ciamas.wsjade.wsdl2jade.projection.owl;
import java.io.IOException;
import java.util.Vector;

import javax.xml.namespace.QName;
import javax.wsdl.Operation;
import javax.wsdl.PortType;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.Parameter;
import org.apache.axis.wsdl.symbolTable.Parameters;
import org.apache.axis.wsdl.symbolTable.SchemaUtils;
import org.apache.axis.wsdl.symbolTable.SymbolTable;


/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OWLConceptWriter {
	protected Operation opt;
	protected BindingEntry bEntry;
	protected PortType pType;
	Vector names = new Vector();
	
	public OWLConceptWriter(Operation o, PortType p,
			BindingEntry b, SymbolTable symbolTable) {
	
		this.opt = o;
		this.pType = p;
		this.bEntry = b;
	
	}

	protected void preprocess()  {
		
		Parameters parms = bEntry.getParameters(opt);
		if (parms.returnParam != null) {

			Parameter p = parms.returnParam;
			QName xmlType = p.getType().getQName();
			String typeName = xmlType.getLocalPart();
			String variableName= p.getName() ;
			names.add(typeName);
			names.add(variableName);
			if (SchemaUtils.isSimpleSchemaType(xmlType))
				names.add(new Boolean(true));
			else
				names.add(new Boolean(false));
			
		}
		for (int i = 0; parms != null && i < parms.list.size(); ++i) {
			Parameter p = (Parameter) parms.list.get(i);

			if (p.getMode() == Parameter.OUT || p.getMode() == Parameter.INOUT) {
				
				QName xmlType = p.getType().getQName();
				String typeName = xmlType.getLocalPart();
				String variableName= p.getName() ;
				names.add(typeName);
				names.add(variableName);
				if (SchemaUtils.isSimpleSchemaType(xmlType))
					names.add(new Boolean(true));
				else
					names.add(new Boolean(false));

			}

		}	

	}
	public void generate() throws IOException {
		preprocess();
		String clsName =opt.getName()+"Concept";
		String clsDef = "<owl:Class rdf:ID=\"" + clsName + "\">\n"
				+ "    <owl:subClassOf rdf:resource=\"&owl;#Thing\"/>\n"
				+ "</owl:Class>\n";

		Temp.getInstance().addText(clsDef);

		for (int i = 0; i < names.size(); i = i + 3) {
			boolean b = ((Boolean) names.elementAt(i + 2)).booleanValue();
			String protT = "";
			if (b)
				protT = "&xsd;" + "#" + names.elementAt(i);
			else
				protT = "#" + names.elementAt(i);
			String propDef =

			"\n<owl:Property rdf:ID=\"" + names.elementAt(i + 1) + "\">\n"
					+ "    <rdfs:range rdf:resource=\"" + protT + "\"/>\n"
					+ "    <rdfs:domain rdf:resource=\"" + "#" + clsName
					+ "\"/>\n" + "</owl:Property>\n";

			Temp.getInstance().addText(propDef);
		}
	}

	
}
