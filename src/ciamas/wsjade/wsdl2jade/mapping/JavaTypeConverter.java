/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.mapping;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.namespace.QName;

public class JavaTypeConverter {

	//Primitive holder coversion
	public static Object convert(Object inObj, String outClsName) {
		if (inObj == null)
			return null;

		String inClsName = inObj.getClass().getName();
		//String outClsName = outCls.getName();

		if (TypeMapping.isComplex(inClsName))
			return null;

		//Go through all Jaxrpc to Jade cases
		if (outClsName.equals("java.lang.Integer")) {
			if (inClsName.equals("java.lang.Integer"))
				return inObj;
			else if (inClsName.equals("java.lang.Short")) {
				Short obj = (Short) inObj;
				return new Integer(obj.intValue());
			} else if (inClsName.equals("java.lang.Byte")) {
				Byte obj = (Byte) inObj;
				return new Integer(obj.intValue());
			} else if (inClsName.equals("java.math.BigInteger")) {
				BigInteger obj = (BigInteger) inObj;
				return new Integer(obj.intValue());
			}		
			else if (inClsName.equals("org.apache.axis.types.PositiveInteger")) 
			{
				Integer obj = new Integer(((org.apache.axis.types.PositiveInteger) inObj).intValue());
				return obj;
			} 
			else if (inClsName.equals("org.apache.axis.types.NonPositiveInteger")) 
			{
				Integer obj = new Integer(((org.apache.axis.types.NonPositiveInteger) inObj).intValue());
				return obj;
			}
			else if (inClsName.equals("org.apache.axis.types.NonNegativeInteger")) 
			{
				Integer obj = new Integer(((org.apache.axis.types.NonNegativeInteger) inObj).intValue());
				return obj;
			}
			else if (inClsName.equals("org.apache.axis.types.NegativeInteger")) 
			{
			
				Integer obj = new Integer(((org.apache.axis.types.NegativeInteger) inObj).intValue());
				return obj;
			}
			else if (inClsName.equals("org.apache.axis.types.UnsignedShort")) 
			{
				Integer obj = new Integer(((org.apache.axis.types.UnsignedShort) inObj).intValue());
				return obj;
			} 
			
			else
				return null; 

		} 
		else if (outClsName.equals("java.lang.Long")) {
			if (inClsName.equals("java.lang.Long")) {
				return inObj;
			}
			else if (inClsName.equals("org.apache.axis.types.UnsignedInt")) 
			{
					Long obj = new Long(((org.apache.axis.types.UnsignedInt) inObj).longValue());
					return obj;

			} 
			else if (inClsName.equals("org.apache.axis.types.UnsignedLong")) 
			{
				Long obj = new Long(((org.apache.axis.types.UnsignedLong) inObj).longValue());
				return obj;

		    } 
			else
				return null;
		} else if (outClsName.equals("java.lang.Float")) {
			if (inClsName.equals("java.lang.Float")) {
				return inObj;

			} else
				return null;
		} else if (outClsName.equals("java.lang.Double")) {
			if (inClsName.equals("java.lang.Double")) {
				return inObj;
			} else if (inClsName.equals("java.math.BigDecimal")) {
				BigDecimal obj = (BigDecimal) inObj;
				return new Double(obj.doubleValue());

			} else
				return null;

		} else if (outClsName.equals("java.lang.Boolean")) {
			if (inClsName.equals("java.lang.Boolean")) {
				return inObj;
			} else
				return null;
		} else if (outClsName.equals("javax.xml.namespace.QName")) {
			if (inClsName.equals("java.lang.String")) {
				QName obj = new QName(""+inObj);
				return obj;
			} else
				return null;

		} else if (outClsName.equals("java.util.Date")) {
			if (inClsName.equals("java.util.Calendar")) {
				GregorianCalendar obj = (GregorianCalendar) inObj;
				return obj.getTime();
			} else
				return null;
		}
		
		 else if (outClsName.equals("java.lang.String")) {
			if (inClsName.equals("org.apache.axis.types.NormalizedString")) {
				return ""+ inObj;

			} 
			else
				return null;
		 }
		
		//Go through all Jade to Jax cases
		else if (outClsName.equals("java.math.BigInteger")) {
			if (inClsName.equals("java.lang.Integer")) {
				Integer obj = (Integer) inObj;
				return new BigInteger("" + obj);
			} else
				return null;
		} else if (outClsName.equals("java.lang.Short")) {
			if (inClsName.equals("java.lang.Integer")) //Lose sign and
			// precision
			{
				Integer obj = (Integer) inObj;
				return new Short(obj.shortValue());
			} else
				return null;
		} else if (outClsName.equals("java.lang.Byte")) {
			if (inClsName.equals("java.lang.Integer")) {
				Integer obj = (Integer) inObj;
				return new Byte(obj.byteValue());

			} else
				return null;
		} else if (outClsName.equals("java.math.BigDecimal")) {
			if (inClsName.equals("java.lang.Double")) {
				Double obj = (Double) inObj;
				return new BigDecimal("" + obj);
			} else
				return null;
		} else if (outClsName.equals("javax.xml.namespace.QName")) {
			if (inClsName.equals("java.lang.String")) {
				String obj = (String) inObj;
				return QName.valueOf(obj);

			} else
				return null;
		} else if (outClsName.equals("java.util.GregorianCalendar")) {
			if (inClsName.equals("java.util.Date")) {

				Date obj = (Date) inObj;
				GregorianCalendar calen = new GregorianCalendar();
				calen.setTime(obj);
				return calen;
			} else
				return null;
		}else if (outClsName.equals("org.apache.axis.types.PositiveInteger")) {
			if (inClsName.equals("java.lang.Integer")) {
				org.apache.axis.types.PositiveInteger obj = new org.apache.axis.types.PositiveInteger(""+inObj);
				return obj;
			} else
				return null;
		} else if (outClsName.equals("org.apache.axis.types.NonPositiveInteger")) {
			if (inClsName.equals("java.lang.Integer")) {
				org.apache.axis.types.NonPositiveInteger obj = new 
				org.apache.axis.types.NonPositiveInteger(""+inObj);
				return obj;
			} else
				return null;
		} else if (outClsName.equals("org.apache.axis.types.NonNegativeInteger")) {
			if (inClsName.equals("java.lang.Integer")) {
				org.apache.axis.types.NonNegativeInteger obj = new
				org.apache.axis.types.NonNegativeInteger("" +inObj);
				return obj;
			} else
				return null;
		} else if (outClsName.equals("org.apache.axis.types.NegativeInteger")) {
			if (inClsName.equals("java.lang.Integer")) {
				
				org.apache.axis.types.NegativeInteger obj = new 
				org.apache.axis.types.NegativeInteger(""+inObj);
				return obj;
			} else
				return null;
		} else if (outClsName.equals("org.apache.axis.types.UnsignedInt")) {
			if (inClsName.equals("java.lang.Long")) {
				
				org.apache.axis.types.UnsignedInt obj = new 
				org.apache.axis.types.UnsignedInt(""+ inObj);
				return obj;
			} else
				return null;
		} else if (outClsName.equals("org.apache.axis.types.UnsignedLong")) {
			if (inClsName.equals("java.lang.Long")) {
				org.apache.axis.types.UnsignedLong obj = new
				org.apache.axis.types.UnsignedLong(""+ inObj);
				return obj;
			} else
				return null;
		} else if (outClsName.equals("org.apache.axis.types.UnsignedShort")) {
			if (inClsName.equals("java.lang.Integer")) {
				org.apache.axis.types.UnsignedShort obj = new 
				org.apache.axis.types.UnsignedShort(""+ inObj);
				return obj;
			} else
				return null;
		} else if (outClsName.equals("org.apache.axis.types.NormalizedString")) {
			if (inClsName.equals("java.lang.String")) {
				org.apache.axis.types.NormalizedString obj = new
				org.apache.axis.types.NormalizedString(""+ inObj);
				return obj;
				
			} else
				return null;
		}
		else if (inClsName.equals("org.apache.axis.types.Day")) 
		{
			ciamas.wsjade.wsdl2jade.onto.Day obj = new 
							ciamas.wsjade.wsdl2jade.onto.Day();
			//cast
			org.apache.axis.types.Day in = (org.apache.axis.types.Day)inObj;
			obj.setDay(in.getDay());
			obj.setTimezone(in.getTimezone());
			return obj;
		} 
		else if (inClsName.equals("ciamas.wsjade.wsdl2jade.onto.Day")) 
		{
			//cast
			ciamas.wsjade.wsdl2jade.onto.Day in = (ciamas.wsjade.wsdl2jade.onto.Day)inObj;
			org.apache.axis.types.Day obj = new org.apache.axis.types.Day(in.getDay(), in.getTimezone());
			return obj;
		}
		else if (inClsName.equals("org.apache.axis.types.Month")) 
		{
			ciamas.wsjade.wsdl2jade.onto.Month obj = new 
							ciamas.wsjade.wsdl2jade.onto.Month();
			//cast
			org.apache.axis.types.Month in = (org.apache.axis.types.Month)inObj;
			obj.setMonth(in.getMonth());
			obj.setTimezone(in.getTimezone());
			return obj;
		} 
		else if (inClsName.equals("ciamas.wsjade.wsdl2jade.onto.Month")) 
		{		
			//cast
			ciamas.wsjade.wsdl2jade.onto.Month in = (ciamas.wsjade.wsdl2jade.onto.Month)inObj;
			org.apache.axis.types.Month obj = new 
						org.apache.axis.types.Month(in.getMonth(), in.getTimezone());
			return obj;
		} 
		else if (inClsName.equals("org.apache.axis.types.MonthDay")) 
		{
			ciamas.wsjade.wsdl2jade.onto.MonthDay obj = new 
							ciamas.wsjade.wsdl2jade.onto.MonthDay();
			//cast
			org.apache.axis.types.MonthDay in = (org.apache.axis.types.MonthDay)inObj;
			obj.setMonth(in.getMonth());
			obj.setTimezone(in.getTimezone());
			obj.setDay(in.getDay());
			return obj;
		} 
		else if (inClsName.equals("ciamas.wsjade.wsdl2jade.onto.MonthDay")) 
		{	
			//cast
			ciamas.wsjade.wsdl2jade.onto.MonthDay in = (ciamas.wsjade.wsdl2jade.onto.MonthDay)inObj;
			org.apache.axis.types.MonthDay obj = new org.apache.axis.types.MonthDay(
			in.getMonth(),in.getDay(),in.getTimezone());
			return obj;
		} 
		else if (inClsName.equals("org.apache.axis.types.Duration")) 
		{
			ciamas.wsjade.wsdl2jade.onto.Duration obj = new 
							ciamas.wsjade.wsdl2jade.onto.Duration();
			//cast
			org.apache.axis.types.Duration in = (org.apache.axis.types.Duration)inObj;
			obj.setDays(in.getDays());
			obj.setHours(in.getHours());
			obj.setMinutes(in.getMinutes());
			obj.setMonths(in.getMonths());
			obj.setNegative(in.isNegative());
			obj.setSeconds(in.getSeconds());
			obj.setYears(in.getYears());
			return obj;
		} 
		else if (inClsName.equals("ciamas.wsjade.wsdl2jade.onto.Duration")) 
		{
			org.apache.axis.types.Duration obj = new 
			org.apache.axis.types.Duration();
			//cast
			ciamas.wsjade.wsdl2jade.onto.Duration in = (ciamas.wsjade.wsdl2jade.onto.Duration)inObj;
			obj.setDays(in.getDays());
			obj.setHours(in.getHours());
			obj.setMinutes(in.getMinutes());
			obj.setMonths(in.getMonths());
			obj.setNegative(in.isNegative());
			obj.setSeconds((int)in.getSeconds());
			obj.setYears(in.getYears());
			return obj;
		} 
		return null;
	}
}