/*
 * Created on 2/12/2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package samples;

/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

import jade.content.*;
import jade.content.onto.*;
import jade.core.*;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import jade.util.leap.*;
import jade.content.onto.basic.*;
import jade.content.lang.*;
import jade.content.lang.sl.*;


import JadeGen.onto.Google.GoogleSearchPort.Request.DoGoogleSearch;
import JadeGen.onto.reflect.GoogleSearch.GoogleSearchResult;


public class GoogleClientAgent extends Agent {

	// CREATE THE ONTOLOGY INSTANCE
        private ContentManager manager  = (ContentManager) getContentManager();
        private Codec codec = new SLCodec();
        private Ontology ontology = JadeGen.onto.Google.WebOntology.getInstance();

        protected void setup() {

		// REGISTER THE ONTOLOGY
		manager.registerLanguage(codec);
		manager.registerOntology(ontology);

		// ADD HANDLEINFORMBEHAVIOUR
		addBehaviour(new HandleInformBehaviour(this));

		// CREATE A REQUEST MESSAGE
		ACLMessage requestMsg = new ACLMessage(ACLMessage.REQUEST);
		
		requestMsg.setSender(getAID());

		// IDENTIFY THE WEB SERVICE AGENT 
		requestMsg.addReceiver(new AID("GoogleProvider", false)); 
		requestMsg.setLanguage(codec.getName());
		requestMsg.setOntology(ontology.getName());
	

		// INSTANTIATE THE CHOSEN AGENTACTION CLASS
		DoGoogleSearch requestAction = 
		new DoGoogleSearch();
		requestAction.setKey("/muoyLpQFHKE9gCRkTaiN9yZ8KJSatH4");
		requestAction.setMaxResults(5);
		requestAction.setFilter(false);
		requestAction.setIe("");
		requestAction.setLr("lang_en" );
		requestAction.setOe("");
		requestAction.setQ("ciamas");
		requestAction.setRestrict("0");
		requestAction.setSafeSearch(true);
		requestAction.setStart(0);
	
	   
		System.out.println("Client Agent search for word \"ciamas\"");
	     try 
	     { 
			// SEND THE MESSAGE
			manager.fillContent( requestMsg, 
					new Action(new AID("GoogleClientAgent", false), requestAction));
					
			send(requestMsg);
		
	     } 
	     catch(Exception e) 
	     { e.printStackTrace(); }
	   }
	   
	   
     class HandleInformBehaviour extends CyclicBehaviour
     {
        public HandleInformBehaviour(Agent a) { super(a); }
        
        public void action() 
        {
            ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));
            if (msg != null) 
            {
             try 
             {
                ContentElement ce = manager.extractContent(msg);
					
				if (ce instanceof jade.content.onto.basic.Result) 
				{
					jade.content.onto.basic.Result result = (jade.content.onto.basic.Result)ce;
					System.out.println();
					System.out.print("Returned value: ");
					JadeGen.onto.Google.GoogleSearchPort.Response.DoGoogleSearch	ret =
						
					(JadeGen.onto.Google.GoogleSearchPort.Response.DoGoogleSearch)result.getValue();
					
					GoogleSearchResult search_result = ret.get_return();
					ArrayList lists =search_result.getResultElements();
					for(int i=0;i< lists.size();i++)
					{
						JadeGen.onto.reflect.GoogleSearch.ResultElement aResult = (JadeGen.onto.reflect.GoogleSearch.ResultElement)lists.get(i);
						System.out.println(aResult.getURL());
						
					}
		
				} 
		
               } catch(Exception e) { e.printStackTrace(); }
            } else { block(); }
    	} 
    } 
} 


