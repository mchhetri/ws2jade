/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.wsdl2jade.projection.owl;

import javax.wsdl.Binding;
import javax.wsdl.Definition;
import javax.wsdl.Message;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import javax.xml.namespace.QName;

import org.apache.axis.encoding.DefaultSOAPEncodingTypeMappingImpl;
import org.apache.axis.encoding.TypeMapping;
import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.gen.GeneratorFactory;
import org.apache.axis.wsdl.gen.NoopGenerator;
import org.apache.axis.wsdl.symbolTable.BaseTypeMapping;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.symbolTable.TypeEntry;



/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class OWLGeneratorFactory implements GeneratorFactory {

	public void generatorPass(Definition def, SymbolTable symbolTable) {
	} // generatorPass

	public Generator getGenerator(Message message, SymbolTable symbolTable) {
		return new NoopGenerator();
	} // getGenerator

	public Generator getGenerator(PortType portType, SymbolTable symbolTable) {
		return new NoopGenerator();
	} // getGenerator

	public Generator getGenerator(Binding binding, SymbolTable symbolTable) {
		Generator writer = new OWLBindingWriter(null, binding, symbolTable);
		return writer;
	} // getGenerator

	public Generator getGenerator(Service service, SymbolTable symbolTable) {
		return new NoopGenerator();
	} // getGenerator

	public Generator getGenerator(TypeEntry type, SymbolTable symbolTable) {

		Generator writer = new OWLTypeWriter(null, type, symbolTable);
		return writer;

	} // getGenerator

	public Generator getGenerator(Definition definition, SymbolTable symbolTable) {
		return new NoopGenerator();
	} // getGenerator

	private BaseTypeMapping btm = null;

	public void setBaseTypeMapping(BaseTypeMapping btm) {
		this.btm = btm;
	} // setBaseTypeMapping

	public BaseTypeMapping getBaseTypeMapping() {
		if (btm == null) {
			btm = new BaseTypeMapping() {
				TypeMapping defaultTM = DefaultSOAPEncodingTypeMappingImpl
						.createWithDelegate();

				public String getBaseName(QName qNameIn) {
					javax.xml.namespace.QName qName = new javax.xml.namespace.QName(
							qNameIn.getNamespaceURI(), qNameIn.getLocalPart());
					Class cls = defaultTM.getClassForQName(qName);
					if (cls == null) {
						return null;
					} else {

						return cls.getName();
					}
				}
			};
		}
		return btm;
	} // getBaseTypeMapping

}