/*
 * SearchResult.java
 *
 * Created on 31 January 2005, 15:53
 */

package ciamas.ws2jadedemo.ontology;

import jade.content.*;

/**
 *
 * @author  403515
 */
public class SearchResult implements Concept {
    
    private String productName = null;
    private String asin = null;
    private String listPrice = null;
    private String ourPrice = null;
    private String availability = null;
    private String manufacturer = null;
    private String url = null;
    private String crComment = null;
    private String crRating = null;
    private String crSummary = null;
    private String quantity = null;
    
    /** Creates a new instance of SearchResult */
    public SearchResult() {
    }
    
    public void setProductName(String productName) {
        this.productName = productName;
    }
    
    public String getProductName() {
        return productName;
    }
    
    public void setAsin(String asin) {
        this.asin = asin;
    }
    
    public String getAsin() {
        return asin;
    }
    
    public void setListPrice(String listPrice) {
        this.listPrice = listPrice;
    }
    
    public String getListPrice() {
        return listPrice;
    }
    
    public void setOurPrice(String ourPrice) {
        this.ourPrice = ourPrice;
    }
    
    public String getOurPrice() {
        return ourPrice;
    }
    
    public void setAvailability (String availability) {
        this.availability = availability;        
    }
    
    public String getAvailability () {
        return availability;
    }
    
    public void setManufacturer (String manufacturer) {
        this.manufacturer = manufacturer;        
    }
    
    public String getManufacturer () {
        return manufacturer;
    }
    
    public void setUrl (String url) {
        this.url = url;        
    }
    
    public String getUrl () {
        return url;
    }
    
    public void setCrRating (String crRating) {
        this.crRating = crRating;        
    }
    
    public String getCrRating () {
        return crRating;
    }
    
    public void setCrSummary (String crSummary) {
        this.crSummary = crSummary;        
    }
    
    public String getCrSummary () {
        return crSummary;
    }
    
    public void setCrComment (String crComment) {
        this.crComment = crComment;        
    }
    
    public String getCrComment () {
        return crComment;
    }
    
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    
    public String getQuantity() {
        return quantity;
    }
    
}
