/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.mapping;

import ciamas.wsjade.wsdl2jade.utils.Common;

public class TypeMapping {

	public static boolean needList(String javaType) {
		if ((!javaType.startsWith("byte[")) && (javaType.indexOf("[") >= 0))
			return true;
		return false;
	}

	public static String getJadeSchemaType(String jadeType) {
		//In the generated code this version, everything is either
		//Action or Concept
		if (jadeType == null)
			return null;

		if (jadeType.equals("jade.content.onto.BasicOntology.BOOLEAN")
				|| jadeType.equals("jade.content.onto.BasicOntology.INTEGER")
				|| jadeType.equals("jade.content.onto.BasicOntology.FLOAT")
				|| jadeType
						.equals("jade.content.onto.BasicOntology.BYTE_SEQUENCE")
				|| jadeType.equals("jade.content.onto.BasicOntology.DATE")
				|| jadeType.equals("jade.content.onto.BasicOntology.STRING"))
			return "PrimitiveSchema";
		else if (jadeType.equals("jade.content.onto.CONTENT_ELEMENT_LIST"))
			return "AggregateSchema";
		else if (Common.getInstance().isRequest(jadeType)) {
			return "AgentActionSchema";
		} else
			return "ConceptSchema";

	}

	public static String getJadeOntology(String javaType) {
		javaType=javaType.trim();
		if (javaType.equals("boolean") || javaType.equals("java.lang.Boolean"))
			return "jade.content.onto.BasicOntology.BOOLEAN";
		else if (javaType.equals("int") || javaType.equals("short")
				|| javaType.equals("byte") || javaType.equals("long")
				|| javaType.equals("java.math.BigInteger")
				|| javaType.equals("java.lang.Byte")
				|| javaType.equals("java.lang.Integer")
				|| javaType.equals("java.lang.Short")
				|| javaType.equals("java.lang.Long"))
			return "jade.content.onto.BasicOntology.INTEGER";
		else if (javaType.startsWith("byte["))
			return "jade.content.onto.BasicOntology.BYTE_SEQUENCE";
		else if (javaType.indexOf("[") >= 0) {
			String realType = javaType.substring(0, javaType.length() - 3);
			return getJadeOntology(realType);

		} else if (javaType.equals("java.util.Calendar")
				|| javaType.equals("java.util.GregorianCalendar")
				|| javaType.equals("java.util.Date")) {
			return "jade.content.onto.BasicOntology.DATE";
		} else if (javaType.equals("double") || javaType.equals("float")
				|| javaType.equals("java.math.BigDecimal")
				|| javaType.equals("java.lang.Double")
				|| javaType.equals("java.lang.Float"))
			return "jade.content.onto.BasicOntology.FLOAT";
		else if (javaType.equals("java.lang.String")
				|| javaType.equals("javax.xml.rpc.namespace.QName"))
			return "jade.content.onto.BasicOntology.STRING";
		else if (javaType.equals("java.util.Vector"))
			return "jade.content.onto.BasicOntology.CONTENT_ELEMENT_LIST";
		else if (isUnknown(javaType))
			return "Unknow Concept"; //No mapping
		else
			return javaType; //Note that this is still javaType

	}
	public static boolean isUnknown(String javaType)
	{
		javaType = javaType.trim();
		if (javaType.equals("java.util.HashMap")
				|| javaType.equals("org.w3c.dom.Element") 
				|| javaType.equals("java.lang.Object") 
			|| javaType.equals("org.apache.axis.message.MessageElement"))
	return true;
	return false;
	
	}
	public static boolean isAxisPrimitive(String javaType)
	{
		javaType = javaType.trim();
		if (javaType.equals("org.apache.axis.types.Day")
				|| javaType.equals("org.apache.axis.types.Month") 
				|| javaType.equals("org.apache.axis.types.MonthDay") 
			|| javaType.equals("org.apache.axis.types.Duration")) //more
	return true;
	return false;
	
	}

	public static boolean isCompositeOnto(String javaCls) {
		if (javaCls.equals("jade.content.onto.BasicOntology.BOOLEAN")
				|| javaCls.equals("jade.content.onto.BasicOntology.INTEGER")
				|| javaCls
						.equals("jade.content.onto.BasicOntology.BYTE_SEQUENCE")
				|| javaCls.equals("jade.content.onto.BasicOntology.DATE")
				|| javaCls.equals("jade.content.onto.BasicOntology.FLOAT")
				|| javaCls.equals("jade.content.onto.BasicOntology.STRING")
				|| javaCls
						.equals("jade.content.onto.BasicOntology.CONTENT_ELEMENT_LIST"))
			return false;

		return true;

	}

	//Flag b is on if we want a full qualified class name
	public static String getJadeDataType(String jaxType, boolean b) {
		jaxType = jaxType.trim();
		//All primitive types
		if (jaxType.equals("boolean"))
			return "boolean";
		else if (jaxType.equals("java.lang.Boolean"))
			return "java.lang.Boolean";
		else if (jaxType.equals("int") || jaxType.equals("short")
				|| jaxType.equals("byte"))
			return "int";
		else if (jaxType.equals("java.math.BigInteger")
				|| jaxType.equals("java.lang.Integer")
				|| jaxType.equals("java.lang.Short")
				|| jaxType.equals("java.lang.Byte"))
			return "java.lang.Integer";
		else if (jaxType.equals("long"))
			return "long";
		else if (jaxType.equals("java.lang.Long"))
			return "java.lang.Long";
		else if (jaxType.startsWith("byte["))
			return "byte[]";
		else if (jaxType.indexOf("[") >= 0) {
			return "jade.util.leap.ArrayList";
		} else if (jaxType.equals("java.util.Calendar")
				|| jaxType.equals("java.util.GregorianCalendar")
				|| jaxType.equals("java.util.Date")) {
			return "java.util.Date";
		} else if (jaxType.equals("double"))
			return "double";
		else if (jaxType.equals("java.math.BigDecimal")
				|| jaxType.equals("java.lang.Double"))
			return "java.lang.Double";
		else if (jaxType.equals("float"))
			return "float";
		else if (jaxType.equals("java.lang.Float"))
			return "java.lang.Float";
		else if (jaxType.equals("java.lang.String")
				|| jaxType.equals("javax.xml.rpc.namespace.QName"))
			return "java.lang.String";
		else if (jaxType.equals("java.util.Vector"))
			return "jade.util.leap.ArrayList";
		else if (jaxType.equals("java.util.ArrayList"))
			return "jade.util.leap.ArrayList";
		//To do: Create new Onto with Facets for following types
		else if(jaxType.equals("org.apache.axis.types.PositiveInteger"))
		{
			return "java.lang.Integer";
		}
		else if(jaxType.equals("org.apache.axis.types.NonPositiveInteger"))
		{
			return "java.lang.Integer";
		}
		else if(jaxType.equals("org.apache.axis.types.NonNegativeInteger"))
		{
			return "java.lang.Integer";
		}
		else if(jaxType.equals("org.apache.axis.types.NegativeInteger"))
		{
			return "java.lang.Integer";
		}
		else if(jaxType.equals("org.apache.axis.types.NormalizedString"))
		{
			return "java.lang.String";
		}
		else if(jaxType.equals("org.apache.axis.types.UnsignedInt"))
		{
			return "java.lang.Long";
		}
		else if(jaxType.equals("org.apache.axis.types.UnsignedLong"))
		{
			return "java.lang.Long";
		}
		else if(jaxType.equals("org.apache.axis.types.UnsignedShort"))
		{
			return "java.lang.Integer";
		}
		else if(jaxType.equals("org.apache.axis.types.Day"))
		{
			return "ciamas.wsjade.wsdl2jade.onto.Day";
		}
		else if(jaxType.equals("org.apache.axis.types.Month"))
		{
			return "ciamas.wsjade.wsdl2jade.onto.Month";
		}
		else if(jaxType.equals("org.apache.axis.types.MonthDay"))
		{
			return "ciamas.wsjade.wsdl2jade.onto.MonthDay";
		}
		else if(jaxType.equals("org.apache.axis.types.Year"))
		{
			return "ciamas.wsjade.wsdl2jade.onto.Year";
		}
		else if(jaxType.equals("org.apache.axis.types.Duration"))
		{
			return "ciamas.wsjade.wsdl2jade.onto.Duration";
		}
		else if(jaxType.startsWith("org.apache.axis.types") 
				|| isUnknown(jaxType)) 
		{
			return "ciamas.wsjade.wsdl2jade.onto.Unknown"; //Fail
		}	
		else
		{
			String reflectPkg = Common.getFullReflectPkg();
			String jaxPkg = Common.getFullJaxPkg();

			return reflectPkg + jaxType;
		}

	}

	public static String getComplexJaxType(String jadeType) {
		String reflectPkg = Common.getFullReflectPkg();
		String jaxPkg = Common.getFullJaxPkg();

		if (jadeType.startsWith(reflectPkg)) {
			String s = jadeType.substring(reflectPkg.length());
			return jaxPkg + s;
		} else
			return null;
	}

	public static String getComplexJadeType(String jaxType) {
		String reflectPkg = Common.getFullReflectPkg();
		/*
		 * String jaxPkg = Common.getFullJadePkg();
		 * 
		 * if(jaxType.startsWith(reflectPkg)) { String s =
		 * jaxType.substring(reflectPkg.length() +1); return jaxPkg + s; } else
		 * return "UNKNOWN" +s;
		 */
		return reflectPkg + "." + jaxType;

	}

	//Complex is anything else except primitive and primitive wrappers
	public static boolean isComplex(String jaxType) {

		if (jaxType.equals("boolean") || jaxType.equals("java.lang.Boolean")
				|| jaxType.equals("int") || jaxType.equals("short")
				|| jaxType.equals("byte")
				|| jaxType.equals("java.math.BigInteger")
				|| jaxType.equals("java.lang.Integer")
				|| jaxType.equals("java.lang.Short")
				|| jaxType.equals("java.lang.Byte") || jaxType.equals("long")
				|| jaxType.equals("java.lang.Long")
				|| jaxType.equals("java.math.BigDecimal")
				|| jaxType.indexOf("[") >= 0
				|| jaxType.equals("java.util.Calendar")
				|| jaxType.equals("java.util.GregorianCalendar")
				|| jaxType.equals("java.util.Date")
				|| jaxType.equals("double")
				|| jaxType.equals("java.lang.Double")
				|| jaxType.equals("java.lang.Float")
				|| jaxType.equals("java.lang.String")
				|| jaxType.equals("javax.xml.rpc.namespace.QName")
				|| jaxType.equals("org.apache.axis.types.PositiveInteger")
				|| jaxType.equals("org.apache.axis.types.NonPositiveInteger")
				|| jaxType.equals("org.apache.axis.types.NonNegativeInteger")
				|| jaxType.equals("org.apache.axis.types.NegativeInteger")
				|| jaxType.equals("org.apache.axis.types.NormalizedString")
				|| jaxType.equals("org.apache.axis.types.UnsignedInt")
				|| jaxType.equals("org.apache.axis.types.UnsignedLong")
				|| jaxType.equals("org.apache.axis.types.UnsignedLong")
				|| jaxType.equals("org.apache.axis.types.UnsignedShort")
				|| jaxType.equals("org.apache.axis.types.Day")
				|| jaxType.equals("org.apache.axis.types.Month")
				|| jaxType.equals("org.apache.axis.types.MonthDay")
				|| jaxType.equals("org.apache.axis.types.Duration") 
				||  jaxType.startsWith("org.apache.axis.types")
				||	jaxType.equals("java.lang.Object")
				||	jaxType.equals("java.util.HashMap")
				|| jaxType.equals("org.w3c.dom.Element")
				|| jaxType.equals("org.apache.axis.message.MessageElement")
			
		)
			return false;

		return true;

	}

	public static boolean isPrimitive(String jaxType) {
		//All primitive types
		if (jaxType.equals("boolean") || jaxType.equals("int")
				|| jaxType.equals("short") || jaxType.equals("byte")
				|| jaxType.equals("long") || jaxType.equals("double"))
			return true;

		return false;

	}

	public static boolean isPrimitiveObject(String jaxType) {

		if (jaxType.equals("java.lang.Boolean")
				|| jaxType.equals("java.math.BigInteger")
				|| jaxType.equals("java.lang.Integer")
				|| jaxType.equals("java.lang.Short")
				|| jaxType.equals("java.lang.Byte")
				|| jaxType.equals("java.lang.Long")
				|| jaxType.equals("java.math.BigDecimal")
				|| jaxType.equals("java.lang.Double")
				|| jaxType.equals("java.lang.Float")
				|| jaxType.equals("java.lang.String")
				|| jaxType.equals("javax.xml.rpc.namespace.QName"))
			return true;

		return false;

	}
//	Not all wrapper, only for Jade types
	public static String getWrapperName(String primitive) {
		if (primitive.equals("int")) {
			return "Integer";
		} else if (primitive.equals("float")) {
			return "Float";
		} else if (primitive.equals("long")) {
			return "Long";
		} else if (primitive.equals("double")) {
			return "Double";
		} else if (primitive.equals("boolean")) {
			return "Boolean";
		}
		return null;
	}

	public static boolean isArray(String jaxType) //Not include byte[]
	{
		if (jaxType.indexOf("[") > 0 && (!jaxType.startsWith("byte[")))
			return true;
		return false;
	}

}