/*
 * Cart.java
 *
 * Created on 7 February 2005, 16:50
 */

package ciamas.ws2jadedemo.ontology;

import jade.content.*;

/**
 *
 * @author  403515
 */
public class Cart implements Concept {
    
    private String cartId = null;
    private String purchaseUrl = null;
    private ciamas.ws2jadedemo.ontology.SearchResults items = null;
    private String HMAC = null;
    
    /** Creates a new instance of Cart */
    public Cart() {
    }
    
    public void setCartId(String cartId) {
        this.cartId = cartId;
    }
    
    public String getCartId() {
        return cartId;
    }
    
     public void setPurchaseUrl(String purchaseUrl) {
        this.purchaseUrl = purchaseUrl;
    }
    
    public String getPurchaseUrl() {
        return purchaseUrl;
    }
    
     public void setHMAC(String HMAC) {
        this.HMAC = HMAC;
    }
    
    public String getHMAC() {
        return HMAC;
    }
    
     public void setItems(ciamas.ws2jadedemo.ontology.SearchResults items) {
        this.items = items;
    }
    
    public ciamas.ws2jadedemo.ontology.SearchResults getItems() {
        return items;
    }
    
}
