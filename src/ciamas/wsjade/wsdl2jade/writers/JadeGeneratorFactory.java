/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.*;
import javax.wsdl.Binding;
import javax.wsdl.Definition;
import javax.wsdl.Message;
import javax.wsdl.PortType;
import javax.wsdl.Service;
import org.apache.axis.wsdl.toJava.*;
import org.apache.axis.utils.Messages;
import org.apache.axis.wsdl.gen.Generator;

import org.apache.axis.wsdl.gen.NoopGenerator;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.MessageEntry;
import org.apache.axis.wsdl.symbolTable.PortTypeEntry;
import org.apache.axis.wsdl.symbolTable.ServiceEntry;
import org.apache.axis.wsdl.symbolTable.SymTabEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.symbolTable.TypeEntry;
import org.apache.axis.wsdl.toJava.JavaDefinitionWriter;


import ciamas.wsjade.wsdl2jade.utils.JadeCacheGenerator;

public class JadeGeneratorFactory extends JavaGeneratorFactory{//implements GeneratorFactory {
	
    /** Field symbolTable */
    //protected SymbolTable symbolTable;

   
	public JadeGeneratorFactory() {
		addGenerators();
	}

	public JadeGeneratorFactory(OntParser ontParser) {
		this.emitter = ontParser;
		addGenerators();
	}

	public void setEmitter(OntParser ontParser) {
		this.emitter = ontParser;
	}

	private void addGenerators() {

		addDefinitionGenerators();
	} // addGenerators

	protected void addDefinitionGenerators() {
		addGenerator(Definition.class, JavaDefinitionWriter.class); // for
		// faults
		
	} // addDefinitionGenerators

	public void generatorPass(Definition def, SymbolTable symbolTable) {
		this.symbolTable = symbolTable;
		javifyNames(symbolTable);
		//setFaultContext(symbolTable);
		resolveNameClashes(symbolTable);
		determineInterfaceNames(symbolTable);
		if (emitter.isAllWanted()) {
			setAllReferencesToTrue();
		} else {
			ignoreNonSOAPBindings(symbolTable);
		}
		constructSignatures(symbolTable);
		determineIfHoldersNeeded(symbolTable);
	} // generatorPass

	

	private Writers messageWriters = new Writers();

	public Generator getGenerator(Message message, SymbolTable symbolTable) {

		MessageEntry mEntry = symbolTable.getMessageEntry(message.getQName());
		messageWriters.addStuff(new JadeCacheGenerator(), mEntry, symbolTable);
		return messageWriters;
	} // getGenerator

	/**
	 * Return Wsdl2java's JavaPortTypeWriter object.
	 */
	private Writers portTypeWriters = new Writers();

	public Generator getGenerator(PortType portType, SymbolTable symbolTable) {
		PortTypeEntry ptEntry = symbolTable.getPortTypeEntry(portType
				.getQName());

		portTypeWriters
				.addStuff(new JadeCacheGenerator(), ptEntry, symbolTable);
		return portTypeWriters;
	} // getGenerator

	/**
	 * Return Wsdl2java's JavaBindingWriter object.
	 */
	protected Writers bindingWriters = new Writers();

	public Generator getGenerator(Binding binding, SymbolTable symbolTable) {
		Generator writer = new JadeBindingWriter((OntParser)emitter, binding,
				symbolTable);
		BindingEntry bEntry = symbolTable.getBindingEntry(binding.getQName());

		bindingWriters.addStuff(writer, bEntry, symbolTable);
		return bindingWriters;
	} // getGenerator

	/**
	 * Return Wsdl2java's JavaServiceWriter object.
	 */
	protected Writers serviceWriters = new Writers();

	public Generator getGenerator(Service service, SymbolTable symbolTable) {

		ServiceEntry sEntry = symbolTable.getServiceEntry(service.getQName());
		JadeCacheGenerator cacheGenerator = new JadeCacheGenerator(
				JadeCacheGenerator.JAX_SERVICE_STUB, sEntry.getName(), sEntry,
				symbolTable);
		serviceWriters.addStuff(cacheGenerator, sEntry, symbolTable);
		return serviceWriters;
	} // getGenerator

	/**
	 * Return Wsdl2java's JavaTypeWriter object.
	 */
	private Writers typeWriters = new Writers();

	public Generator getGenerator(TypeEntry type, SymbolTable symbolTable) {
		Generator writer = new JadeTypeWriter(emitter, type, symbolTable);
		typeWriters.addStuff(writer, type, symbolTable);

		return typeWriters;
	} // getGenerator

	public Generator getGenerator(Definition definition, SymbolTable symbolTable) {

		return new NoopGenerator();
	} // getGenerator

	
	protected class Writers implements Generator {
		Vector writers = new Vector();

		SymbolTable symbolTable = null;

		Generator baseWriter = null;

		// entry or def, but not both, will be a parameter.
		SymTabEntry entry = null;

		Definition def = null;

		public void addGenerator(Class writer) {
			writers.add(writer);
		} // addWriter

		public void addStuff(Generator baseWriter, SymTabEntry entry,
				SymbolTable symbolTable) {
			this.baseWriter = baseWriter;
			this.entry = entry;
			this.symbolTable = symbolTable;
		} // addStuff

		public void addStuff(Generator baseWriter, Definition def,
				SymbolTable symbolTable) {
			this.baseWriter = baseWriter;
			this.def = def;
			this.symbolTable = symbolTable;
		} // addStuff

		public void generate() throws IOException {
			if (baseWriter != null) {
				baseWriter.generate();
			}
			Class[] formalArgs = null;
			Object[] actualArgs = null;
			if (entry != null) {
				formalArgs = new Class[] { OntParser.class, entry.getClass(),
						SymbolTable.class };
				actualArgs = new Object[] { emitter, entry, symbolTable };
			} else {
				formalArgs = new Class[] { OntParser.class, Definition.class,
						SymbolTable.class };
				actualArgs = new Object[] { emitter, def, symbolTable };
			}
			for (int i = 0; i < writers.size(); ++i) {
				Class wClass = (Class) writers.get(i);
				Generator gen = null;
				try {
					Constructor ctor = wClass.getConstructor(formalArgs);
					gen = (Generator) ctor.newInstance(actualArgs);
				} catch (Throwable t) {
					throw new IOException(Messages.getMessage("exception01", t
							.getMessage()));
				}
				gen.generate();
			}
		} // generate
	} // class Writers


   


} // class JadeGeneratorFactory
