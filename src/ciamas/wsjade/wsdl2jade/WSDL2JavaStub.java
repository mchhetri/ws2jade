/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade;

import java.util.List;

import org.apache.axis.utils.CLArgsParser;
import org.apache.axis.utils.CLOption;
import org.apache.axis.utils.Messages;
import org.apache.axis.wsdl.WSDL2Java;

import ciamas.wsjade.management.Status;
import org.apache.log4j.Logger;

//This class is to solve the restriction of invoking WSDL2Java.run outside the
// its package and system exit
public class WSDL2JavaStub extends WSDL2Java {
	public WSDL2JavaStub() {
		super();
	}

	//Following is exact Axis code, only get rid of the System termination code
	public void run(String[] args) {
		Logger log = Logger.getLogger("WS2JADE");

		// Parse the arguments
		CLArgsParser argsParser = new CLArgsParser(args, options);

		// Print parser errors, if any
		if (null != argsParser.getErrorString()) {
			log.debug(Messages.getMessage("error01", argsParser
					.getErrorString()));
			printUsage();
		}

		// Get a list of parsed options
		List clOptions = argsParser.getArguments();
		int size = clOptions.size();

		try {
			// Parse the options and configure the emitter as appropriate.
			for (int i = 0; i < size; i++) {
				parseOption((CLOption) clOptions.get(i));
			}

			// validate argument combinations
			//
			validateOptions();

			parser.run(wsdlURI);
			Status.setStatus(wsdlURI, Status.JAVA_STUB_GENERATION_PARAM, true);

			// everything is good
			//Dont bother to exit here
		} catch (Throwable t) {
			Status.setStatus(wsdlURI, Status.JAVA_STUB_GENERATION_PARAM, false);
			log.debug("Exception occurred", t);
			//t.printStackTrace();
			//Dont bother to exit
		}
	}

	protected void printUsage() {
	}
}