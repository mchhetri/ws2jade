/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.management;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class ActionStatus {
	

	private volatile boolean isJavaStubGeneration = false;

	private volatile boolean isJavaOntoGeneration = false;

	private volatile boolean isJarStubGeneration = false;

	private volatile boolean isJarOntoGeneration = false;

	public ActionStatus() {

	}

	/**
	 * @return Returns the isJarOntoGeneration.
	 */
	public boolean isJarOntoGeneration() {
		return isJarOntoGeneration;
	}

	/**
	 * @param isJarOntoGeneration
	 *            The isJarOntoGeneration to set.
	 */
	public void setJarOntoGeneration(boolean isJarOntoGeneration) {
		this.isJarOntoGeneration = isJarOntoGeneration;
	}

	/**
	 * @return Returns the isJarStubGeneration.
	 */
	public boolean isJarStubGeneration() {
		return isJarStubGeneration;
	}

	/**
	 * @param isJarStubGeneration
	 *            The isJarStubGeneration to set.
	 */
	public void setJarStubGeneration(boolean isJarStubGeneration) {
		this.isJarStubGeneration = isJarStubGeneration;
	}

	/**
	 * @return Returns the isJavaOntoGeneration.
	 */
	public boolean isJavaOntoGeneration() {
		return isJavaOntoGeneration;
	}

	/**
	 * @param isJavaOntoGeneration
	 *            The isJavaOntoGeneration to set.
	 */
	public void setJavaOntoGeneration(boolean isJavaOntoGeneration) {
		this.isJavaOntoGeneration = isJavaOntoGeneration;
	}

	/**
	 * @return Returns the isJavaStubGeneration.
	 */
	public boolean isJavaStubGeneration() {
		return isJavaStubGeneration;
	}

	/**
	 * @param isJavaStubGeneration
	 *            The isJavaStubGeneration to set.
	 */
	public void setJavaStubGeneration(boolean isJavaStubGeneration) {
		this.isJavaStubGeneration = isJavaStubGeneration;
	}

}