/*
 * Created on 4/02/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ciamas.wsjade.management;

/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Authorization {
	
	private static String username;
	private static String password;

	
	/**
	 * @return Returns the password.
	 */
	public static String getPassword() {
		return password;
	}
	/**
	 * @param password The password to set.
	 */
	public static void setPassword(String password) {
		Authorization.password = password;
	}
	/**
	 * @return Returns the username.
	 */
	public static String getUsername() {
		return username;
	}
	/**
	 * @param username The username to set.
	 */
	public static void setUsername(String username) {
		Authorization.username = username;
	}
}
