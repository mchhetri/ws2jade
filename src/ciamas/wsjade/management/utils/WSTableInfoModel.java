/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.management.utils;

import java.util.ArrayList;

import javax.swing.table.AbstractTableModel;

import ciamas.wsjade.management.AGWSRecord;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class WSTableInfoModel extends AbstractTableModel {
	protected ArrayList data = null;

	protected static int NUM_COLUMNS = 2;

	protected static int START_NUM_ROWS = 0;

	protected int nextEmptyRow = 0;

	protected int numRows = 0;

	static final public String agentName = "Agent Nickname";

	static final public String wsURI = "Webservice Address";

	public WSTableInfoModel() {
		data = new ArrayList();

	}

	public Class getColumnClass(int column) {
		return java.lang.String.class;
	}

	public String getColumnName(int column) {
		switch (column) {
		case 0:
			return agentName;
		case 1:
			return wsURI;

		}
		return "";
	}

	//XXX Should this really be synchronized?
	public synchronized int getColumnCount() {
		return NUM_COLUMNS;
	}

	public synchronized int getRowCount() {

		return START_NUM_ROWS;

	}

	public synchronized Object getValueAt(int row, int column) {
		try {
			AGWSRecord p = (AGWSRecord) data.get(row);
			switch (column) {
			case 0:
				return p.getAgentName();
			case 1:
				return p.getWsURL();

			}
		} catch (Exception e) {
		}
		return "";
	}

	public synchronized void addData(AGWSRecord record) {
		if(getTableModelListeners() == null)
			return;
		
		START_NUM_ROWS = START_NUM_ROWS + 1;
		data.add(record);
		//Notify listeners that the data changed.
		int lastRow = data.size();
		fireTableRowsInserted(lastRow, lastRow);
		fireTableRowsUpdated(lastRow, lastRow);
		//this.fireTableDataChanged();
	}

	public synchronized void removeData(AGWSRecord record) {
		if(getTableModelListeners() == null)
			return;
		START_NUM_ROWS = START_NUM_ROWS - 1;
		for (int i = 0; i < data.size(); i++) {
			AGWSRecord a = (AGWSRecord) data.get(i);
			if (a.getAgentName().equals(record.getAgentName())
					&& a.getWsURL().equals(record.getWsURL())) {
				data.remove(i);
				this.fireTableRowsDeleted(i, data.size() - 1);
				this.fireTableRowsUpdated(i, data.size() - 1);
			}

		}

	}

	public synchronized void removeData(String agentName) {
		if(getTableModelListeners() == null)
			return;
		int old_counts = START_NUM_ROWS;
		int updateFrom = 0;
		int back_index = 0;
		for (int i = 0; i < old_counts; i++) {
			AGWSRecord a = (AGWSRecord) data.get(i - back_index);
			if (a.getAgentName().equals(agentName)) {
				if (updateFrom == 0)
					updateFrom = i;
				System.out.println("Remove");
				data.remove(i - back_index);
				back_index++;
				START_NUM_ROWS = START_NUM_ROWS - 1;
				this.fireTableRowsDeleted(i, i);

			}

		}
		this.fireTableRowsUpdated(updateFrom, old_counts);

	}

	public synchronized void clear() {
		int oldNumRows = numRows;

		numRows = START_NUM_ROWS;
		data.clear();
		nextEmptyRow = 0;

		if (oldNumRows > START_NUM_ROWS) {
			fireTableRowsDeleted(START_NUM_ROWS, oldNumRows - 1);
		}
		fireTableRowsUpdated(0, START_NUM_ROWS - 1);
	}

}