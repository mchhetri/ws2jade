/*
 * CheckoutDialog.java
 *
 * Created on 11 February 2005, 11:37
 */

package ciamas.ws2jadedemo.gui;

import jade.gui.GuiEvent;

/**
 *
 * @author  403515
 */
public class CheckoutDialog extends javax.swing.JDialog {
    
    private ciamas.ws2jadedemo.client.ClientAgent agent;
    
    /** Creates new form CheckoutDialog */
    public CheckoutDialog(java.awt.Frame parent, boolean modal, ciamas.ws2jadedemo.client.ClientAgent agent) {
        super(parent, modal);
        this.agent = agent;
        initComponents();
    }
    
    public void setAmount(String amount) {
        amountField.setText(amount);
    }
    
    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    private void initComponents() {//GEN-BEGIN:initComponents
        tabbedPane = new javax.swing.JTabbedPane();
        deliveryDetails = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        streetNumberField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        streetNameField = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        suburbField = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        stateField = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        postcodeField = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        countryField = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        deliveryDateField = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        preferredTimeField = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        descriptionField = new javax.swing.JTextField();
        paymentDetails = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        emailField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        passwordField = new javax.swing.JPasswordField();
        jLabel12 = new javax.swing.JLabel();
        recipientEmailField = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        amountField = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        currencyPane = new javax.swing.JScrollPane();
        currencyList = new javax.swing.JList();
        jLabel15 = new javax.swing.JLabel();
        noteField = new javax.swing.JTextField();
        transactionButton = new javax.swing.JButton();

        setTitle("Checkout");
        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        deliveryDetails.setLayout(new java.awt.GridLayout(9, 2));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Street Number");
        deliveryDetails.add(jLabel3);

        streetNumberField.setText("302");
        deliveryDetails.add(streetNumberField);

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Street Name");
        deliveryDetails.add(jLabel4);

        streetNameField.setText("Cotham Road");
        deliveryDetails.add(streetNameField);

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Suburb");
        deliveryDetails.add(jLabel5);

        suburbField.setText("Kew");
        deliveryDetails.add(suburbField);

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("State");
        deliveryDetails.add(jLabel7);

        stateField.setText("Vic");
        deliveryDetails.add(stateField);

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Postcode");
        deliveryDetails.add(jLabel6);

        postcodeField.setText("3101");
        deliveryDetails.add(postcodeField);

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Country");
        deliveryDetails.add(jLabel8);

        countryField.setText("Australia");
        deliveryDetails.add(countryField);

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Delivery Date");
        deliveryDetails.add(jLabel9);

        deliveryDateField.setText("Friday, 11 February 2005");
        deliveryDetails.add(deliveryDateField);

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Preferred Delivery Time Period");
        deliveryDetails.add(jLabel10);

        preferredTimeField.setText("13:00 - 14:30");
        deliveryDetails.add(preferredTimeField);

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("Description");
        deliveryDetails.add(jLabel11);

        descriptionField.setText("Amazon.Com purchase");
        deliveryDetails.add(descriptionField);

        tabbedPane.addTab("Delivery Details", deliveryDetails);

        paymentDetails.setLayout(new java.awt.GridLayout(6, 2));

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("Email Address");
        paymentDetails.add(jLabel1);

        emailField.setText("user@fake.com");
        paymentDetails.add(emailField);

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Password");
        paymentDetails.add(jLabel2);

        passwordField.setText("password");
        paymentDetails.add(passwordField);

        jLabel12.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel12.setText("Recipient Email Address");
        paymentDetails.add(jLabel12);

        recipientEmailField.setText("dummy@amazon.com");
        paymentDetails.add(recipientEmailField);

        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel13.setText("Amount");
        paymentDetails.add(jLabel13);

        amountField.setEditable(false);
        amountField.setText("0.0");
        paymentDetails.add(amountField);

        jLabel14.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel14.setText("Currency");
        paymentDetails.add(jLabel14);

        String[] currencies = {"USD", "AUD", "EUR", "HKD", "NZD", "VND"};
        currencyList.setListData(currencies);
        currencyList.setSelectedIndex(0);
        currencyPane.setViewportView(currencyList);

        paymentDetails.add(currencyPane);

        jLabel15.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel15.setText("Notes");
        paymentDetails.add(jLabel15);

        noteField.setText("Amazon.Com payment");
        paymentDetails.add(noteField);

        tabbedPane.addTab("Payment Details", paymentDetails);

        getContentPane().add(tabbedPane, java.awt.BorderLayout.CENTER);

        transactionButton.setText("Complete Transaction");
        transactionButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                transactionButtonActionPerformed(evt);
            }
        });

        getContentPane().add(transactionButton, java.awt.BorderLayout.SOUTH);

        java.awt.Dimension screenSize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        setBounds((screenSize.width-400)/2, (screenSize.height-300)/2, 400, 300);
    }//GEN-END:initComponents

    private void transactionButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_transactionButtonActionPerformed
        if (amountField.getText() == null || amountField.getText().equals("")
            || countryField.getText() == null || countryField.getText().equals("")
            || currencyList.getSelectedValue() == null
            || deliveryDateField.getText() == null || deliveryDateField.getText().equals("")
            || descriptionField.getText() == null || descriptionField.getText().equals("")
            || emailField.getText() == null || emailField.getText().equals("")
            || noteField.getText() == null || noteField.getText().equals("")
            || passwordField.getText() == null || passwordField.getText().equals("")
            || postcodeField.getText() == null || postcodeField.getText().equals("")
            || preferredTimeField.getText() == null
            || recipientEmailField.getText() == null || recipientEmailField.getText().equals("")
            || stateField.getText() == null || stateField.getText().equals("")
            || streetNameField.getText() == null || streetNameField.getText().equals("")
            || streetNumberField.getText() == null || streetNumberField.getText().equals("")
            || suburbField.getText() == null || suburbField.getText().equals("")) {
            javax.swing.JOptionPane.showMessageDialog(this, "Must complete all fields!", 
                "Error", javax.swing.JOptionPane.ERROR_MESSAGE); 
        }
        else {
            // Construct a Payment object to send to Client Agent
            ciamas.ws2jadedemo.ontology.Payment payment = new ciamas.ws2jadedemo.ontology.Payment();
            payment.setAmount(amountField.getText());
            payment.setCurrency((String)currencyList.getSelectedValue());
            payment.setEmailAddress(emailField.getText());
            payment.setNote(noteField.getText());
            String pw = new String(passwordField.getPassword());
            System.out.println("PayFriend/GlobalTransports duel User Name: " + emailField.getText());
            System.out.println("PayFriend/GlobalTransports duel Password: " + pw);
            payment.setPassword(pw);
            payment.setRecipientEmailAddress(recipientEmailField.getText());
            // Construct a Delivery object to send to Client Agent
            ciamas.ws2jadedemo.ontology.Delivery delivery = new ciamas.ws2jadedemo.ontology.Delivery();
            delivery.setCountry(countryField.getText());
            delivery.setDeliveryDate(deliveryDateField.getText());
            delivery.setDescription(descriptionField.getText());
            delivery.setEmailAddress(emailField.getText());
            delivery.setPassword(pw);
            delivery.setPostcode(postcodeField.getText());
            delivery.setPreferredDeliveryTimePeriod(preferredTimeField.getText());
            delivery.setState(stateField.getText());
            delivery.setStreetName(streetNameField.getText());
            delivery.setStreetNumber(streetNumberField.getText());
            delivery.setSuburb(suburbField.getText());
            // Post delivery and payment to the client agent
            GuiEvent ge = new GuiEvent(this, agent.CHECKOUT);
            ge.addParameter(payment);
            ge.addParameter(delivery);
            agent.postGuiEvent(ge);
            this.dispose();
        }
    }//GEN-LAST:event_transactionButtonActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField amountField;
    private javax.swing.JTextField countryField;
    private javax.swing.JList currencyList;
    private javax.swing.JScrollPane currencyPane;
    private javax.swing.JTextField deliveryDateField;
    private javax.swing.JPanel deliveryDetails;
    private javax.swing.JTextField descriptionField;
    private javax.swing.JTextField emailField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JTextField noteField;
    private javax.swing.JPasswordField passwordField;
    private javax.swing.JPanel paymentDetails;
    private javax.swing.JTextField postcodeField;
    private javax.swing.JTextField preferredTimeField;
    private javax.swing.JTextField recipientEmailField;
    private javax.swing.JTextField stateField;
    private javax.swing.JTextField streetNameField;
    private javax.swing.JTextField streetNumberField;
    private javax.swing.JTextField suburbField;
    private javax.swing.JTabbedPane tabbedPane;
    private javax.swing.JButton transactionButton;
    // End of variables declaration//GEN-END:variables
    
}
