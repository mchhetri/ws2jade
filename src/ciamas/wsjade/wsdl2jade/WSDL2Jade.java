/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade;

import java.util.Vector;

import org.apache.axis.wsdl.gen.Parser;
import org.apache.axis.wsdl.gen.WSDL2;

import ciamas.wsjade.management.Status;
import ciamas.wsjade.wsdl2jade.utils.Common;
import ciamas.wsjade.wsdl2jade.writers.OntParser;
//import org.apache.log4j.Level;
import org.apache.log4j.Logger;

/**
 * Command line interface to the WSDL2Jade utility
 *  
 */
public class WSDL2Jade extends WSDL2 {
	private OntParser ontParser;

	public WSDL2Jade() {
		ontParser = (OntParser) parser;

	}

	protected Parser createParser() {
		return new OntParser();
	}

	public static void printInfo() {
		//The wsdl URI was not specified.
		System.out
				.println("Usage:  java ciamas.wsjade.wsdl2jade.WSDL2Jade [options] WSDL-URI");
		System.out.println("Options:");
		System.out.println("	-h, --help");
		System.out.println("		print this message and exit");
		System.out.println("	-n, --noImports");
		System.out
				.println("		only generate code for the immediate WSDL document");
		System.out.println("	-W, --noWrapped");
		System.out
				.println("		turn off support for \"wrapped\" document/literal");
		System.out.println("	-r, --rpcPackage <argument>");
		System.out.println("		package for jax-rpc stubs");
		System.out.println("	-j, --jadePackage <argument>");
		System.out.println("		package for generated jade, default: JadeGen");
		System.out.println("	-S, --serviceName <argument>");
		System.out
				.println("		service name assigned by the agent, default: GenericService");
		System.out.println("	-a, --agentOut <argument>");
		System.out
				.println("		output folder for all agent related files, default: output");
		System.out.println("	-s, --stubOut <argument>");
		System.out
				.println("		output folder for all web service stubs, default: output");
		System.out.println("	-U, --user <argument>");
		System.out.println("		 username to access the WSDL-URI");
		System.out.println("	-P, --password <argument>");
		System.out.println("		 password to access the WSDL-URI");

	}

	public void printUsage() {
		//printUsage();
	}

	public static void main(String args[]) {

		Vector newArgs = new Vector();
		//newArgs.addElement( "-O");
		//newArgs.addElement("-1");

		if (args.length < 1) {
			printInfo();
		} else {
			int i = 0;
			boolean hasOutput = false;
			while (i < args.length) {
				String arg = args[i];
				if (arg.equals("-h") || arg.equals("--help")) {
					printInfo();
					i = i + 1;
				} else if (arg.equals("-n") || arg.equals("--noImports")) {

					newArgs.addElement("-n");
					i = i + 1;

				} else if (arg.equals("-W") || arg.equals("--noWrapped")) {

					newArgs.addElement("-W");
					i = i + 1;
				} else if (arg.equals("-r") || arg.equals("--rpcPackage")) {

					newArgs.addElement("-p");
					newArgs.addElement(args[i + 1]);

					Common.setJaxGenRootPkg(args[i + 1]);
					i = i + 2;

				} else if (arg.equals("-j") || arg.equals("--jadePackage")) {
					Common.setJadeGenRootPkg(args[i + 1]);
					i = i + 2;
				} else if (arg.equals("-S") || arg.equals("--serviceName")) {
					Common.setServiceName(args[i + 1]);
					i = i + 2;
				} else if (arg.equals("-a") || arg.equals("--agentOut")) {
					Common.setJadeOutputDir(args[i + 1]);
					i = i + 2;
				} else if (arg.equals("-s") || arg.equals("--stubOut")) {
					newArgs.addElement("-o");
					newArgs.addElement(args[i + 1]);
					Common.setJaxOutputDir(args[i + 1]);
					hasOutput = true;
					i = i + 2;
				} else
					i = i + 1;

			}

			String wsdlURL = args[args.length - 1];
			newArgs.addElement(wsdlURL);

			if (!hasOutput) {
				newArgs.addElement("-o");
				newArgs.addElement(Common.getJaxOutputDir());
			}

			//Convert to wsdl2java args
			String[] wsdl2javaArgs = new String[newArgs.size()];

			for (int k = 0; k < wsdl2javaArgs.length; k++) {
				wsdl2javaArgs[k] = (String) newArgs.elementAt(k);
				if ((wsdl2javaArgs[k]).equals("-o"))
					hasOutput = true;

			}

			//Register
			Common.setServiceURL(wsdlURL);
			Status.registerActionStatus(wsdlURL);

			//Parse to Stubs code
			WSDL2JavaStub wsdl2java = new WSDL2JavaStub();
			wsdl2java.run(wsdl2javaArgs);
			//Parse to Agent code
			WSDL2Jade wsdl2Jade = new WSDL2Jade();
			wsdl2Jade.run(wsdlURL);

		}

	}

	public void run(String args) {
		Logger log = Logger.getLogger("WS2JADE");
		try {

			parser.run(args);
			Status.setStatus(args, Status.JAVA_ONTO_GENERATION_PARAM, true);
			//Dont exit the system
		} catch (Exception e) {
			//e.printStackTrace();
			log.info("Fail when parsing WSDL to JADE");
			log.debug("Exception occurred", e);
			
		}

	}

}