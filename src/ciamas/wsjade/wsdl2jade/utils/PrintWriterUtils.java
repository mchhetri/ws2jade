/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.utils;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.Writer;

public class PrintWriterUtils extends PrintWriter {
	private int tabNum;

	private String tabSpaces = "   ";

	public PrintWriterUtils(OutputStream out) {
		super(out);
	}

	public PrintWriterUtils(OutputStream out, boolean autoFlush) {
		super(out, autoFlush);
	}

	public PrintWriterUtils(Writer out) {
		super(out);
	}

	public PrintWriterUtils(Writer out, boolean autoFlush) {
		super(out, autoFlush);
	}

	public void setTabNum(int i) {
		tabNum = i;
	}

	public int getTabNum() {
		return tabNum;
	}

	public void increaseTab() {
		tabNum++;
	}

	public void decreaseTab() {
		tabNum--;
	}

	public void println(String s) {
		String newString = "";
		for (int i = 0; i < tabNum; i++)
			newString = tabSpaces + newString;
		newString = newString + s;

		super.println(newString);
	}
}