/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.utils;

import java.io.PrintWriter;

import ciamas.wsjade.wsdl2jade.writers.OntSchemaTemp;

public class BeanUtils {

	private String b_type;

	private String b_name;

	private String b_realType;

	public BeanUtils(String type, String realType, String name) {
		b_type = type;
		b_name = name;
		b_realType = realType;

	}

	public void register(String clName) {
		OntSchemaTemp ontTemp = OntSchemaTemp.getInstance();

		//Register the ontology concept for later writing
		String s = b_realType;
		int i = -1;
		if ((i = s.indexOf("[")) >= 0)
			s = s.substring(0, i);

		ontTemp.register(b_name, b_type, s, clName);
		//System.out.println(b_type+ " " + b_realType+ " " +clName);
	}

	public String getType() {
		return b_type;
	}

	public String getName() {
		return b_name;
	}

	public void writeBean(PrintWriter pw) {

		pw.println("public void set" + upperFirstLetter(b_name) + "(" + b_type
				+ " " + b_name + ")");
		pw.println("{");
		pw.println("this." + b_name + " = " + b_name + ";");
		pw.println("}");
		pw.println();

		pw.println("public " + b_type + " get" + upperFirstLetter(b_name)
				+ "()");
		pw.println("{");
		pw.println("return " + b_name + ";");
		pw.println("}");

	}

	//Utilities
	public static String upperFirstLetter(String name) {
		if (name == null || name.equals(""))
			return name;

		char start = name.charAt(0);

		if (Character.isLowerCase(start)) {
			start = Character.toUpperCase(start);
			return start + name.substring(1);
		}
		return name;
	}

	public static String lowerFirstLetter(String name) {
		if (name == null || name.equals(""))
			return name;

		char start = name.charAt(0);

		if (Character.isUpperCase(start)) {
			start = Character.toLowerCase(start);
			return start + name.substring(1);
		}
		return name;
	}

}