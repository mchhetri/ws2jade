/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.wsdl2jade.projection.owl;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class Temp {

	private StringBuffer temp = new StringBuffer();

	private static Temp me = null;

	private String header = "<?xml version=\"1.0\" encoding=\"ISO-8859-1\" ?>\n"
			+ "<!DOCTYPE uridef[\n"
			+ "	<!ENTITY rdf \"http://www.w3.org/1999/02/22-rdf-syntax-ns\">\n"
			+ "	<!ENTITY rdfs \"http://www.w3.org/2000/01/rdf-schema\">\n"
			+ "	<!ENTITY owl \"http://www.w3.org/2002/07/owl\">\n"
			+ "	<!ENTITY xsd  \"http://www.w3.org/2001/XMLSchema\">\n"
			+ "]>\n"
			+ "<rdf:RDF \n"
			+ "	xmlns:rdf=     \"&rdf;#\"\n"
			+ "	xmlns:rdfs=    \"&rdfs;#\"\n"
			+ "	xmlns:owl=    \"&owl;#\"\n"
			+ "	xmlns:xsd=     \"&xsd;\"\n"
			+ ">\n"
			+ "<owl:Ontology about=\"\">\n"
			+ "	<owl:versionInfo></owl:versionInfo> \n"
			+ "	<rdfs:comment>   ---ADD INFO---</rdfs:comment> \n"
			+ "	<owl:imports rdf:resource=\"&rdf;\" />\n"
			+ "	<owl:imports rdf:resource=\"&rdfs;\" />\n"
			+ "	<owl:imports rdf:resource=\"&owl;\" />\n\n"
			+ "</owl:Ontology>\n\n\n"
			+ "<owl:Class rdf:ID=\"AccessoryArray\">\n"
			+ "    <owl:subClassOf rdf:resource=\"http://www.w3.org/2002/07/owl#List\"/>\n"
			+ "    <owl:subClassOf>\n"
			+ "        <owl:Restriction>\n"
			+ "        <owl:onProperty rdf:resource=\"http://www.w3.org/2002/07/owl#item\"/>\n"
			+ "        <owl:toClass rdf:resource=\"&xsd;#string\"/>\n"
			+ "    </owl:Restriction>\n"
			+ "    </owl:subClassOf>\n"
			+ "</owl:Class>\n\n</rdf:RDF>";

	public static Temp getInstance() {
		if (me == null) {
			me = new Temp();
			me.reset();
		}
		return me;
	}

	public void reset() {
		temp = null;
		temp = new StringBuffer();
		temp.append(header);
	}

	public void addText(String s) {
		temp.insert(temp.length() - 11, s);
	}

	public String getText() {
		return temp.toString();
	}
}