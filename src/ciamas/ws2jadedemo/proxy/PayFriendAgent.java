/*
 * PayFriendAgent.java
 *
 * Created on 31 January 2005, 10:58
 */

package ciamas.ws2jadedemo.proxy;

import jade.content.*;
import jade.content.onto.*;
import jade.core.*;
import jade.lang.acl.*;
import jade.content.onto.basic.*;
import jade.content.lang.*;
import jade.content.lang.sl.*;
import jade.proto.AchieveREResponder;
import jade.proto.AchieveREInitiator;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import java.util.Vector;

/**
 * This agent communicates with PayFriend web services. It does so by coordinating with a
 * CIAMAS WS2Jade WSAG via an automatically generated ontology.
 * @author 403515
 */
//TODO: Add log4j
public class PayFriendAgent extends Agent {
    
    private ContentManager manager;
    private Codec codec;
    private Ontology ontology;
    private Ontology commonOntology;
    
    /** Creates a new instance of PayFriendAgent */
    public PayFriendAgent() {
        System.out.println("Initialising Agent...");
        manager = (ContentManager)getContentManager();
        codec = new SLCodec();
        ontology = JadeGen.onto.PayFriendService.WebOntology.getInstance();
        commonOntology = ciamas.ws2jadedemo.ontology.CommonOntology.getInstance();
    }
    
    public void setup() {
        System.out.println("Agent " + getAID().getLocalName() + " is ready.");
        // Register the ontology
        manager.registerLanguage(codec);
        manager.registerOntology(ontology);
        manager.registerOntology(commonOntology);
        MessageTemplate template = MessageTemplate.and(
            MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
            MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
        AchieveREResponder responderBehaviour = new AchieveREResponder(this, template) {
            protected ACLMessage prepareResponse(ACLMessage request) throws NotUnderstoodException, RefuseException {
                System.out.println("REQUEST received from " + request.getSender().getName() + ".");
                ACLMessage agree = request.createReply();
                agree.setPerformative(ACLMessage.AGREE);
                return agree;
            }
        };
        // Register an AchieveREInitiator in the PREPARE_RESULT_NOTIFICATION state
        responderBehaviour.registerPrepareResultNotification(new AchieveREInitiator(this, null) {

            private AID responder = new AID("PayFriendProvider", AID.ISLOCALNAME);
            protected Vector prepareRequests(ACLMessage request) {
                Vector v = null;
                // Retrieve the incoming request from the DataStore
                String incomingRequestKey = (String)((AchieveREResponder)parent).REQUEST_KEY;
                ACLMessage incomingRequest = (ACLMessage)getDataStore().get(incomingRequestKey);
                // Prepare the request to forward to the responder
                System.out.println("Forwarding request to " + responder.getName() + ".");
                ACLMessage outgoingRequest = new ACLMessage(ACLMessage.REQUEST);
                outgoingRequest.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
                outgoingRequest.setLanguage(codec.getName());
                outgoingRequest.setOntology(ontology.getName());
                outgoingRequest.addReceiver(responder);
                ContentElement content = null;
                try {
                    content = manager.extractContent(incomingRequest);
                }
                catch (Exception e) {
                   // e.printStackTrace();
                }
                Concept action = ((Action)content).getAction();
                // If the request content is a Payment object replace it with a MakePaymentRequest
                // object
                if (action instanceof ciamas.ws2jadedemo.ontology.Payment) {
                    System.out.println("<PAYMENT REQUEST>");
                    ciamas.ws2jadedemo.ontology.Payment payment = (ciamas.ws2jadedemo.ontology.Payment)action;
                    JadeGen.onto.reflect.PayFriendNS.Payment payment_r =
                        new JadeGen.onto.reflect.PayFriendNS.Payment();
                    payment_r.setAmount(payment.getAmount());
                    payment_r.setCurrency(payment.getCurrency());
                    payment_r.setEmailAddress(payment.getEmailAddress());
                    payment_r.setNote(payment.getNote());
                    payment_r.setPassword(payment.getPassword());
                    payment_r.setRecipientEmailAddress(payment.getRecipientEmailAddress());
                    JadeGen.onto.PayFriendService.PayFriend.Request.MakePaymentRequest makePaymentRequest =
                        new JadeGen.onto.PayFriendService.PayFriend.Request.MakePaymentRequest();
                    makePaymentRequest.setP(payment_r);
                    try {
                        manager.fillContent(outgoingRequest, new Action(getAID(), makePaymentRequest));
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    outgoingRequest.setReplyByDate(incomingRequest.getReplyByDate());
                    v = new Vector(1);
                    v.addElement(outgoingRequest);
                }
                return v;
            }

            protected void handleInform(ACLMessage inform) {
                System.out.println("Received INFORM.");
                storeNotification(ACLMessage.INFORM, inform);
            }

            protected void handleRefuse(ACLMessage refuse) {
                System.out.println("Received REFUSE.");
                storeNotification(ACLMessage.FAILURE, null);
            }

            protected void handleNotUnderstood(ACLMessage notUnderstood) {
                System.out.println("Received NOT_UNDERSTOOD.");
                storeNotification(ACLMessage.FAILURE, null);
            }

            protected void handleFailure(ACLMessage failure) {
                System.out.println("Received FAILURE.");
                storeNotification(ACLMessage.FAILURE, null);
            }

            protected void handleAllResultNotifications(Vector notifications) {
                if (notifications.size() == 0) {
                    // Timeout
                    System.out.println("FAILURE.");
                    storeNotification(ACLMessage.FAILURE, null);
                }
            }

            private void storeNotification(int performative, ACLMessage m) {
                // Retrieve the incoming request from the DataStore
                String incomingRequestkey = (String)((AchieveREResponder)parent).REQUEST_KEY;
                ACLMessage incomingRequest = (ACLMessage) getDataStore().get(incomingRequestkey);
                // Prepare the notification to the request originator and store it in the DataStore
                ACLMessage notification = incomingRequest.createReply();
                notification.setPerformative(performative);
                ContentElement content = null;
                try {
                    content = manager.extractContent(m);
                }
                catch (Exception e) {
                    //e.printStackTrace();
                }
                if (content instanceof jade.content.onto.basic.Result) {
                    jade.content.onto.basic.Result result = (jade.content.onto.basic.Result)content;
                    // If msg content is an instanceof MakePaymentRequest make a call to
                    // a CheckPaymentDetails so we can get a receipt
                    if (result.getValue() instanceof JadeGen.onto.PayFriendService.PayFriend.Response.MakePaymentRequest) {
                        JadeGen.onto.PayFriendService.PayFriend.Response.MakePaymentRequest mpr = 
                            (JadeGen.onto.PayFriendService.PayFriend.Response.MakePaymentRequest)result.getValue();
                        System.out.println("<PAYMENT INFORM>");
                        System.out.println(mpr.getMakePaymentRequestReturn());
                        notification.setContent(mpr.getMakePaymentRequestReturn());
                        ACLMessage informAmazon = new ACLMessage(ACLMessage.INFORM);
                        informAmazon.setContent(mpr.getMakePaymentRequestReturn());
                        informAmazon.addReceiver(new AID("AmazonAgent", AID.ISLOCALNAME));
                        send(informAmazon);
                    }
                }
                String notificationkey = (String)((AchieveREResponder)parent).RESULT_NOTIFICATION_KEY;
                System.out.println("Informing requesting agent of results.");
                getDataStore().put(notificationkey, notification);
            }

        });
        addBehaviour(responderBehaviour);
    }
    
    public void takeDown() {
        System.out.println(getLocalName() + " is now shutting down.");
    }
    
}
