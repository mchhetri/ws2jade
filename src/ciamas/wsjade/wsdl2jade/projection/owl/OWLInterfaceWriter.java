/*
 * Created on 3/02/2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package ciamas.wsjade.wsdl2jade.projection.owl;
import java.io.IOException;
import java.util.Iterator;
import java.util.Vector;

import javax.wsdl.Operation;
import javax.wsdl.PortType;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.PortTypeEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;

import ciamas.wsjade.wsdl2jade.writers.OntParser;
/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class OWLInterfaceWriter implements Generator{
	protected PortType portType;

	protected OntParser ontParser;

	protected SymbolTable symbolTable;

	protected BindingEntry bEntry;

	protected Vector writers = new Vector();

	protected OWLInterfaceWriter(OntParser parser, PortTypeEntry ptEntry,
			BindingEntry bEntry, SymbolTable symbolTable) {

		this.portType = ptEntry.getPortType();
		this.ontParser = parser;
		this.symbolTable = symbolTable;
		this.bEntry = bEntry;

	}

	public void generate() throws IOException {
		setGenerators();

		for (int i = 0; i < writers.size(); i++) {
			if (writers.elementAt(i) instanceof OWLActionWriter)
				((OWLActionWriter) writers.elementAt(i)).generate();
			else if (writers.elementAt(i) instanceof OWLConceptWriter)
				((OWLConceptWriter) writers.elementAt(i)).generate();

		}

	}

	protected void setGenerators() {
		Iterator operations = portType.getOperations().iterator();
		while (operations.hasNext()) {
			Operation operation = (Operation) operations.next();
			createWriters(operation);
		}

	}
	private void createWriters(Operation operation) {
		

			writers.addElement(new OWLActionWriter(operation,
					portType, bEntry, symbolTable));

			writers.addElement(new OWLConceptWriter(operation,
					portType, bEntry, symbolTable));
		

	}

}
