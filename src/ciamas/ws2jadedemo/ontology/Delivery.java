/*
 * Delivery.java
 *
 * Created on 9 February 2005, 11:59
 */

package ciamas.ws2jadedemo.ontology;

import jade.content.*;

/**
 *
 * @author  403515
 */
public class Delivery implements Concept {
    
    private String emailAddress;
    private String password;
    
    private String streetNumber;
    private String streetName;
    private String suburb;
    private String postcode;
    private String state;
    private String country;
    
    private String deliveryDate;
    private String preferredDeliveryTimePeriod;
    
    private String description;
    
    /** Creates a new instance of Delivery */
    public Delivery() {
    }
    
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress; 
    }
    
    public String getEmailAddress() {
        return emailAddress;
    }
    
    public void setPassword(String password) {
        this.password = password; 
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setStreetNumber(String streetNumber) {
        this.streetNumber = streetNumber; 
    }
    
    public String getStreetNumber() {
        return streetNumber;
    }
    
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }
    
    public String getStreetName() {
        return streetName;
    }
    
    public void setSuburb(String suburb) {
        this.suburb = suburb;
    }
    
    public String getSuburb() {
        return suburb;
    }
    
    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }
    
    public String getPostcode() {
        return postcode;
    }
    
    public void setState(String state) {
        this.state = state;
    }
    
    public String getState() {
        return state;
    }
    
    public void setCountry(String country) {
        this.country = country;
    }
    
    public String getCountry() {
        return country;
    }
    
    public void setDeliveryDate(String deliveryDate) {
        this.deliveryDate = deliveryDate;
    }
    
    public String getDeliveryDate() {
        return deliveryDate;
    }
    
    public void setPreferredDeliveryTimePeriod(String preferredDeliveryTimePeriod) {
        this.preferredDeliveryTimePeriod = preferredDeliveryTimePeriod;
    }
    
    public String getPreferredDeliveryTimePeriod() {
        return preferredDeliveryTimePeriod;
    }
    
    public void setDescription(String description) {
        this.description = description;
    }
    
    public String getDescription() {
        return description;
    }
    
}
