/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.utils;

import jade.content.ContentElement;
import jade.content.ContentManager;
import jade.content.lang.Codec;
import jade.content.lang.sl.SLCodec;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.onto.basic.Action;
import jade.content.onto.basic.Result;
import jade.core.Agent;
import jade.core.behaviours.CyclicBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;
import jade.domain.DFService;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.DFAgentDescription;
import jade.domain.FIPAAgentManagement.ServiceDescription;
import java.io.File;
import java.lang.reflect.Method;

import java.util.NoSuchElementException;
import java.util.StringTokenizer;

import org.apache.log4j.Logger;

import ciamas.wsjade.management.utils.Utility;
import ciamas.wsjade.wsdl2jade.onto.Error;


public class WSAgent extends Agent {
	
	private ContentManager manager = (ContentManager) getContentManager();

	private Codec codec = new SLCodec();

	private Ontology adminOntology = ciamas.wsjade.wsdl2jade.onto.WebAdminOntology
			.getInstance();
	private Ontology wsOntology = ciamas.wsjade.wsdl2jade.onto.WSOntology
	.getInstance();

	protected DFAgentDescription dfd = new DFAgentDescription();
	protected void setup() {
		manager.registerLanguage(codec);
		manager.registerOntology(adminOntology);
		manager.registerOntology(wsOntology);
		try {
			DFService.register(this, dfd);
		}
		catch (Exception fe) {
			Logger.getLogger("WS2JADE").debug("Exception occurred", fe);
			//fe.printStackTrace();
		}
		//Ontologies are dynamicly pluggin later
		setEnabledO2ACommunication(true, 3);

		addBehaviour(new HandleRequestBehaviour(this));
	}

	public class HandleRequestBehaviour extends CyclicBehaviour {

		public HandleRequestBehaviour(Agent a) {
			super(a);
		}

		private void addOnto(AddOntAction ce) throws Exception {
			String ontClassName = null;
			//TO DO: Should check first if this request is from manger

			ontClassName = ce.getOntClassName();
			String serviceName =ce.getServiceName();
			 //System.out.println("Agent service name="+ ce.getServiceName());
			String jadeCls = (new File(Common.getJadeOutputDir()))
					.getCanonicalPath();
			String jaxCls = (new File(Common.getJaxOutputDir()))
					.getCanonicalPath();
			
			
			//URL urlJade = new File(Common.getJadeOutputDir() + File.separatorChar+ "Onto.jar").toURL();
			//URL urlJava = new File(Common.getJaxOutputDir() + File.separatorChar+ "Stub.jar").toURL();
			
			//URL[] urlsToLoadFrom = new URL[]{urlJade, urlJava};
			//java.net.URLClassLoader loader = new java.net.URLClassLoader(urlsToLoadFrom);
			
			//			Update classpath
			
			String jadebase = Common.getJadeBaseOutputDir()+ File.separatorChar + myAgent.getLocalName() + 
			  File.separatorChar + serviceName;
			String jaxbase = Common.getJaxBaseOutputDir()+ File.separatorChar + myAgent.getLocalName() + 
			  File.separatorChar + serviceName;
			//Debug
			Logger log = Logger.getLogger("WS2JADE");
			//log.info("Load "+jadebase + File.separatorChar
			//		+ "Onto.jar");
			
			Utility.addFile(jadebase + File.separatorChar
					+ "Onto.jar");
	
			Utility.addFile(jaxbase + File.separatorChar
					+ "Stub.jar");
			Class ontClass = Class.forName(ontClassName);
			
			//create an instance
			Method method = ontClass.getMethod("getInstance", new Class[0]);
			Object o = method.invoke(null, new Object[0]);

			//Check if it is an Ontology
			if (o instanceof Ontology) {
				//ContentManager mgr =
				// (ContentManager)myAgent.getContentManager();
				manager.registerOntology((Ontology) o);
				log.info("Successfully update my ontology base with onto="
								+ ontClassName);
				//Register with DF
				registerDF(serviceName,ce.getOntClassName());

			} else {
				log.error("Invalid Ontology name");
			}

		}
		private void registerDF(String serviceName, String ontClsName)
		{		
			dfd.setName(getAID());
			dfd.addProtocols(FIPANames.InteractionProtocol.FIPA_REQUEST);
			dfd.addLanguages(codec.getName());
			dfd.addOntologies(wsOntology.getName());
			dfd.addOntologies(adminOntology.getName());
			
			ServiceDescription sd = new ServiceDescription();
			sd.setType("Web Service");
			sd.setName(serviceName);	
			sd.addOntologies(ontClsName.substring(0,ontClsName.lastIndexOf('.'))+"-ontology");
			sd.addProtocols(FIPANames.InteractionProtocol.FIPA_REQUEST);
			sd.addLanguages(codec.getName());
		
			dfd.addServices(sd);
			//I "HAVE TO" re-register ! Why ?
			//First, de-register
			try {
				DFService.deregister(myAgent);				
			}
			catch (Exception fe) {
				Logger.getLogger("WS2JADE").debug("Exception occurred", fe);
				//fe.printStackTrace();
			}
			try {
			DFService.register(myAgent, dfd);
			}
			catch (Exception fe) {
				Logger.getLogger("WS2JADE").debug("Exception occurred", fe);
				//fe.printStackTrace();
			}
			
			
		}
		public void action() {
			Logger log =Logger.getLogger("WS2JADE");
			try {
				//Handle app object
				Object ontoObj = getO2AObject();
				if (ontoObj != null && (ontoObj instanceof AddOntAction)) {
					AddOntAction addOntAction = (AddOntAction) ontoObj;
					addOnto(addOntAction);
					
				}
			
				//Handle admin msgs
				ACLMessage msg = receive(MessageTemplate
						.MatchPerformative(ACLMessage.REQUEST));
				if (msg != null) {
					ContentElement ce = (ContentElement) ((Action) manager
							.extractContent(msg)).getAction();

					//Handle request to add new Ontology
					if (ce != null && (ce instanceof AddOntAction)) {
						AddOntAction addOntAction = (AddOntAction) ce;
						addOnto(addOntAction);

					} else //Suppose ce therefore will be one of agent's Action
					// class
					{
						//TO DO: Try to get conformace by extending a WSAction
						Logger.getLogger("WS2JADE").debug("WSAG got a request with type: "+ce.getClass()
								.getName());
						String[] details = resolveReqDetails(ce.getClass()
								.getName());

						//Spawn off a new thread
						WSClient wsClient = new WSClient();
						wsClient.setup(details[0], details[1], details[2]);
						wsClient.setInput(ce);

						//Construct and send off the message
						wsClient.setAgent(myAgent);
						wsClient.setMsg(msg);
						(new Thread(wsClient)).start();
					}
				} else {
					block();
				}
			} catch (OntologyException eo) {			
				
				log.error("Unsupported Ontology\n");
				log.debug("Exception occurred", eo);
			} catch (NoSuchMethodException e) {
				log.error("No getInstance Method found\n");
				log.debug("Exception occurred", e);
			} catch (Exception e) {
				log.debug("Exception occurred", e);

			}
		}

		//Resove the invocation details from the Action class name
		//The naming convention is
		// JadeGen.onto.GenericService.GoogleSearchPort.Request.ReqName
		private String[] resolveReqDetails(String s) {
			String[] details = new String[3];
			Logger log = Logger.getLogger("WS2JADE");
			try {

				StringTokenizer st = new StringTokenizer(s, ".");
				int num = st.countTokens();
				for (int i = 0; i < num - 4; i++)
					st.nextToken();

				//Service (assigned by the Agent) name
				details[0] = st.nextToken();

				//Porttype name
				details[1] = st.nextToken();

				//Request package
				String type = st.nextToken();
				if (!type.equals("Request")) {
					
					log.error("Actions must be in Request package!");
				}

				details[2] = st.nextToken();

				//Debug
				log.debug("Agent Service Name = " + details[0]
						+ "\nPort Name = " + details[1] + "\nAction Name= "
						+ details[2]);
			} catch (NoSuchElementException en) {
				log.error("Action class naming convention violated");
				log.debug("Exception occurred", en);
			} catch (Exception e) {
				log.debug("Exception occurred", e);
			}

			return details;

		}

	}

	public class WSClient extends Thread {

		private String optName = null;

		private String portName = null;

		private String serviceName = null;

		//Input - AgentAction object
		private Object input = null;

		//Who is your dady
		private Agent agent = null;

		private ACLMessage answerMsg = null;

		public void setup(String sName, String pName, String oName) {
			serviceName = sName;
			optName = oName;
			portName = pName;

		}

		public void setAgent(Agent a) {
			agent = a;
		}

		public void setMsg(ACLMessage msg) {
			answerMsg = msg;

		}

		public void setInput(Object o) {
			input = o;
		}

		//Implement of the Thread
		public void run() {
			Logger log = Logger.getLogger("WS2JADE");
			//Locate the Java mapping file JadeGen.mapping.GenericService
			String rootPck = Common.getJadeGenRootPkg();
			String mappingClsName = rootPck + ".mapping." + serviceName
					+ ".JavaTypeMapping";
			Result result = null;
			Object jadeResponse=null;
			try {
				Class mappingCls = Class.forName(mappingClsName);
				Object obj = mappingCls.newInstance();

				//Get back the location of Axis service stub by invoke
				// getStubServiceClass() on JavaTypeMapping
				Method methodStub = mappingCls.getMethod("getStubServiceClass",
						new Class[0]);
				String axisServiceStub = methodStub.invoke(obj, new Object[0])
						.toString();
				Class axisServiceCls = Class.forName(axisServiceStub
						+ "Locator");
				Object axisServiceObj = axisServiceCls.newInstance();

				//Get back the portname of the stub by invoke getAnyPortName()
				// on JavaTypeMapping
				Class[] getAnyPort_ArgsType = { Class
						.forName("java.lang.String") };
				Object[] getAnyPort_Inputs = { portName };
				methodStub = mappingCls.getMethod("getAnyPortName",
						getAnyPort_ArgsType);
				String stubPortName = methodStub.invoke(obj, getAnyPort_Inputs)
						.toString();
				
				//Getback the interface stub file by invoke getPortName() on
				// axis service stub
				String getPortName = "get" + stubPortName;
				methodStub = axisServiceCls
						.getMethod(getPortName, new Class[0]);
				//And the port stub and its class name are
				Object portObj = methodStub.invoke(axisServiceObj,
						new Object[0]);
				Class portCls = methodStub.getReturnType();

				//We get to the second level of introspection
				//Find the oper mapping by invoking invoke(portClass,
				// actionClass)
				log.debug("Try to do " +obj.getClass() +
						".invoke(" + portCls  +"," + input.getClass());
				log.debug("Input is " + portObj.getClass() +" and " + input.getClass());
				Class[] invoke_ArgsType = { portCls, input.getClass() };
				Method invokeMethod = mappingCls.getMethod("invoke",
						invoke_ArgsType);
				Object[] invoke_inputs = { portObj, input };
				jadeResponse = invokeMethod.invoke(obj, invoke_inputs);
			}
			catch(Exception e)
			{
				//e.printStackTrace();
				log.debug("Exception occurred", e);
				Error error = new Error();
				error.setErrorMsg("BAD REQUEST");
				jadeResponse =error;
			}
			try
			{

				//Java pass object by ref, so make sure you haven't anything
				// with
				//msg in the main agent code !!!
				ACLMessage msg = answerMsg.createReply();
				msg.setPerformative(ACLMessage.INFORM);

				ContentManager mgr = agent.getContentManager();
				Action requestedAction = (Action) mgr.extractContent(answerMsg);

				result = new Result(requestedAction, jadeResponse);
				mgr.fillContent(msg, result);
				agent.send(msg);
			} catch (Exception e) {
				//e.printStackTrace();
				log.debug("Exception occurred", e);
				
			}
			

		}

	}

}