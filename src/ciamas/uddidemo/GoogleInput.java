package ciamas.uddidemo;

import javax.swing.*;
import java.awt.*;
import com.borland.jbcl.layout.XYLayout;
import com.borland.jbcl.layout.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.Border;
import javax.swing.border.TitledBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <p>Title: My Jade Examples</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: CIAMAS, Swinburne University</p>
 *
 * @author not attributable
 * @version 1.0
 */
public class GoogleInput extends JFrame{

    JPanel main_Panel = new JPanel();
    JPanel jPanel1 = new JPanel();
    XYLayout xYLayout1 = new XYLayout();
    Border border1 = BorderFactory.createEtchedBorder(EtchedBorder.RAISED,
            Color.white, new Color(165, 163, 151));
    Border border2 = new TitledBorder(border1, "Enter Search Parameters");
    XYLayout xYLayout2 = new XYLayout();
    JLabel search_Label = new JLabel();
    JLabel count_Label = new JLabel();
    JTextField search_TextField = new JTextField();
    JTextField result_TextField = new JTextField();
    JButton submit_Button = new JButton();
    JButton reset_Button = new JButton();
    JButton cancel_Button = new JButton();
    GoogleClient g_C;
    JProgressBar search_ProgressBar = new JProgressBar();

    public GoogleInput(GoogleClient g_Client) {
        g_C = g_Client;
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        main_Panel.setLayout(xYLayout1);
        jPanel1.setBorder(border2);
        jPanel1.setLayout(xYLayout2);
        count_Label.setText("Result Count:");
        search_TextField.setToolTipText("Enter search terms ");
        search_TextField.setText("");
        result_TextField.setToolTipText("Enter the number of results you want");
        result_TextField.setText("");
        submit_Button.setText("Submit");
        submit_Button.addActionListener(new
                                        GoogleInput_submit_Button_actionAdapter(this));
        reset_Button.setText("Reset");
        reset_Button.addActionListener(new
                                       GoogleInput_reset_Button_actionAdapter(this));
        cancel_Button.setText("Cancel");
        cancel_Button.addActionListener(new
                                        GoogleInput_cancel_Button_actionAdapter(this));
        search_Label.setText("Search Term/Phrase:");
        this.setTitle("Google Search Input");
        jPanel1.add(search_Label, new XYConstraints(13, 6, 129, -1));
        jPanel1.add(count_Label, new XYConstraints(14, 32, 124, -1));
        jPanel1.add(search_TextField, new XYConstraints(128, 2, 243, -1));
        jPanel1.add(result_TextField, new XYConstraints(128, 26, 243, -1));
        main_Panel.add(search_ProgressBar, new XYConstraints(3, 123, 391, -1));
        main_Panel.add(submit_Button, new XYConstraints(180, 96, -1, -1));
        main_Panel.add(reset_Button, new XYConstraints(253, 96, -1, -1));
        main_Panel.add(cancel_Button, new XYConstraints(324, 96, -1, -1));
        main_Panel.add(jPanel1, new XYConstraints(7, 10, 387, 79));

        this.getContentPane().add(main_Panel, java.awt.BorderLayout.CENTER);
        this.setSize(410, 175);
        this.setResizable(false);
        this.setTitle("Google Search Input");
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = getSize();
        if (frameSize.height > screenSize.height) {
            frameSize.height = screenSize.height;
        }
        if (frameSize.width > screenSize.width) {
            frameSize.width = screenSize.width;
        }

        this.setLocation( (screenSize.width - frameSize.width) / 2,
                          (screenSize.height -
                           frameSize.height) / 2);
        this.setVisible(true);
    }

 /*   public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(
                    "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            GoogleInput g = new GoogleInput();
            g.setVisible(true);
        }
        catch (Exception e) {}
    }*/

    public void cancel_Button_actionPerformed(ActionEvent e) {
        this.dispose();
        System.exit(0);
    }

    public void reset_Button_actionPerformed(ActionEvent e) {
        this.search_TextField.setText("");
        this.result_TextField.setText("");
    }

    public void submit_Button_actionPerformed(ActionEvent e) {
        if(this.search_TextField.getText().equals("") ||
           this.result_TextField.getText().equals("")) {
            // please enter all values
        }
        else {
            g_C.s_Terms = this.search_TextField.getText().toString();
            g_C.s_Count = this.result_TextField.getText().toString();
            this.search_ProgressBar.setIndeterminate(true);
            synchronized (g_C) {
                g_C.notify();
            }
        }
    }
}


class GoogleInput_reset_Button_actionAdapter implements ActionListener {
    private GoogleInput adaptee;
    GoogleInput_reset_Button_actionAdapter(GoogleInput adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
        adaptee.reset_Button_actionPerformed(e);
    }
}


class GoogleInput_submit_Button_actionAdapter implements ActionListener {
    private GoogleInput adaptee;
    GoogleInput_submit_Button_actionAdapter(GoogleInput adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {

        adaptee.submit_Button_actionPerformed(e);
    }
}


class GoogleInput_cancel_Button_actionAdapter implements ActionListener {
    private GoogleInput adaptee;
    GoogleInput_cancel_Button_actionAdapter(GoogleInput adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
        adaptee.cancel_Button_actionPerformed(e);
    }
}
