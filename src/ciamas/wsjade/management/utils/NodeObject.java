
package ciamas.wsjade.management.utils;

import javax.swing.Icon;

/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class NodeObject {
	public NodeObject(String t, String n, boolean b) {
		this.text = t;
		//this.icon = new ImageIcon(getClass().getResource(n));
		this.deployed = b;
		this.cmd = n;
	}

	// Return text for display
	public String toString() {
		return text;
	}

//	 Return text for special control
	public String getCmd() {
		return cmd;
	}

	// Return the icon
	public Icon getIcon() {
		return icon;
	}

	public boolean isDeploy() {
		return deployed;
	}

	public void setDeploy(boolean b) {
		deployed = b;
	}

	protected String text;

	protected Icon icon;

	protected boolean deployed;
	protected String cmd;
}

