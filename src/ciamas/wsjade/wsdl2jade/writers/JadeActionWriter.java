/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.wsdl.Operation;
import javax.wsdl.PortType;

import org.apache.axis.wsdl.symbolTable.BindingEntry;
import org.apache.axis.wsdl.symbolTable.Parameter;
import org.apache.axis.wsdl.symbolTable.Parameters;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.toJava.Emitter;
import org.apache.axis.wsdl.toJava.JavaClassWriter;
import org.apache.axis.wsdl.toJava.Namespaces;
import org.apache.axis.wsdl.toJava.Utils;

import ciamas.wsjade.wsdl2jade.mapping.TypeMapping;
import ciamas.wsjade.wsdl2jade.utils.BeanUtils;
import ciamas.wsjade.wsdl2jade.utils.Common;

public class JadeActionWriter extends JavaClassWriter {

	protected Operation opt;

	protected BindingEntry bEntry;

	protected PortType pType;

	protected Emitter emitter;

	protected OntSchemaTemp ontSchema = OntSchemaTemp.getInstance();

	public JadeActionWriter(OntParser e, Operation o, PortType p,
			BindingEntry b, SymbolTable symbolTable) {
		super(e, Common.getInstance().getRequestPackageName(p) + "."
				+ BeanUtils.upperFirstLetter(Utils.xmlNameToJava(o.getName())), "complexType");

		this.emitter = e;
		this.opt = o;
		this.pType = p;
		this.bEntry = b;

		//Im not going to mess up with the namespace
		this.namespaces = new Namespaces(Common.getJadeOutputDir());
		this.packageName = Common.getInstance().getRequestPackageName(p);
		this.className = BeanUtils.upperFirstLetter(Utils.xmlNameToJava(o.getName()));

	}

	public void generate() throws IOException {

		String fqClass = getPackage() + "." + getClassName();
		if (!emitter.getGeneratedFileInfo().getClassNames().contains(fqClass)) {
			super.generate();
		}

	}

	protected String getExtendsText() {
		return "implements jade.content.AgentAction";
	}

	protected void writeHeaderComments(PrintWriter pw) throws IOException {
		Common.printBanner(pw);

	}

	protected void writeFileBody(PrintWriter pw) throws IOException {
		String clName = packageName + "." + className;

		Vector v = new Vector();

		Parameters parms = bEntry.getParameters(opt);

		//pw.println("private String " + serviceIdentifier +";");
		//pw.println("private String " + actionIdentifier +";");

		for (int i = 0; parms != null && i < parms.list.size(); ++i) {
			Parameter p = (Parameter) parms.list.get(i);

			String javaName = Utils.xmlNameToJava(p.getName());

			//Type and RealType are the same if not List
			String javaType = null;
			String javaRealType = null;

			if (p.getMode() == Parameter.IN || p.getMode() == Parameter.INOUT) {
				//javatype is jade type
				//javaRealType is jaxrpc type
				javaType = Utils.getParameterTypeName(p);
				//if(TypeMapping.isComplex(javaType))
				//javaType =Common.getFullReflectPkg() + javaType;
				javaRealType = javaType;

				javaType = TypeMapping.getJadeDataType(javaRealType, false);
				javaName = BeanUtils.lowerFirstLetter(javaName);

				//Add into GenCache the mapping name
				GenCache cache = GenCache.getInstance();
				cache.record(clName, p.getName(), javaName, javaRealType,
						javaType);

				//Add into AgentWriter
				JadeAgentWriter agentWriter = JadeAgentWriter.getInstance();
				agentWriter.register(clName, Common.ACTION);

				v.addElement(new BeanUtils(javaType, javaRealType, javaName));
				pw.println("private " + javaType + " " + javaName + ";");

			}

		}

		for (int i = 0; i < v.size(); i++) {

			BeanUtils b = (BeanUtils) v.elementAt(i);
			b.register(packageName + "." + className);
			b.writeBean(pw);

		}

	}

}