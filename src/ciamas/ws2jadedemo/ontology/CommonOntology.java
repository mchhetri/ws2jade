/*
 * CommonOntology.java
 *
 * Created on 31 January 2005, 16:02
 */

package ciamas.ws2jadedemo.ontology;

import jade.content.onto.*;
import jade.content.schema.*;

import jade.util.leap.List;

/**
 *
 * @author  403515
 */
public class CommonOntology extends Ontology implements CommonVocabulary {
    
    public static final String ONTOLOGY_NAME = "CommonOntolgy";
    private static Ontology theInstance = new CommonOntology(BasicOntology.getInstance());
    
    /** Creates a new instance of CommonOntology */
    private CommonOntology(Ontology base) {
        super(ONTOLOGY_NAME, base);
        try {
            add(new ConceptSchema(SEARCH), Search.class); 
            add(new ConceptSchema(SEARCHRESULT), SearchResult.class); 
            add(new ConceptSchema(CARTITEM), CartItem.class);
            add(new ConceptSchema(SEARCHRESULTS), SearchResults.class);
            add(new ConceptSchema(CART), Cart.class);
            add(new ConceptSchema(DELIVERY), Delivery.class);
            add(new ConceptSchema(PAYMENT), Payment.class);
            // Add slots as needed to each concept (schema)
            // SEARCH
            ConceptSchema cs0 = (ConceptSchema) getSchema(SEARCH); 
            cs0.add(SEARCH_KEYWORD, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs0.add(SEARCH_MODE, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            // SEARCHRESULT
            ConceptSchema cs1 = (ConceptSchema) getSchema(SEARCHRESULT);
            cs1.add(SEARCHRESULT_PRODUCTNAME, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs1.add(SEARCHRESULT_ASIN, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs1.add(SEARCHRESULT_LISTPRICE, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs1.add(SEARCHRESULT_OURPRICE, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs1.add(SEARCHRESULT_MANUFACTURER, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs1.add(SEARCHRESULT_AVAILABILITY, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs1.add(SEARCHRESULT_URL, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs1.add(SEARCHRESULT_CRCOMMENT, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs1.add(SEARCHRESULT_CRRATING, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs1.add(SEARCHRESULT_CRSUMMARY, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs1.add(SEARCHRESULT_QUANTITY, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            // SEARCHRESULTS
            ConceptSchema cs2 = (ConceptSchema) getSchema(SEARCHRESULTS);
            cs2.add(SEARCHRESULTS_RESULTS, (ConceptSchema) getSchema(SEARCHRESULT), 0, ObjectSchema.UNLIMITED);
            // CARTITEM
            ConceptSchema cs3 = (ConceptSchema) getSchema(CARTITEM);
            cs3.add(CARTITEM_ITEMID, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs3.add(CARTITEM_QUANTITY, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs3.add(CARTITEM_HASCART, (PrimitiveSchema)getSchema(BasicOntology.BOOLEAN), ObjectSchema.OPTIONAL);
            cs3.add(CARTITEM_HMAC, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs3.add(CARTITEM_CARTID, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            // CART
            ConceptSchema cs4 = (ConceptSchema) getSchema(CART);
            cs4.add(CART_CARTID, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs4.add(CART_PURCHASEURL, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs4.add(CART_HMAC, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs4.add(CART_ITEMS, (ConceptSchema) getSchema(SEARCHRESULTS), ObjectSchema.OPTIONAL);
            // DELIVERY
            ConceptSchema cs5 = (ConceptSchema) getSchema(DELIVERY);
            cs5.add(DELIVERY_EMAILADDRESS, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs5.add(DELIVERY_PASSWORD, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs5.add(DELIVERY_STREETNUMBER, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs5.add(DELIVERY_STREETNAME, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs5.add(DELIVERY_SUBURB, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs5.add(DELIVERY_POSTCODE, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs5.add(DELIVERY_STATE, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs5.add(DELIVERY_COUNTRY, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs5.add(DELIVERY_DELIVERYDATE, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs5.add(DELIVERY_PREFERREDDELIVERYTIMEPERIOD, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs5.add(DELIVERY_DESCRIPTION, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            // PAYMENT
            ConceptSchema cs6 = (ConceptSchema) getSchema(PAYMENT);
            cs6.add(PAYMENT_EMAILADDRESS, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs6.add(PAYMENT_PASSWORD, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs6.add(PAYMENT_RECIPIENTEMAILADDRESS, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs6.add(PAYMENT_AMOUNT, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs6.add(PAYMENT_CURRENCY, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
            cs6.add(PAYMENT_NOTE, (PrimitiveSchema)getSchema(BasicOntology.STRING), ObjectSchema.OPTIONAL);
        }
        catch (OntologyException oe) {
            System.out.println(oe);
            oe.printStackTrace();
        }
    }

    public static Ontology getInstance() {
        return theInstance; 
    } 

}
