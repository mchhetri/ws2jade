/*
 * AmazonAgent.java
 *
 * Created on 31 January 2005, 10:52
 */

package ciamas.ws2jadedemo.proxy;

import jade.content.*;
import jade.content.onto.*;
import jade.core.*;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import jade.util.leap.*;
import jade.content.onto.basic.*;
import jade.content.lang.*;
import jade.content.lang.sl.*;
import jade.proto.AchieveREResponder;
import jade.proto.AchieveREInitiator;
import jade.domain.FIPANames;
import jade.domain.FIPAAgentManagement.NotUnderstoodException;
import jade.domain.FIPAAgentManagement.RefuseException;
import org.apache.log4j.Logger;

import java.util.Vector;

/**
 * This agent communicates with Amazon web services. It does so by coordinating with a
 * CIAMAS WS2Jade WSAG via an automatically generated ontology.
 * @author 403515
 */
public class AmazonAgent extends Agent {
    
    private final String DEVTAG = "0M2NZ4RNE04KG8YDS5G2";
    // Keep track of the Delivery details for the client
    private ciamas.ws2jadedemo.ontology.Delivery delivery = null;
    // This is the Delivery Request Message that we will forward to
    // GlobalTransportsAgent when we receive payment confirmation
    private ACLMessage deliveryRequestMsg = null;
    private ContentManager manager;
    private Codec codec;
    private Ontology ontology;
    private Ontology commonOntology;
    
    
    /** Creates a new instance of AmazonAgent */
    public AmazonAgent() {
        Logger.getLogger("WS2JADE_DEMO").info("Initialising Agent...");
        manager = (ContentManager)getContentManager();
        codec = new SLCodec();
        ontology = JadeGen.onto.AmazonService.WebOntology.getInstance();
        commonOntology = ciamas.ws2jadedemo.ontology.CommonOntology.getInstance();
    }
    
    class HandleDeliveryInform extends CyclicBehaviour {
        
        Agent a;
        MessageTemplate deliveryDetailsTemplate = MessageTemplate.and(
            MessageTemplate.MatchPerformative(ACLMessage.INFORM),
            MessageTemplate.MatchSender(new AID("GlobalTransportsAgent", AID.ISLOCALNAME)));
        Logger log = Logger.getLogger("WS2JADE_DEMO");
        public HandleDeliveryInform(Agent a) {
            this.a = a;
        }
        
        public void action() {
        
            // Got the Delivery confirmation. Send it to the ClientAgent
            ACLMessage deliveryInform = a.receive(deliveryDetailsTemplate);
            if (deliveryInform != null) {
                log.info("<GOT DELIVERY CONFIRMATION>");
                log.info("<FORWARDING TO ClientAgent>");
                // Send to ClientAgent
                ACLMessage sendInform = new ACLMessage(ACLMessage.INFORM);
                sendInform.setSender(getAID());
                sendInform.addReceiver(new AID("ClientAgent", false));
                sendInform.setConversationId("delivery");
                sendInform.setLanguage(codec.getName());
                sendInform.setOntology(commonOntology.getName());
                try {
                    sendInform.setContent(deliveryInform.getContent());
                }
                catch (Exception e) {
                    //e.printStackTrace();
                	log.debug("Exception occur", e);
                }
                send(sendInform);
            }
            else {
                block();
            }
        }
        
    }
    
    class HandlePaymentConfirmation extends CyclicBehaviour {
        
        Agent a;
        MessageTemplate confirmationTemplate = MessageTemplate.and(
            MessageTemplate.MatchPerformative(ACLMessage.INFORM),
            MessageTemplate.MatchSender(new AID("PayFriendAgent", AID.ISLOCALNAME)));
        Logger log = Logger.getLogger("WS2JADE_DEMO");
        public HandlePaymentConfirmation(Agent a) {
            super(a);
            this.a = a;
        }
        
        public void action() {
            ACLMessage paymentConfirmation = a.receive(confirmationTemplate);
            if (paymentConfirmation != null) {
                log.info("<RECEIVED PAYMENT CONFIRMATION>");
                log.info(paymentConfirmation.getContent());
                if (delivery != null) {
                    if (paymentConfirmation.getContent() != null) {
                        log.info("<PAYMENT WAS A SUCCESS>");
                        // Send the delivery request to GlobalTransportsAgent
                        ACLMessage deliverymsg = new ACLMessage(ACLMessage.REQUEST);
                        deliverymsg.setSender(getAID());
                        deliverymsg.addReceiver(new AID("GlobalTransportsAgent", false));
                        deliverymsg.setLanguage(codec.getName());
                        deliverymsg.setOntology(commonOntology.getName());
                        deliverymsg.setPerformative(ACLMessage.REQUEST);
                        try {
                            manager.fillContent(deliverymsg, new Action(getAID(), delivery));
                        }
                        catch (Exception e) { 
                            log.debug("Exception occurred", e);
                        }
                        send(deliverymsg);
                    }
                    else {
                       log.error("<PAYMENT FAILED>");
                    }
                }
            }
            else {
                block();
            }
        }
        
    }
    
    // If the inform content is a Delivery object store it as an instance variable
    // for making a delivery request to GlobalTransportsAgent when payment ocnfirmation
    // is received from PayFriendAgent
    class HandleDetailsInform extends CyclicBehaviour {
        
        Agent a;
        MessageTemplate deliveryDetailsTemplate = MessageTemplate.and(
            MessageTemplate.MatchPerformative(ACLMessage.INFORM),
            MessageTemplate.MatchSender(new AID("ClientAgent", AID.ISLOCALNAME)));
        Logger log = Logger.getLogger("WS2JADE_DEMO");
        public HandleDetailsInform(Agent a) {
            super(a);
            this.a = a;
        }
        
        public void action() {
            ACLMessage deliveryDetails = a.receive(deliveryDetailsTemplate);
            if (deliveryDetails != null) {
                log.info("<RECEIVED DELIVERY DETAILS FROM CLIENT>");
                log.info("<WAITING FOR PAYMENT CONFIRMATION FROM " +
                    "PayFriendAgent BEFORE FORWARDING DELIVERY REQUEST TO GlobalTransportsAgent>");
                ContentElement content = null;
                try {
                    content = manager.extractContent(deliveryDetails);
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
                if (content instanceof jade.content.onto.basic.Action) {
                    Concept action = ((Action)content).getAction();
                    delivery = (ciamas.ws2jadedemo.ontology.Delivery)action;
                }
            }
            else {
                block();
            }
        }
        
    }
       
    public void setup() {
    	Logger log = Logger.getLogger("WS2JADE_DEMO");
        log.info("Agent " + getAID().getLocalName() + " is ready.");
        // Register the ontology
        manager.registerLanguage(codec);
        manager.registerOntology(ontology);
        manager.registerOntology(commonOntology);
        MessageTemplate template = MessageTemplate.and(
            MessageTemplate.MatchProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST),
            MessageTemplate.MatchPerformative(ACLMessage.REQUEST));
        AchieveREResponder responderBehaviour = new AchieveREResponder(this, template) {
            protected ACLMessage prepareResponse(ACLMessage request) throws NotUnderstoodException, RefuseException {
            	Logger.getLogger("WS2JADE_DEMO").info("REQUEST received from " + request.getSender().getName() + ".");
                ACLMessage agree = request.createReply();
                agree.setPerformative(ACLMessage.AGREE);
                return agree;
            }
        };
        // Register an AchieveREInitiator in the PREPARE_RESULT_NOTIFICATION state
        responderBehaviour.registerPrepareResultNotification(new AchieveREInitiator(this, null) {

            private AID responder = new AID("AmazonProvider", AID.ISLOCALNAME);
            protected Vector prepareRequests(ACLMessage request) {
                Vector v = null;
                // Retrieve the incoming request from the DataStore
                String incomingRequestKey = (String)((AchieveREResponder)parent).REQUEST_KEY;
                ACLMessage incomingRequest = (ACLMessage)getDataStore().get(incomingRequestKey);
                // Prepare the request to forward to the responder
                Logger.getLogger("WS2JADE_DEMO").info("Forwarding request to " + responder.getName() + ".");
                ACLMessage outgoingRequest = new ACLMessage(ACLMessage.REQUEST);
                outgoingRequest.setProtocol(FIPANames.InteractionProtocol.FIPA_REQUEST);
                outgoingRequest.setLanguage(codec.getName());
                outgoingRequest.setOntology(ontology.getName());
                outgoingRequest.addReceiver(responder);
                ContentElement content = null;
                try {
                    content = manager.extractContent(incomingRequest);
                }
                catch (Exception e) {
                    //e.printStackTrace();
                	Logger.getLogger("WS2JADE_DEMO").debug(e.getStackTrace());
                }
                Concept action = ((Action)content).getAction();
                // If the request content is a Search object replace it with a KeywordSearchRequest
                // object
                if (action instanceof ciamas.ws2jadedemo.ontology.Search) {
                	Logger.getLogger("WS2JADE_DEMO").info("<SEARCH REQUEST>");
                    ciamas.ws2jadedemo.ontology.Search searchRequest = (ciamas.ws2jadedemo.ontology.Search)action;
                    JadeGen.onto.reflect.com.amazon.soap.KeywordRequest keywordRequest =
                        new JadeGen.onto.reflect.com.amazon.soap.KeywordRequest();
                    keywordRequest.setDevtag(DEVTAG);
                    keywordRequest.setKeyword(searchRequest.getKeyword());
                    keywordRequest.setType("heavy");
                    keywordRequest.setPage("1");
                    keywordRequest.setTag("");
                    keywordRequest.setLocale("");
                    keywordRequest.setSort("");
                    keywordRequest.setVariations("");
                    keywordRequest.setMode(searchRequest.getMode());
                    JadeGen.onto.AmazonService.AmazonSearchPort.Request.KeywordSearchRequest keywordSearchRequest =
                        new JadeGen.onto.AmazonService.AmazonSearchPort.Request.KeywordSearchRequest();
                    keywordSearchRequest.setKeywordSearchRequest(keywordRequest);
                    try {
                        manager.fillContent(outgoingRequest, new Action(getAID(), keywordSearchRequest));
                    }
                    catch (Exception e) {
                       // e.printStackTrace();
                    	Logger.getLogger("WS2JADE_DEMO").debug(e.getStackTrace());
                    }
                    outgoingRequest.setReplyByDate(incomingRequest.getReplyByDate());
                    v = new Vector(1);
                    v.addElement(outgoingRequest);
                }
                // If the request content is a CartItem object replace it with a AddItem
                // object. Make a request for a new cart if cartItem.hasCart = false.
                else if (action instanceof ciamas.ws2jadedemo.ontology.CartItem) {
                	Logger.getLogger("WS2JADE_DEMO").info("<ADD_TO_CART REQUEST>");
                    ciamas.ws2jadedemo.ontology.CartItem cartItem = (ciamas.ws2jadedemo.ontology.CartItem)action;
                    JadeGen.onto.AmazonService.AmazonSearchPort.Request.AddShoppingCartItemsRequest ascir =
                        new JadeGen.onto.AmazonService.AmazonSearchPort.Request.AddShoppingCartItemsRequest();
                    JadeGen.onto.reflect.com.amazon.soap.AddShoppingCartItemsRequest acr =
                        new JadeGen.onto.reflect.com.amazon.soap.AddShoppingCartItemsRequest();
                    acr.setDevtag(DEVTAG);
                    acr.setTag("webservices-20");
                    // Create a shopping cart if needed or modify one if cartItem.getHasCart
                    // == true by providing acr with its HMAC and cartID
                    if (cartItem.getHasCart() == true) {
                        acr.setCartId(cartItem.getCartId());
                        acr.setHMAC(cartItem.getHMAC());
                    }
                    JadeGen.onto.reflect.com.amazon.soap.AddItem addItem = 
                        new JadeGen.onto.reflect.com.amazon.soap.AddItem();
                    addItem.setAsin(cartItem.getItemId());
                    System.out.println("Item ASIN: " + cartItem.getItemId());
                    addItem.setQuantity(cartItem.getQuantity() );
                    System.out.println("Quantity: " + cartItem.getQuantity());
                    ArrayList itemList = new ArrayList();
                    itemList.add(addItem);
                    acr.setItems(itemList);
                    ascir.setAddShoppingCartItemsRequest(acr);
                    try {
                        manager.fillContent(outgoingRequest, new Action(getAID(), ascir));
                    }
                    catch (Exception e) {
                        //e.printStackTrace();
                    	Logger.getLogger("WS2JADE_DEMO").debug(e.getStackTrace());
                    }
                    outgoingRequest.setReplyByDate(incomingRequest.getReplyByDate());
                    v = new Vector(1);
                    v.addElement(outgoingRequest);
                }
                return v;
            }

            protected void handleInform(ACLMessage inform) {
                System.out.println("Received INFORM.");
                storeNotification(ACLMessage.INFORM, inform);
            }

            protected void handleRefuse(ACLMessage refuse) {
                System.out.println("Received REFUSE.");
                storeNotification(ACLMessage.FAILURE, null);
            }

            protected void handleNotUnderstood(ACLMessage notUnderstood) {
                System.out.println("Received NOT_UNDERSTOOD.");
                storeNotification(ACLMessage.FAILURE, null);
            }

            protected void handleFailure(ACLMessage failure) {
                System.out.println("Received FAILURE.");
                storeNotification(ACLMessage.FAILURE, null);
            }

            protected void handleAllResultNotifications(Vector notifications) {
                if (notifications.size() == 0) {
                    // Timeout
                    System.out.println("FAILURE.");
                    storeNotification(ACLMessage.FAILURE, null);
                }
            }

            private void storeNotification(int performative, ACLMessage m) {
            	Logger log = Logger.getLogger("WS2JADE_DEMO");
                // Retrieve the incoming request from the DataStore
                String incomingRequestkey = (String)((AchieveREResponder)parent).REQUEST_KEY;
                ACLMessage incomingRequest = (ACLMessage) getDataStore().get(incomingRequestkey);
                // Prepare the notification to the request originator and store it in the DataStore
                ACLMessage notification = incomingRequest.createReply();
                notification.setPerformative(performative);
                if (performative == ACLMessage.INFORM) {
                    ContentElement content = null;
                    try {
                        content = manager.extractContent(m);
                    }
                    catch (Exception e) {
                        e.printStackTrace();
                    }
                    if (content instanceof jade.content.onto.basic.Result) {
                        jade.content.onto.basic.Result result = (jade.content.onto.basic.Result)content;
                        // If msg content is an instanceof KeywordSearchRequest convert it to
                        // a SearchResults object
                        if (result.getValue() instanceof JadeGen.onto.AmazonService.AmazonSearchPort.Response.KeywordSearchRequest) {
                            log.info("<SEARCH INFORM>");
                            ArrayList resultsList = new ArrayList();
                            ciamas.ws2jadedemo.ontology.SearchResults searchResults =
                                new ciamas.ws2jadedemo.ontology.SearchResults();
                            JadeGen.onto.AmazonService.AmazonSearchPort.Response.KeywordSearchRequest ret = 
                                (JadeGen.onto.AmazonService.AmazonSearchPort.Response.KeywordSearchRequest)result.getValue();
                            JadeGen.onto.reflect.com.amazon.soap.ProductInfo info = ret.get_return();
                            ArrayList detailList = info.getDetails();
                            for(int i=0; i<detailList.size(); i++) {
                                ciamas.ws2jadedemo.ontology.SearchResult tempResult =
                                    new ciamas.ws2jadedemo.ontology.SearchResult();
                                JadeGen.onto.reflect.com.amazon.soap.Details d = 
                                    (JadeGen.onto.reflect.com.amazon.soap.Details)detailList.get(i);
                                //System.out.println("ASIN: " + d.getAsin());
                                tempResult.setAsin(d.getAsin());
                                //System.out.println("Product Name: " + d.getProductName());
                                tempResult.setProductName(d.getProductName());
                                //System.out.println("List Price: " + d.getListPrice());
                                tempResult.setListPrice(d.getListPrice());
                                //System.out.println("Our Price: " + d.getOurPrice());
                                tempResult.setOurPrice(d.getOurPrice());
                                //System.out.println("Availability: " + d.getAvailability());
                                tempResult.setAvailability(d.getAvailability());
                                //System.out.println("Manufacturer: " + d.getManufacturer());
                                tempResult.setManufacturer(d.getManufacturer());
                                //System.out.println("URL: " + d.getImageUrlMedium());
                                tempResult.setUrl(d.getImageUrlMedium());
                                if (d.getReviews().getCustomerReviews() != null) {
                                    JadeGen.onto.reflect.com.amazon.soap.Reviews reviews = d.getReviews();
                                    ArrayList reviewList = reviews.getCustomerReviews();
                                    JadeGen.onto.reflect.com.amazon.soap.CustomerReview cr = 
                                        (JadeGen.onto.reflect.com.amazon.soap.CustomerReview)reviewList.get(0);
                                    //System.out.println("Customer Review Comment: " + cr.getComment());
                                    tempResult.setCrComment(cr.getComment());
                                    //System.out.println("Customer Review Rating: " + cr.getRating());
                                    tempResult.setCrRating(cr.getRating());
                                    //System.out.println("Customer Review Summary: " + cr.getSummary());
                                    tempResult.setCrSummary(cr.getSummary());
                                }
                                else {
                                    tempResult.setCrComment("NA");
                                    tempResult.setCrRating("NA");
                                    tempResult.setCrSummary("NA");
                                }
                                resultsList.add(tempResult);
                            }
                            // The precious search reults ready to be sent
                            searchResults.setResults(resultsList);
                            try {
                                manager.fillContent(notification, new Action(getAID(), searchResults));
                            }
                            catch (Exception e) {
                                //e.printStackTrace();
                            	log.debug(e.getStackTrace());
                            }
                        }
                        else if (result.getValue() instanceof JadeGen.onto.AmazonService.AmazonSearchPort.Response.AddShoppingCartItemsRequest) {
                            log.info("<ADD_TO_CART INFORM>");
                            JadeGen.onto.AmazonService.AmazonSearchPort.Response.AddShoppingCartItemsRequest ascir =
                                (JadeGen.onto.AmazonService.AmazonSearchPort.Response.AddShoppingCartItemsRequest)result.getValue();
                            JadeGen.onto.reflect.com.amazon.soap.ShoppingCart cart = ascir.getShoppingCart();
                            ciamas.ws2jadedemo.ontology.Cart crt = new ciamas.ws2jadedemo.ontology.Cart();
                            crt.setCartId(cart.getCartId());
                            crt.setHMAC(cart.getHMAC());
                            ciamas.ws2jadedemo.ontology.SearchResults cart_items = 
                                new ciamas.ws2jadedemo.ontology.SearchResults();
                            ArrayList tmpList = new ArrayList();
                            for (int i=0; i<cart.getItems().size(); i++) {
                                JadeGen.onto.reflect.com.amazon.soap.Item tmpItem =
                                    (JadeGen.onto.reflect.com.amazon.soap.Item)cart.getItems().get(i);
                                ciamas.ws2jadedemo.ontology.SearchResult cart_item =
                                    new ciamas.ws2jadedemo.ontology.SearchResult();
                                cart_item.setOurPrice(tmpItem.getOurPrice());
                                cart_item.setQuantity(tmpItem.getQuantity());
                                cart_item.setAsin(tmpItem.getAsin());
                                cart_item.setProductName(tmpItem.getProductName());
                                tmpList.add(cart_item);
                            }
                            cart_items.setResults(tmpList);
                            crt.setItems(cart_items);
                            crt.setPurchaseUrl(cart.getPurchaseUrl());
                            // Lets send the shopping cart
                            try {
                                manager.fillContent(notification, new Action(getAID(), crt));
                            }
                            catch (Exception e) {
                                //e.printStackTrace();
                            	log.debug(e.getStackTrace());
                            }
                        }
                    }
                }
                String notificationkey = (String)((AchieveREResponder)parent).RESULT_NOTIFICATION_KEY;
                log.info("Informing requesting agent of results.");
                getDataStore().put(notificationkey, notification);
            }

        });
        addBehaviour(responderBehaviour);
        addBehaviour(new HandlePaymentConfirmation(this));
        addBehaviour(new HandleDetailsInform(this));
        addBehaviour(new HandleDeliveryInform(this));
    }
    
    public void takeDown() {
    	Logger.getLogger("WS2JADE_DEMO").info(getLocalName() + " is now shutting down.");
    }
                      
}
