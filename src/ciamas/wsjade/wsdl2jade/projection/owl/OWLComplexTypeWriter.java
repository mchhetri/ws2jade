/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.wsdl2jade.projection.owl;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Vector;

import javax.xml.namespace.QName;

import org.apache.axis.wsdl.gen.Generator;
import org.apache.axis.wsdl.symbolTable.ElementDecl;
import org.apache.axis.wsdl.symbolTable.SchemaUtils;
import org.apache.axis.wsdl.symbolTable.TypeEntry;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class OWLComplexTypeWriter implements Generator {

	private TypeEntry type;

	private Vector elements;

	private Vector attributes;

	private TypeEntry extendType;

	protected Vector names = new Vector(); // even indices: types, odd: vars

	protected String simpleValueType = null; // name of type of simple value

	protected PrintWriter pw;

	protected OWLComplexTypeWriter(OWLParser emitter, TypeEntry type,
			Vector elements, TypeEntry extendType, Vector attributes) {
		//super(OWLParser, type.getName(), "complexType");
		this.type = type;
		this.elements = elements;
		this.attributes = attributes;
		this.extendType = extendType;

		if (type.isSimpleType()) {
			//enableSimpleConstructors = true;
			//enableToString = true;
		}

	}

	protected void preprocess() {
		// Add element names
		if (elements != null) {
			for (int i = 0; i < elements.size(); i++) {
				ElementDecl elem = (ElementDecl) elements.get(i);
				String typeName = elem.getType().getQName().getLocalPart();
				String variableName = elem.getName();//.getLocalPart();

				names.add(typeName);
				names.add(variableName);
				if (SchemaUtils.isSimpleSchemaType(elem.getType().getQName()))
					names.add(new Boolean(true));
				else
					names.add(new Boolean(false));

				if (type.isSimpleType() && variableName.equals("value")) {
					simpleValueType = typeName;
				}
			}
		}
		// Add attribute names
		if (attributes != null) {
			for (int i = 0; i < attributes.size(); i += 2) {
				String typeName = ((TypeEntry) attributes.get(i)).getQName()
						.getLocalPart();
				QName xmlName = (QName) attributes.get(i + 1);
				String variableName = xmlName.getLocalPart();
				names.add(typeName);
				names.add(variableName);
				if (SchemaUtils.isSimpleSchemaType(xmlName))
					names.add(new Boolean(true));
				else
					names.add(new Boolean(false));

				if (type.isSimpleType() && variableName.equals("value")) {
					simpleValueType = typeName;
				}
			}
		}

		if (extendType != null && extendType.getDimensions().equals("[]")) {
			String typeName = extendType.getName();
			String variableName = extendType.getQName().getLocalPart();
			names.add(typeName);
			names.add(variableName);
		}
	}

	public void generate() throws IOException {
		preprocess();
		String clsName = type.getQName().getLocalPart();
		String clsDef = "<owl:Class rdf:ID=\"" + clsName + "\">\n"
				+ "    <owl:subClassOf rdf:resource=\"&owl;#Thing\"/>\n"
				+ "</owl:Class>\n";

		Temp.getInstance().addText(clsDef);

		for (int i = 0; i < names.size(); i = i + 3) {
			boolean b = ((Boolean) names.elementAt(i + 2)).booleanValue();
			String protT = "";
			if (b)
				protT = "&xsd;" + "#" + names.elementAt(i);
			else
				protT = "#" + names.elementAt(i);
			String propDef =

			"\n<owl:Property rdf:ID=\"" + names.elementAt(i + 1) + "\">\n"
					+ "    <rdfs:range rdf:resource=\"" + protT + "\"/>\n"
					+ "    <rdfs:domain rdf:resource=\"" + "#" + clsName
					+ "\"/>\n" + "</owl:Property>";

			Temp.getInstance().addText(propDef);
		}
	}

}