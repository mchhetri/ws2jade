/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import java.io.File;
import java.io.FileWriter;
import java.util.Vector;

import javax.xml.namespace.QName;

import ciamas.wsjade.wsdl2jade.utils.Common;
import ciamas.wsjade.wsdl2jade.utils.PrintWriterUtils;
import org.apache.log4j.Logger;

public class WSTypeMappingTemp {

	private static WSTypeMappingTemp me = null;

	private Vector mapping = new Vector();

	private WSTypeMappingTemp() {
	}

	public static WSTypeMappingTemp getInstance() {
		if (me == null)
			me = new WSTypeMappingTemp();
		return me;
	}

	public void clear() {
		mapping.clear();
	}

	public void register(QName qn, String c, Class serFac, Class deFac) {
		TypeMappingDesc md = new TypeMappingDesc(qn, c, serFac, deFac);
		mapping.addElement(md);

	}

	public void generate() {
		String pck = Common.getJadeGenRootPkg() + ".mapping."
				+ Common.getServiceName();
		String pckDir = pck.replace('.', File.separatorChar);
		try {
			String outputDir = Common.getJadeOutputDir() + File.separatorChar
					+ pckDir;

			File file = new File(outputDir + File.separatorChar
					+ "WSTypeMapping.java");
			File parent = new File(file.getParent());
			if (!parent.exists())
				parent.mkdirs();
			PrintWriterUtils out = new PrintWriterUtils(new FileWriter(file));

			Common.printBanner(out);
			out.println();
			out.println("package " + pck + ";");
			out.println("import org.apache.axis.client.Call;");
			out.println("import javax.xml.namespace.QName;");

			out.println("public class WSTypeMapping {");

			out.println();
			out.println();

			out
					.println("public void registerTypeMapping(javax.xml.rpc.Call rpcCall){");
			out.println("	Call call =(Call)rpcCall;");
			out.println("	QName poqn = null;");
			out.println("	Class cls = null;");

			for (int i = 0; i < mapping.size(); i++) {
				TypeMappingDesc md = (TypeMappingDesc) mapping.elementAt(i);
				QName qn = md.getQName();
				String c = md.getTypeClass();
				Class ser = md.getSerFactory();
				Class de = md.getDeFactory();

				//Print out the registration
				out.println("");
				out.println("	poqn = new QName(\"" + qn.getNamespaceURI()
						+ "\",\"" + qn.getLocalPart() + "\");");

				out.println("	cls = " + c + ";");
				out.println("	call.registerTypeMapping(cls, poqn, ");

				out.println("		" + ser.getName() + ".class, ");
				out.println("		" + de.getName() + ".class);");

				out.println();
			}
			out.println("	}");

			//Print the footer
			out.println("}");
			out.close();
		} catch (Exception e) {
			//e.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred", e);
		}

	}

	public class TypeMappingDesc {
		private QName qn;

		private String c;

		private Class serFactory;

		private Class deFactory;

		public TypeMappingDesc() {
		}

		public TypeMappingDesc(QName q, String c, Class ser, Class de) {
			this.qn = q;
			this.c = c;
			this.serFactory = ser;
			this.deFactory = de;

		}

		public QName getQName() {
			return qn;
		}

		public String getTypeClass() {
			return c;
		}

		public Class getSerFactory() {
			return serFactory;
		}

		public Class getDeFactory() {
			return deFactory;
		}

		public void setQName(QName q) {
			this.qn = q;
		}

		public void setTypeClass(String c) {
			this.c = c;
		}

		public void setSerFactory(Class c) {
			this.serFactory = c;
		}

		public void setDeFactory(Class c) {
			this.deFactory = c;
		}
	}

}