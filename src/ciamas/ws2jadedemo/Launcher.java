/*
 * Launcher.java
 *
 * Created on 15 February 2005, 13:33
 */

package ciamas.ws2jadedemo;

/**
 *
 * @author  403515
 */
public class Launcher {
    
    private static boolean createSniffer = false;
    
    /** Creates a new instance of Launcher */
    public Launcher() {
    }
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(args.length + " argument/s");
        jade.core.Runtime rt0 = jade.core.Runtime.instance();
        jade.core.Profile mcProfile = new jade.core.ProfileImpl();
        jade.wrapper.AgentContainer mc = rt0.createMainContainer(mcProfile);
        // Create the Agents
        Object[] arguments = new Object[2];
        if (args.length == 2) {
            arguments[0] = args[0];
            arguments[1] = args[1];
        }
        else if (args.length == 3 && args[2].equals("-sniffer")) {
            arguments[0] = args[0];
            arguments[1] = args[1];
            createSniffer = true;
        }
        else if (args.length == 1 && args[0].equals("-sniffer")) {
            arguments = null;
            createSniffer = true;
        }
        else {
            arguments = null;
        }
        try {
            jade.wrapper.AgentController ap = 
            mc.createNewAgent("AmazonProvider", "JadeGen.mapping.AmazonService.WebAgent", arguments);
            ap.start();
            jade.wrapper.AgentController pf = 
            mc.createNewAgent("PayFriendProvider", "JadeGen.mapping.PayFriendService.WebAgent", null);
            pf.start();
            jade.wrapper.AgentController gtp = 
            mc.createNewAgent("GlobalTransportsProvider", "JadeGen.mapping.GTService.WebAgent", null);
            gtp.start();
            jade.wrapper.AgentController aa = 
            mc.createNewAgent("AmazonAgent", "ciamas.ws2jadedemo.proxy.AmazonAgent", null);
            aa.start();
            jade.wrapper.AgentController pa = 
            mc.createNewAgent("PayFriendAgent", "ciamas.ws2jadedemo.proxy.PayFriendAgent", null);
            pa.start();
            jade.wrapper.AgentController gta = 
            mc.createNewAgent("GlobalTransportsAgent", "ciamas.ws2jadedemo.proxy.GTAgent", null);
            gta.start();
            jade.wrapper.AgentController client = 
            mc.createNewAgent("ClientAgent", "ciamas.ws2jadedemo.client.ClientAgent", arguments);
            client.start();
            if (createSniffer) {
                jade.wrapper.AgentController sniffer = 
                mc.createNewAgent("SnifferAgent", "jade.tools.sniffer.Sniffer", null);
                sniffer.start();
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
