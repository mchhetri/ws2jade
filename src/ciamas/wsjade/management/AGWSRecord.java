/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.management;

/**
 * @author xnguyen
 * 
 * TODO To change the template for this generated type comment go to Window -
 * Preferences - Java - Code Style - Code Templates
 */
public class AGWSRecord {

	private String agentName = null;

	private String wsURL = null;

	public AGWSRecord(String a, String w) {
		agentName = a;
		wsURL = w;
	}

	/**
	 * @return Returns the agentName.
	 */
	public String getAgentName() {
		return agentName;
	}

	/**
	 * @param agentName
	 *            The agentName to set.
	 */
	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	/**
	 * @return Returns the wsURL.
	 */
	public String getWsURL() {
		return wsURL;
	}

	/**
	 * @param wsURL
	 *            The wsURL to set.
	 */
	public void setWsURL(String wsURL) {
		this.wsURL = wsURL;
	}
}