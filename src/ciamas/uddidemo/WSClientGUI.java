package ciamas.uddidemo;

import javax.swing.*;
import java.awt.*;
import com.borland.jbcl.layout.XYLayout;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.border.BevelBorder;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseMotionAdapter;

/**
 * <p>Title: My Jade Examples</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: CIAMAS, Swinburne University</p>
 *
 * @author Mohan Baruwal Chhetri
 * @version 1.0
 */

public class WSClientGUI extends JFrame{

    JSplitPane m_SplitPane = new JSplitPane();
    JTabbedPane right_TabbedPane = new JTabbedPane();
    JTabbedPane left_TabbedPane = new JTabbedPane();
    JPanel r1_Panel = new JPanel();
    JPanel r2_Panel = new JPanel();
    JPanel r3_Panel = new JPanel();
    JPanel l1_Panel = new JPanel();
    JPanel main_Panel = new JPanel();
    JPanel left_Panel = new JPanel();
    JPanel right_Panel = new JPanel();
    XYLayout xYLayout1 = new XYLayout();
    JPanel middle_Panel = new JPanel();
    GridLayout gridLayout1 = new GridLayout();
    Border border1 = BorderFactory.createEtchedBorder(EtchedBorder.RAISED,
            Color.white, new Color(165, 163, 151));
    Border border2 = new TitledBorder(border1, "Enter Search Parameters");
    JPanel bottom_Panel = new JPanel();
    Border border3 = BorderFactory.createEtchedBorder(EtchedBorder.RAISED,
            Color.white, new Color(165, 163, 151));
    JProgressBar search_ProgressBar = new JProgressBar();
    GridLayout gridLayout2 = new GridLayout();
    CardLayout cardLayout1 = new CardLayout();
    GridBagLayout gridBagLayout1 = new GridBagLayout();
    Border border4 = BorderFactory.createBevelBorder(BevelBorder.RAISED,
            Color.white, Color.white, new Color(115, 114, 105),
            new Color(165, 163, 151));
    Border border5 = BorderFactory.createEtchedBorder(EtchedBorder.RAISED,
            Color.white, new Color(165, 163, 151));
    Border border6 = BorderFactory.createEtchedBorder(new Color(255, 250, 250),
            new Color(178, 122, 122));
    JLabel search_Label = new JLabel();
    JLabel jLabel1 = new JLabel();
    GridBagLayout gridBagLayout2 = new GridBagLayout();
    JLabel jLabel2 = new JLabel();
    GridBagLayout gridBagLayout3 = new GridBagLayout();
    JLabel jLabel3 = new JLabel();
    JTextField service_TextField = new JTextField();
    JLabel jLabel4 = new JLabel();
    JButton search_Button = new JButton();
    GridBagLayout gridBagLayout4 = new GridBagLayout();
    JLabel jLabel5 = new JLabel();
    JButton psearch_Button = new JButton();
    JLabel jLabel6 = new JLabel();
    JTextField provider_TextField = new JTextField();
    GridBagLayout gridBagLayout5 = new GridBagLayout();
    JLabel jLabel7 = new JLabel();
    JLabel jLabel8 = new JLabel();
    JTextField tmodel_TextField = new JTextField();
    JButton tSearch_Button = new JButton();
    GridBagLayout gridBagLayout6 = new GridBagLayout();
    MicrosoftAgent m_Agent;
    JScrollPane l_ScrollPane = new JScrollPane();
    GridLayout gridLayout3 = new GridLayout();
    CardLayout cardLayout2 = new CardLayout();
    JList result_List = new JList();

    public WSClientGUI(MicrosoftAgent ms_Agent) {
        m_Agent = ms_Agent;
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

/*    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(
                    "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            WSClientGUI _client = new WSClientGUI();
        }
        catch (Exception e) {}
    }*/

    private void jbInit() throws Exception {
        result_List.setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        result_List.addMouseMotionListener(new
                WSClientGUI_result_List_mouseMotionAdapter(this));
        result_List.addMouseListener(new WSClientGUI_result_List_mouseAdapter(this));
        result_List.setModel(new DefaultListModel());
        result_List.setFixedCellHeight(17);
        result_List.setFixedCellWidth( -1);
        result_List.setFont(new java.awt.Font("Dialog", 0, 11));

        m_SplitPane.setBackground(SystemColor.control);
        m_SplitPane.setForeground(Color.black);
        m_SplitPane.setMinimumSize(new Dimension(0, 0));
        m_SplitPane.setDividerSize(3);
        m_SplitPane.setOneTouchExpandable(false);
        m_SplitPane.setBorder(null);
        m_SplitPane.setResizeWeight(0.0);
        right_TabbedPane.addTab("  Services  ", null, r1_Panel,
                                "Search enables you to locate Web Service Information");
        right_TabbedPane.addTab("  Providers  ", null, r2_Panel,
                                "Type one or more initial characters in the name of the provider you want to find");
        right_TabbedPane.addTab("  tModels  ", null, r3_Panel,
                                "Type one or more initial characters in the name of the tModel you want to find");
        main_Panel.setLayout(gridBagLayout1);
        middle_Panel.setBackground(new Color(233, 223, 216));
        middle_Panel.setBorder(border6);
        middle_Panel.setLayout(gridLayout1);
        right_TabbedPane.setBorder(null);
        bottom_Panel.setBorder(border3);
        bottom_Panel.setLayout(cardLayout1);
        right_Panel.setLayout(gridBagLayout2);
        search_Label.setFont(new java.awt.Font("Arial", Font.BOLD, 12));
        search_Label.setText("Search Criteria");
        jLabel1.setText("Search enables you to locate Web service information.");
        left_Panel.setPreferredSize(new Dimension(100, 100));
        left_Panel.setLayout(gridBagLayout3);
        jLabel2.setFont(new java.awt.Font("Arial", Font.BOLD, 12));
        jLabel2.setText("Search ");
        r1_Panel.setLayout(gridBagLayout4);
        jLabel3.setText(
                "Type one or more initial characters in the name of the service you " +
                "want to find.");
        jLabel4.setFont(new java.awt.Font("Arial", Font.BOLD, 11));
        jLabel4.setText("Service Name:");
        service_TextField.setToolTipText("Enter service name");
        search_Button.setText("Search");
        search_Button.addActionListener(new
                                        WSClientGUI_search_Button_actionAdapter(this));
        r2_Panel.setLayout(gridBagLayout5);
        jLabel5.setText(
                "Type one or more initial characters in the name of the provider you " +
                "want to find.");
        psearch_Button.setText("Search");
        psearch_Button.addActionListener(new
                WSClientGUI_psearch_Button_actionAdapter(this));
        jLabel6.setFont(new java.awt.Font("Arial", Font.BOLD, 11));
        jLabel6.setText("Provider Name:");
        provider_TextField.setToolTipText("Enter provider name");
        provider_TextField.setText("");
        provider_TextField.addActionListener(new
                WSClientGUI_provider_TextField_actionAdapter(this));
        r3_Panel.setLayout(gridBagLayout6);
        jLabel7.setText(
                "Type one or more initial characters in the name of the tModel you " +
                "want to find.");
        jLabel8.setFont(new java.awt.Font("Arial", Font.BOLD, 11));
        jLabel8.setText("tModel Name:");
        tmodel_TextField.setToolTipText("Enter tModel name");
        tmodel_TextField.addActionListener(new
                WSClientGUI_tmodel_TextField_actionAdapter(this));
        tSearch_Button.setText("Search");
        tSearch_Button.addActionListener(new
                WSClientGUI_tSearch_Button_actionAdapter(this));
        right_Panel.setMinimumSize(new Dimension(100, 100));
        this.addWindowListener(new WSClientGUI_this_windowAdapter(this));
        l1_Panel.setLayout(cardLayout2);
        this.setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        this.getContentPane().add(main_Panel, java.awt.BorderLayout.CENTER);
        left_TabbedPane.addTab("  Results  ", null, l1_Panel, "Search Results");
        l1_Panel.add(l_ScrollPane, "l_ScrollPane");
        l_ScrollPane.getViewport().add(result_List);
        middle_Panel.add(m_SplitPane);
        bottom_Panel.add(search_ProgressBar, "search_ProgressBar");
        main_Panel.add(middle_Panel,
                       new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
                                              , GridBagConstraints.CENTER,
                                              GridBagConstraints.BOTH,
                                              new Insets(0, 0, 0, 0), 285, 127));
        main_Panel.add(bottom_Panel,
                       new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0
                                              , GridBagConstraints.CENTER,
                                              GridBagConstraints.HORIZONTAL,
                                              new Insets(0, 0, 0, 0), 294, 5));
        right_Panel.add(right_TabbedPane,
                        new GridBagConstraints(0, 2, 1, 1, 1.0, 1.0
                                               , GridBagConstraints.CENTER,
                                               GridBagConstraints.BOTH,
                                               new Insets(6, 8, 1, 2), 299, 182));
        right_Panel.add(jLabel1, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                , GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(0, 8, 0, 16), 5, 5));
        right_Panel.add(search_Label,
                        new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                                               , GridBagConstraints.WEST,
                                               GridBagConstraints.NONE,
                                               new Insets(8, 8, 0, 156), 24, 5));
        m_SplitPane.add(left_Panel, JSplitPane.LEFT);
        m_SplitPane.add(right_Panel, JSplitPane.RIGHT);
        left_Panel.add(jLabel2, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0
                , GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(8, 5, 0, 47), 24, 5));
        left_Panel.add(left_TabbedPane,
                       new GridBagConstraints(0, 1, 1, 1, 1.0, 1.0
                                              , GridBagConstraints.CENTER,
                                              GridBagConstraints.BOTH,
                                              new Insets(7, 5, 2, 4), 28, 225));
        r1_Panel.add(search_Button, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
                , GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(0, 159, 152, 30), 0, 0));
        r1_Panel.add(service_TextField,
                     new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0
                                            , GridBagConstraints.WEST,
                                            GridBagConstraints.NONE,
                                            new Insets(0, 5, 152, 0), 143, 0));
        r2_Panel.add(provider_TextField,
                     new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0
                                            , GridBagConstraints.WEST,
                                            GridBagConstraints.NONE,
                                            new Insets(0, 5, 149, 0), 126, 0));
        r2_Panel.add(psearch_Button,
                     new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
                                            , GridBagConstraints.WEST,
                                            GridBagConstraints.NONE,
                                            new Insets(0, 159, 149, 30), 0, -1));
        r3_Panel.add(tSearch_Button,
                     new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0
                                            , GridBagConstraints.CENTER,
                                            GridBagConstraints.NONE,
                                            new Insets(0, 161, 153, 30), 0, 0));
        r1_Panel.add(jLabel3, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
                , GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(4, 5, 0, 10), 13, 0));
        r2_Panel.add(jLabel5, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
                , GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(4, 5, 0, 7), 13, 0));
        r3_Panel.add(jLabel7, new GridBagConstraints(0, 0, 2, 1, 0.0, 0.0
                , GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(4, 5, 0, 6), 13, 0));
        r3_Panel.add(jLabel8, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0
                , GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(6, 25, 0, 53), 193, 0));
        r3_Panel.add(tmodel_TextField,
                     new GridBagConstraints(0, 2, 1, 1, 1.0, 0.0
                                            , GridBagConstraints.WEST,
                                            GridBagConstraints.NONE,
                                            new Insets(0, 5, 153, 0), 133, 0));
        r1_Panel.add(jLabel4, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0
                , GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(9, 27, 0, 1), 30, 0));
        r2_Panel.add(jLabel6, new GridBagConstraints(0, 1, 2, 1, 0.0, 0.0
                , GridBagConstraints.WEST, GridBagConstraints.NONE,
                new Insets(6, 23, 0, 35), 38, 0));
        this.setSize(550, 400);
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension frameSize = getSize();
        if (frameSize.height > screenSize.height) {
          frameSize.height = screenSize.height;
        }
        if (frameSize.width > screenSize.width) {
          frameSize.width = screenSize.width;
        }

        this.setLocation( (screenSize.width - frameSize.width) / 2,
                         (screenSize.height -
                      frameSize.height) / 2);
        this.setVisible(true);
    }

    public void provider_TextField_actionPerformed(ActionEvent e) {

    }

    public void this_windowClosing(WindowEvent e) {
        int i = JOptionPane.showConfirmDialog(this, "Do you want to exit?", "Confirm Dialog",
            JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null);
        if(i==JOptionPane.YES_OPTION) {
            System.exit(0);
        }
        else if (i==JOptionPane.NO_OPTION) {
            // do nothing
        }
    }

    public void search_Button_actionPerformed(ActionEvent e) {
        m_Agent.search_Term = service_TextField.getText().toString();
        m_Agent.search_Type = 1;
        m_Agent.start_Search();
        this.search_ProgressBar.setIndeterminate(true);
    }

    public void psearch_Button_actionPerformed(ActionEvent e) {
        m_Agent.search_Term = provider_TextField.getText().toString();
        m_Agent.search_Type = 2;
        m_Agent.start_Search();
        this.search_ProgressBar.setIndeterminate(true);
    }

    public void tSearch_Button_actionPerformed(ActionEvent e) {
        m_Agent.search_Term = tmodel_TextField.getText().toString();
        m_Agent.search_Type = 3;
        m_Agent.start_Search();
        this.search_ProgressBar.setIndeterminate(true);
    }

    public void result_List_mouseEntered(MouseEvent e) {
        if(result_List.getModel().getSize()>0) {
            int l_Size = result_List.getModel().getSize();
            int max_Y = l_Size * result_List.getFixedCellHeight();
            //System.out.println("Max position " + max_Y);
            //System.out.println("Current pos " + e.getY());
            // get co-ordinates of selected index
            if (e.getY() <= max_Y) {
                this.setCursor(Cursor.HAND_CURSOR);
            }
            else if(e.getY() > max_Y) {
                this.setCursor(Cursor.DEFAULT_CURSOR);
            }
        }
        else {
            this.setCursor(Cursor.DEFAULT_CURSOR);
        }
    }

    public void result_List_mouseExited(MouseEvent e) {
        this.setCursor(Cursor.DEFAULT_CURSOR);
    }

    public void result_List_mouseClicked(MouseEvent e) {
        int indices = result_List.getSelectedIndices().length;
        int l_Size = result_List.getModel().getSize();
        int max_Y = l_Size * result_List.getFixedCellHeight();

        // if only one item is selected
        if(indices == 1) {
            int s_Index = result_List.getSelectedIndex();
            if(e.getY() <= max_Y && (e.getClickCount()==2)) {
                // Display additional information about the selected object
               // JOptionPane.showMessageDialog(this, "hi");
            	m_Agent.search_Type =4;
            	 m_Agent.start_QueryTModel(result_List.getSelectedValues()[0].toString());
            }
            else if(e.getY() > max_Y) {
                // do nothing
                result_List.clearSelection();
            }
        }
        else {
            // clear selection
            result_List.clearSelection();
        }
    }

    public void result_List_mouseMoved(MouseEvent e) {
        if(result_List.getModel().getSize()>0) {
             int l_Size = result_List.getModel().getSize();
             int max_Y = l_Size * result_List.getFixedCellHeight();
             // get co-ordinates of selected index
             if (e.getY() <= max_Y) {
                 this.setCursor(Cursor.HAND_CURSOR);
             }
             else if(e.getY() > max_Y) {
                 this.setCursor(Cursor.DEFAULT_CURSOR);
             }
         }
         else {
             this.setCursor(Cursor.DEFAULT_CURSOR);
        }
    }

    public void tmodel_TextField_actionPerformed(ActionEvent e) {

    }
}


class WSClientGUI_tmodel_TextField_actionAdapter implements ActionListener {
    private WSClientGUI adaptee;
    WSClientGUI_tmodel_TextField_actionAdapter(WSClientGUI adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
        adaptee.tmodel_TextField_actionPerformed(e);
    }
}


class WSClientGUI_result_List_mouseAdapter extends MouseAdapter {
    private WSClientGUI adaptee;
    WSClientGUI_result_List_mouseAdapter(WSClientGUI adaptee) {
        this.adaptee = adaptee;
    }

    public void mouseEntered(MouseEvent e) {
       adaptee.result_List_mouseEntered(e);
    }

    public void mouseExited(MouseEvent e) {
        adaptee.result_List_mouseExited(e);
    }

    public void mouseClicked(MouseEvent e) {
        adaptee.result_List_mouseClicked(e);
    }
}


class WSClientGUI_result_List_mouseMotionAdapter extends MouseMotionAdapter {
    private WSClientGUI adaptee;
    WSClientGUI_result_List_mouseMotionAdapter(WSClientGUI adaptee) {
        this.adaptee = adaptee;
    }

    public void mouseMoved(MouseEvent e) {
        adaptee.result_List_mouseMoved(e);
    }
}


class WSClientGUI_tSearch_Button_actionAdapter implements ActionListener {
    private WSClientGUI adaptee;
    WSClientGUI_tSearch_Button_actionAdapter(WSClientGUI adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {

        adaptee.tSearch_Button_actionPerformed(e);
    }
}


class WSClientGUI_search_Button_actionAdapter implements ActionListener {
    private WSClientGUI adaptee;
    WSClientGUI_search_Button_actionAdapter(WSClientGUI adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
        adaptee.search_Button_actionPerformed(e);
    }
}


class WSClientGUI_psearch_Button_actionAdapter implements ActionListener {
    private WSClientGUI adaptee;
    WSClientGUI_psearch_Button_actionAdapter(WSClientGUI adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
        adaptee.psearch_Button_actionPerformed(e);
    }
}


class WSClientGUI_this_windowAdapter extends WindowAdapter {
    private WSClientGUI adaptee;
    WSClientGUI_this_windowAdapter(WSClientGUI adaptee) {
        this.adaptee = adaptee;
    }

    public void windowClosing(WindowEvent e) {
        adaptee.this_windowClosing(e);
    }
}


class WSClientGUI_provider_TextField_actionAdapter implements ActionListener {
    private WSClientGUI adaptee;
    WSClientGUI_provider_TextField_actionAdapter(WSClientGUI adaptee) {
        this.adaptee = adaptee;
    }

    public void actionPerformed(ActionEvent e) {
        adaptee.provider_TextField_actionPerformed(e);
    }
}
