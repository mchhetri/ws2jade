/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.management;

import java.io.*;
import java.util.Date;

import javax.swing.JOptionPane;

import ciamas.wsjade.management.utils.Utility;
import ciamas.wsjade.wsdl2jade.WSDL2Jade;
import ciamas.wsjade.wsdl2jade.WSDL2JavaStub;
import ciamas.wsjade.wsdl2jade.utils.Common;
import ciamas.wsjade.wsdl2jade.writers.GenCache;
import ciamas.wsjade.wsdl2jade.writers.OntSchemaTemp;
import ciamas.wsjade.wsdl2jade.writers.WSTypeMappingTemp;
import org.apache.log4j.Logger;

/**
 * 
 * @author xnguyen
 */
public class OntoManager {

	private String axisBase = null;

	private String jadeBase = null;

	private static OntoManager me = null;

	//private Vector wsList = null;

	/** Creates a new instance of OntoManager */
	public static OntoManager getInstance() {
		if (me == null)
			me = new OntoManager();
		return me;
	}

	private OntoManager() {
		//wsList = new Vector();
	}

	private OntoManager(String axisBase, String jadeBase) {
		this.axisBase = axisBase;
		this.jadeBase = jadeBase;
		Common.setJaxOutputDir(axisBase);
		Common.setJadeOutputDir(jadeBase);
		//wsList = new Vector();

	}

	public void setBases(String axisBase, String jadeBase) {
		this.axisBase = axisBase;
		this.jadeBase = jadeBase;
		Common.setJaxOutputDir(axisBase);
		Common.setJadeOutputDir(jadeBase);
	}

	public void resetCache() {
		GenCache.getInstance().clear();
		OntSchemaTemp.getInstance().clear();
		WSTypeMappingTemp.getInstance().clear();
	}

	public String deployOnto(String ontoURI, String agentName,
			String serviceName, boolean remote) throws Exception {
		resetCache();
		//Check if existing
		//if (!deployed(ontoURI))
		//wsList.addElement(ontoURI);
		//		Register
		Common.setServiceName(serviceName);
		Common.setAgentName(agentName);
		Common.setServiceURL(ontoURI);
		Status.registerActionStatus(ontoURI);
		
		//Generate the stub
		WSDL2Jade wsdl2jade = new WSDL2Jade();
		WSDL2JavaStub wsdl2java = new WSDL2JavaStub();

		String[] axisArgs = new String[3];
		axisArgs[0] = "-o";
		axisArgs[1] = Common.getJaxOutputDir();
		axisArgs[2] = ontoURI;
		wsdl2java.run(axisArgs);

		//		Generate the ontology
		
		wsdl2jade.run(ontoURI);

		//Try to compile the newly generated files
		String cmdDir = (new File(Common.getJadeOutputDir()))
				.getCanonicalPath();
		//String cmd = cmdDir + File.separatorChar + Common.getServiceName()
				//+ Utility.getSystemCommand(Utility.SCRIPT_EXTENSTION);
		String cmd ="." + File.separatorChar + GenCache.ExecFilePrefix
		+ Utility.getSystemCommand(Utility.SCRIPT_EXTENSTION);
		System.out
				.println( "\n" + (new Date()) +":	Generate ontology for " + Common.getServiceName()+"\n" );
		Runtime rt = Runtime.getRuntime();
		Process proc = Runtime.getRuntime().exec(cmd);

		 // any error message?
        StreamGobbler errorGobbler = new 
            StreamGobbler(proc.getErrorStream(), "ERROR");            
        
        // any output?
        StreamGobbler outputGobbler = new 
            StreamGobbler(proc.getInputStream(), "OUTPUT");
            
        // kick them off
        errorGobbler.start();
        outputGobbler.start();

		
		int exitVal = proc.waitFor();

		if(exitVal !=0)
			JOptionPane.showMessageDialog(new javax.swing.JFrame(), "Fail to deploy the service");
		String ontoCls = Common.getJadeGenRootPkg() + ".onto."
				+ Common.getServiceName() + ".WebOntology";

		//Update classpath
		Utility.addFile(Common.getJadeOutputDir() + File.separatorChar
				+ "Onto.jar");
		Utility.addFile(Common.getJaxOutputDir() + File.separatorChar
				+ "Stub.jar");

		//Update Cardinal Mapping
		//if(!remote)
		CardinalMappingManager.addRecord(agentName, new ASWSRecord(serviceName,
				ontoURI));

		return ontoCls;
	}

	
	
	class StreamGobbler extends Thread
	{
	    InputStream is;
	    String type;
	    
	    StreamGobbler(InputStream is, String type)
	    {
	        this.is = is;
	        this.type = type;
	    }
	    
	    public void run()
	    {
	        try
	        {
	            InputStreamReader isr = new InputStreamReader(is);
	            BufferedReader br = new BufferedReader(isr);
	            String line=null;
	            while ( (line = br.readLine()) != null)
	                System.out.print('.'); 
	            
	            System.out.println();
	            } 
	        	catch (IOException ioe)
	              {
	        		Logger log = Logger.getLogger("WS2JADE");
	                log.debug("Exception occurred", ioe);
	              }
	    }
	}

}