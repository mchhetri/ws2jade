package ciamas.uddidemo;

import jade.content.*;
import jade.content.onto.*;
import jade.core.*;
import jade.core.behaviours.*;
import jade.lang.acl.*;
import jade.util.leap.*;
import jade.content.onto.basic.*;
import jade.content.lang.*;
import jade.content.lang.sl.*;
import java.io.*;

import JadeGen.onto.Google.GoogleSearchPort.Request.DoGoogleSearch;
import JadeGen.onto.reflect.GoogleSearch.GoogleSearchResult;
import JadeGen.onto.Google.GoogleSearchPort.Request.DoSpellingSuggestion;
import javax.swing.UIManager;

/*
 * Created on 2/12/2004
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */

public class GoogleClient extends Agent {
    // CREATE THE ONTOLOGY INSTANCE
    private ContentManager manager  = (ContentManager) getContentManager();
    private Codec codec = new SLCodec();
    private Ontology ontology = JadeGen.onto.Google.WebOntology.getInstance();
    public String s_Terms;
    public String s_Count;

/*    public GoogleClient() {
        try {
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }*/

    protected void setup() {
        try {
            UIManager.setLookAndFeel(
                    "com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
            GoogleInput g = new GoogleInput(this);

            synchronized (this) {
                try {
                    this.wait();
                }
                catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }

        // REGISTER THE ONTOLOGY
        manager.registerLanguage(codec);
        manager.registerOntology(ontology);

        // ADD HANDLEINFORMBEHAVIOUR
        addBehaviour(new HandleInformBehaviour(this));

        // CREATE A REQUEST MESSAGE
        ACLMessage requestMsg = new ACLMessage(ACLMessage.REQUEST);

        requestMsg.setSender(getAID());

        // IDENTIFY THE WEB SERVICE AGENT
        requestMsg.addReceiver(new AID("GoogleProvider", false));
        requestMsg.setLanguage(codec.getName());
        requestMsg.setOntology(ontology.getName());


        // INSTANTIATE THE CHOSEN AGENTACTION CLASS
        int max_Result = Integer.parseInt(s_Count);
        DoGoogleSearch requestAction =
            new DoGoogleSearch();
        requestAction.setKey("/muoyLpQFHKE9gCRkTaiN9yZ8KJSatH4");
        requestAction.setMaxResults(max_Result);
        requestAction.setFilter(false);
        requestAction.setIe("");
        requestAction.setLr("lang_en" );
        requestAction.setOe("");
        requestAction.setQ(s_Terms);
        requestAction.setRestrict("0");
        requestAction.setSafeSearch(true);
        requestAction.setStart(0);

        // INSTANTIATE THE CHOSEN AGENTACTION CLASS
        DoSpellingSuggestion d_Suggestion = new DoSpellingSuggestion();
        d_Suggestion.setPhrase("CIAMAS");
        d_Suggestion.setKey("/muoyLpQFHKE9gCRkTaiN9yZ8KJSatH4");

        System.out.println("Client Agent search for word " + s_Terms);
        try
        {
           // SEND THE MESSAGE
/*           manager.fillContent( requestMsg,
                                new Action(new AID("GoogleClient", false), requestAction))*/;
           manager.fillContent( requestMsg,
                                new Action(new AID("GoogleClient", false), d_Suggestion));
           System.out.println(manager.toString());
           send(requestMsg);

       }
       catch(Exception e)
       { e.printStackTrace(); }
   }

   private void jbInit() throws Exception {
   }


   class HandleInformBehaviour extends CyclicBehaviour
   {
       public HandleInformBehaviour(Agent a) { super(a); }

       public void action()
       {
           ACLMessage msg = receive(MessageTemplate.MatchPerformative(ACLMessage.INFORM));
           if (msg != null)
           {
               try
               {
                   ContentElement ce = manager.extractContent(msg);

                   if (ce instanceof jade.content.onto.basic.Result)
                   {
                       jade.content.onto.basic.Result result = (jade.content.onto.basic.Result)ce;
                       System.out.println();
                       System.out.print("Returned value: ");
/*                       JadeGen.onto.Google.GoogleSearchPort.Response.DoGoogleSearch	ret =

            (JadeGen.onto.Google.GoogleSearchPort.Response.DoGoogleSearch)result.getValue();

            GoogleSearchResult search_result = ret.get_return();
            ArrayList lists =search_result.getResultElements();
            for(int i=0;i< lists.size();i++)
            {
                JadeGen.onto.reflect.GoogleSearch.ResultElement aResult = (JadeGen.onto.reflect.GoogleSearch.ResultElement)lists.get(i);
                System.out.println(aResult.getURL());

            }*/
JadeGen.onto.Google.GoogleSearchPort.Response.DoSpellingSuggestion	ret =

(JadeGen.onto.Google.GoogleSearchPort.Response.DoSpellingSuggestion)result.getValue();

String suggestion = ret.get_return();
System.out.println("Result " + suggestion);
        }

    } catch(Exception e)
    {
        e.printStackTrace();
    }
} else { block(); }
       }
   }
}
