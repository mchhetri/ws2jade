/*
 * CartItem.java
 *
 * Created on 31 January 2005, 15:59
 */

package ciamas.ws2jadedemo.ontology;

import jade.content.*;

/**
 *
 * @author  403515
 */
public class CartItem implements Concept {
    
    private String itemId = null;
    private String quantity = null;
    private boolean hasCart = false;
    private String HMAC = null;
    private String cartId = null;
    
    /** Creates a new instance of CartItem */
    public CartItem() {
    }
    
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }
    
    public String getItemId() {
        return itemId;
    }
    
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }
    
    public String getQuantity() {
        return quantity;
    }
    
    public void setHasCart(boolean hasCart) {
        this.hasCart = hasCart;
    }
    
    public boolean getHasCart() {
        return hasCart;
    }
    
    public void setHMAC(String HMAC) {
        this.HMAC = HMAC;
    }
    
    public String getHMAC() {
        return HMAC;
    }
    
    public void setCartId(String cartId) {
        this.cartId = cartId;
    }
    
    public String getCartId() {
        return cartId;
    }
    
}
