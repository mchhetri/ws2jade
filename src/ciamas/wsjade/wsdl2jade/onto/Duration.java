/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.wsdl2jade.onto;

/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Duration  implements jade.content.Concept{

	private int days;
	private int hours;
	private boolean isNegative;
	private int minutes;
	private int months;
	private double seconds;
	private int years;
	
	/**
	 * @return Returns the days.
	 */
	public int getDays() {
		return days;
	}
	/**
	 * @param days The days to set.
	 */
	public void setDays(int days) {
		this.days = days;
	}
	/**
	 * @return Returns the hours.
	 */
	public int getHours() {
		return hours;
	}
	/**
	 * @param hours The hours to set.
	 */
	public void setHours(int hours) {
		this.hours = hours;
	}
	/**
	 * @return Returns the isNegative.
	 */
	public boolean isNegative() {
		return isNegative;
	}
	/**
	 * @param isNegative The isNegative to set.
	 */
	public void setNegative(boolean isNegative) {
		this.isNegative = isNegative;
	}
	/**
	 * @return Returns the minutes.
	 */
	public int getMinutes() {
		return minutes;
	}
	/**
	 * @param minutes The minutes to set.
	 */
	public void setMinutes(int minutes) {
		this.minutes = minutes;
	}
	/**
	 * @return Returns the months.
	 */
	public int getMonths() {
		return months;
	}
	/**
	 * @param months The months to set.
	 */
	public void setMonths(int months) {
		this.months = months;
	}
	/**
	 * @return Returns the seconds.
	 */
	public double getSeconds() {
		return seconds;
	}
	/**
	 * @param seconds The seconds to set.
	 */
	public void setSeconds(double seconds) {
		this.seconds = seconds;
	}
	/**
	 * @return Returns the years.
	 */
	public int getYears() {
		return years;
	}
	/**
	 * @param years The years to set.
	 */
	public void setYears(int years) {
		this.years = years;
	}
}
