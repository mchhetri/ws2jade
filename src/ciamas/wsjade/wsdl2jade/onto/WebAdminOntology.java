/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.wsdl2jade.onto;

import jade.content.onto.BasicOntology;
import jade.content.onto.Ontology;
import jade.content.onto.OntologyException;
import jade.content.schema.AgentActionSchema;
import jade.content.schema.ConceptSchema;
import jade.content.schema.PrimitiveSchema;

public class WebAdminOntology extends Ontology {
	public static final String ONTOLOGY_NAME = "ciamas.wsadmin-ontology";

	// The singleton instance of this ontology
	private static Ontology theInstance = new WebAdminOntology(BasicOntology
			.getInstance());

	public static Ontology getInstance() {
		return theInstance;
	}

	/** * Constructor ** */
	private WebAdminOntology(Ontology base) {
		super(ONTOLOGY_NAME, base);
		try {

			add(new AgentActionSchema("Onto-Register-Request"),
					ciamas.wsjade.wsdl2jade.utils.AddOntAction.class);

			AgentActionSchema ontReg = (AgentActionSchema) getSchema("Onto-Register-Request");
			ontReg.add("ontClassName",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			ontReg.add("serviceName",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));

			add(new AgentActionSchema("Service-Deployment-Request"),
					ciamas.wsjade.wsdl2jade.onto.ServiceDeploymentRequest.class);

			AgentActionSchema adepReq = (AgentActionSchema) getSchema("Service-Deployment-Request");
			adepReq.add("uri",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			adepReq.add("agentName",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			adepReq.add("serviceName",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			adepReq.add("username",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			adepReq.add("password",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			
			add(new AgentActionSchema("Agent-Deployment-Request"),
					ciamas.wsjade.wsdl2jade.onto.AgentDeploymentRequest.class);

			AgentActionSchema depReq = (AgentActionSchema) getSchema("Agent-Deployment-Request");
			depReq.add("agentName",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			depReq.add("username",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			depReq.add("password",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			
			
			add(new ConceptSchema("Deployment Response"),
					ciamas.wsjade.wsdl2jade.onto.DeploymentResponse.class);

			ConceptSchema depRes = (ConceptSchema) getSchema("Deployment Response");
			depRes.add("success",
				(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.BOOLEAN));
			depRes.add("reason",
					(PrimitiveSchema) getSchema(jade.content.onto.BasicOntology.STRING));
			
		} catch (OntologyException oe) {
			oe.printStackTrace();
		}
	}
}