package ciamas.uddidemo;

/**
 * <p>Title: My Jade Examples</p>
 *
 * <p>Description: </p>
 *
 * <p>Copyright: Copyright (c) 2005</p>
 *
 * <p>Company: CIAMAS, Swinburne University</p>
 *
 * @author not attributable
 * @version 1.0
 */

import javax.swing.Icon;

public class NodeObject {
        public NodeObject(String t, String n, boolean b) {
                this.text = t;
                //this.icon = new ImageIcon(getClass().getResource(n));
                this.deployed = b;
                this.cmd = n;
        }

        // Return text for display
        public String toString() {
                return text;
        }

//	 Return text for special control
        public String getCmd() {
                return cmd;
        }

        // Return the icon
        public Icon getIcon() {
                return icon;
        }

        public boolean isDeploy() {
                return deployed;
        }

        public void setDeploy(boolean b) {
                deployed = b;
        }

        protected String text;

        protected Icon icon;

        protected boolean deployed;
        protected String cmd;
}

