/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/
package ciamas.wsjade.wsdl2jade.onto;

/**
 * @author xnguyen
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class MonthDay  implements jade.content.Concept{

	private int month;
	private int day;
	private String timezone;
	/**
	 * @return Returns the day.
	 */
	public int getDay() {
		return day;
	}
	/**
	 * @param day The day to set.
	 */
	public void setDay(int day) {
		this.day = day;
	}
	/**
	 * @return Returns the month.
	 */
	public int getMonth() {
		return month;
	}
	/**
	 * @param month The month to set.
	 */
	public void setMonth(int month) {
		this.month = month;
	}
	/**
	 * @return Returns the timezone.
	 */
	public String getTimezone() {
		return timezone;
	}
	/**
	 * @param timezone The timezone to set.
	 */
	public void setTimezone(String timezone) {
		this.timezone = timezone;
	}
}
