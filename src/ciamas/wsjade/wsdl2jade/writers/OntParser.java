/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Properties;
import java.util.Vector;

import javax.wsdl.WSDLException;
import javax.xml.namespace.QName;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.axis.encoding.DefaultSOAPEncodingTypeMappingImpl;
import org.apache.axis.encoding.DefaultTypeMappingImpl;
import org.apache.axis.encoding.TypeMapping;
import org.apache.axis.enum.Scope;
import org.apache.axis.i18n.Messages;
import org.apache.axis.utils.ClassUtils;
import org.apache.axis.utils.JavaUtils;
import org.apache.axis.wsdl.gen.GeneratorFactory;
import org.apache.axis.wsdl.symbolTable.BaseTypeMapping;
import org.apache.axis.wsdl.symbolTable.SymTabEntry;
import org.apache.axis.wsdl.symbolTable.SymbolTable;
import org.apache.axis.wsdl.toJava.Emitter;
import org.apache.axis.wsdl.toJava.GeneratedFileInfo;
import org.apache.axis.wsdl.toJava.Namespaces;
import org.apache.axis.wsdl.toJava.Utils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;
import org.apache.log4j.Logger;

public class OntParser extends Emitter {

	public static final String DEFAULT_NSTOPKG_FILE = "NStoPkg.properties";

	protected HashMap namespaceMap = new HashMap();

	protected String typeMappingVersion = "1.1";

	protected BaseTypeMapping baseTypeMapping = null;

	protected Namespaces namespaces = null;

	protected String NStoPkgFilename = null;

	private boolean bEmitServer = false;

	private boolean bDeploySkeleton = false;

	private boolean bEmitTestCase = false;

	private boolean bGenerateAll = false;

	private boolean bHelperGeneration = false;

	private String packageName = null;

	private Scope scope = null;

	private GeneratedFileInfo fileInfo = new GeneratedFileInfo();

	private HashMap delayedNamespacesMap = new HashMap();

	private String outputDir = null;

	/**
	 * Default constructor.
	 */
	public OntParser() {
		setFactory(new JadeGeneratorFactory(this));

	}

	public void setFactory(String factory) {
		try {
			Class clazz = ClassUtils.forName(factory);
			GeneratorFactory genFac = null;
			try {
				Constructor ctor = clazz
						.getConstructor(new Class[] { getClass() });
				genFac = (GeneratorFactory) ctor
						.newInstance(new Object[] { this });
			} catch (NoSuchMethodException ex) {
				genFac = (GeneratorFactory) clazz.newInstance();
			}
			setFactory(genFac);
		} catch (Exception ex) {
			//ex.printStackTrace();
			Logger.getLogger("WS2JADE").debug("Exception occurred", ex);
			
		}
	} // setFactory

	/**
	 * Reuse from Axis
	 */

	public String getJavaName(QName qName) {
		//Common common = Common.getInstance();
		//String commonDir = common.getCommonDir();

		// If this is one of our special 'collection' qnames.
		// get the element type and append []
		if (qName.getLocalPart().indexOf("[") > 0) {
			String localPart = qName.getLocalPart().substring(0,
					qName.getLocalPart().indexOf("["));
			QName eQName = new QName(qName.getNamespaceURI(), localPart);
			return getJavaName(eQName) + "[]";
		}

		// Handle the special "java" namespace for types
		if (qName.getNamespaceURI().equalsIgnoreCase("java")) {
			return qName.getLocalPart();
		}

		// The QName may represent a base java name, so check this first
		String fullJavaName = getFactory().getBaseTypeMapping().getBaseName(
				qName);
		if (fullJavaName != null)
			return fullJavaName;

		// Use the namespace uri to get the appropriate package
		String pkg = getPackage(qName.getNamespaceURI());
		if (pkg != null) {
			fullJavaName = pkg + "."
					+ Utils.xmlNameToJavaClass(qName.getLocalPart());
		} else {
			fullJavaName = Utils.xmlNameToJavaClass(qName.getLocalPart());
		}
		return fullJavaName;
	} // getJavaName

	public void run(String wsdlURL) throws Exception {
		setup();
		//Create the temp store for onto schema
		OntSchemaTemp ontSchemaTemp = OntSchemaTemp.getInstance();
		WSTypeMappingTemp typeMappingTemp = WSTypeMappingTemp.getInstance();
		JadeAgentWriter agentWriter = JadeAgentWriter.getInstance();
		GenCache genCache = GenCache.getInstance();
		genCache.setParser(this);

		super.run(wsdlURL);

		//System.out.println(ontSchemaTemp.getText());
		ontSchemaTemp.generate();
		typeMappingTemp.generate();
		//agentWriter.generate();
		genCache.generate();

	} // run

	public void run(String context, Document doc) throws IOException,
			SAXException, WSDLException, ParserConfigurationException {
		setup();
		super.run(context, doc);
	} // run

	private void setup() throws IOException {

		//Set the output directory
		//setOutputDir(Common.getInstance().getCommonDir());
		if (baseTypeMapping == null) {
			setTypeMappingVersion(typeMappingVersion);
		}
		getFactory().setBaseTypeMapping(baseTypeMapping);

		namespaces = new Namespaces(outputDir);

		if (packageName != null) {
			namespaces.setDefaultPackage(packageName);
		} else {

			getNStoPkgFromPropsFile(namespaces);

			if (delayedNamespacesMap != null) {
				namespaces.putAll(delayedNamespacesMap);
			}
		}
	} // setup

	protected void sanityCheck(SymbolTable symbolTable) {
		Iterator it = symbolTable.getHashMap().values().iterator();
		while (it.hasNext()) {
			Vector v = (Vector) it.next();
			for (int i = 0; i < v.size(); ++i) {
				SymTabEntry entry = (SymTabEntry) v.elementAt(i);
				String namespace = entry.getQName().getNamespaceURI();
				String packageName = org.apache.axis.wsdl.toJava.Utils
						.makePackageName(namespace);
				String localName = entry.getQName().getLocalPart();
				if (localName.equals(packageName)
						&& packageName.equals(namespaces.getCreate(namespace))) {
					packageName += "_pkg";
					namespaces.put(namespace, packageName);
				}

			}
		}
	}

	private void getNStoPkgFromPropsFile(HashMap namespaces) throws IOException {

		Properties mappings = new Properties();
		if (NStoPkgFilename != null) {
			try {
				mappings.load(new FileInputStream(NStoPkgFilename));
				if (verbose) {
					System.out.println(Messages.getMessage(
							"nsToPkgFileLoaded00", NStoPkgFilename));
				}
			} catch (Throwable t) {
				// loading the custom mapping file failed. We do not try
				// to load the mapping from a default mapping file.
				throw new IOException(Messages.getMessage(
						"nsToPkgFileNotFound00", NStoPkgFilename));
			}
		} else {
			try {
				mappings.load(new FileInputStream(DEFAULT_NSTOPKG_FILE));
				if (verbose) {
					System.out.println(Messages.getMessage(
							"nsToPkgFileLoaded00", DEFAULT_NSTOPKG_FILE));
				}
			} catch (Throwable t) {
				try {
					mappings.load(ClassUtils.getResourceAsStream(
							OntParser.class, DEFAULT_NSTOPKG_FILE));
					//if (verbose) {
						//System.out.println(Messages.getMessage(
								//"nsToPkgDefaultFileLoaded00",
								//DEFAULT_NSTOPKG_FILE));
					//}

				} catch (Throwable t1) {
					// loading the default mapping file failed.
					// The built-in default mapping is used
					// No message is given, since this is generally what happens
				}
			}
		}

		Enumeration keys = mappings.propertyNames();
		while (keys.hasMoreElements()) {
			String key = (String) keys.nextElement();
			namespaces.put(key, mappings.getProperty(key));
		}
	} // getNStoPkgFromPropsFile

	public void setTypeMappingVersion(String typeMappingVersion) {
		if (typeMappingVersion.equals("1.1")) {
			baseTypeMapping = new BaseTypeMapping() {
				final TypeMapping defaultTM = DefaultTypeMappingImpl
						.getSingletonDelegate();

				public String getBaseName(QName qNameIn) {
					javax.xml.namespace.QName qName = new javax.xml.namespace.QName(
							qNameIn.getNamespaceURI(), qNameIn.getLocalPart());
					Class cls = defaultTM.getClassForQName(qName);
					if (cls == null)
						return null;
					else
						return JavaUtils.getTextClassName(cls.getName());
				}
			};
		} else {
			baseTypeMapping = new BaseTypeMapping() {
				final TypeMapping defaultTM = DefaultSOAPEncodingTypeMappingImpl
						.createWithDelegate();

				public String getBaseName(QName qNameIn) {
					javax.xml.namespace.QName qName = new javax.xml.namespace.QName(
							qNameIn.getNamespaceURI(), qNameIn.getLocalPart());
					Class cls = defaultTM.getClassForQName(qName);
					if (cls == null)
						return null;
					else
						return JavaUtils.getTextClassName(cls.getName());
				}
			};
		}
	}

}