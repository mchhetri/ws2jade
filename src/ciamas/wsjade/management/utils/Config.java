/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.management.utils;

import java.io.File;

/**
 * 
 * @author xnguyen
 */
public class Config {

	/** Creates a new instance of Config */
	private String jadeServerName;

	private int jadeServerPort;

	private String jadeOutDir;

	private String stubOutDir;

	private boolean isMainContainer;

	public static String DEFAULT_SERVER = "localhost";

	public static int DEFAULT_SERVER_PORT = 7778;

	public static String DEFAULT_ONTO_DIR = "." + File.separatorChar + "output";

	public static String DEFAULT_STUB_DIR = "." + File.separatorChar + "output";

	public static boolean DEFAULT_MAIN_CONTAINER = true;

	public Config() {
		//Default
		jadeServerName = DEFAULT_SERVER;
		jadeServerPort = DEFAULT_SERVER_PORT;
		jadeOutDir = DEFAULT_ONTO_DIR;
		stubOutDir = DEFAULT_STUB_DIR;
		isMainContainer = true;
	}

	public Config(String sName, int port, String jOut, String sOut, boolean m) {
		jadeServerName = sName;
		jadeServerPort = port;
		jadeOutDir = jOut;
		stubOutDir = sOut;
		isMainContainer = m;
	}

	public String getJadeSName() {
		return jadeServerName;
	}

	public void setJadeSName(String s) {
		jadeServerName = s;
	}

	public int getJadePort() {
		return jadeServerPort;
	}

	public void setJadePort(int i) {
		jadeServerPort = i;
	}

	public String getJadeOutDir() {
		return jadeOutDir;
	}

	public void setJadeOutDir(String s) {
		jadeOutDir = s;
	}

	public String getStubOutDir() {
		return stubOutDir;
	}

	public void setStubOutDir(String s) {
		stubOutDir = s;
	}

	public boolean isMainContainer() {
		return this.isMainContainer;
	}

	public void setMainContainer(boolean b) {
		isMainContainer = b;
	}

}