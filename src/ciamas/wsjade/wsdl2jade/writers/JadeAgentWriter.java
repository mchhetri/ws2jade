/*
* This package is free software, you can redistribute it 
* and/or modify 
* it under  the terms of the GNU Lesser General Public License 
* as published by the Free Software Foundation; 
* either version 2 of the License, or any later version.
* The library is distributed in the hope that it will be useful, 
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
* See the GNU Lesser General Public License for more details.
*/

package ciamas.wsjade.wsdl2jade.writers;

//FIX ME: This file is not in use
import java.io.File;
import java.io.FileWriter;
import java.util.Vector;

import ciamas.wsjade.wsdl2jade.utils.Common;
import ciamas.wsjade.wsdl2jade.utils.PrintWriterUtils;

public class JadeAgentWriter {

	private static JadeAgentWriter me = null;

	private Vector agentActionClasses = new Vector();

	private Vector agentConceptClasses = new Vector();

	String pkg = null;

	private JadeAgentWriter() {
	}

	public static JadeAgentWriter getInstance() {
		if (me == null)
			me = new JadeAgentWriter();
		return me;
	}

	public void register(String cls, int type) {
		if (type == Common.CONCEPT)
			agentConceptClasses.addElement(cls);
		else if (type == Common.ACTION)
			agentActionClasses.addElement(cls);
	}

	public void generate() {
		pkg = Common.getJadeGenRootPkg() + ".mapping."
				+ Common.getServiceName();
		String pkgDir = pkg.replace('.', File.separatorChar);

		try {
			String outputDir = Common.getJadeOutputDir() + File.separatorChar
					+ pkgDir;

			File file = new File(outputDir + File.separatorChar
					+ "WebAgent.java");
			File parent = new File(file.getParent());
			if (!parent.exists())
				parent.mkdirs();

			PrintWriterUtils out = new PrintWriterUtils(new FileWriter(file));
			Common.printBanner(out);
			out.println();
			out.println("package " + pkg + ";");
			out.println();
			writeImportHeader(out);
			writeClassHeader(out);
			writeSetupMethod(out);
			writeBehaviourClass(out);
			//writeBehaviourActionBody(out);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	//FIX ME: Following methods should be read out from a file
	private void writeImportHeader(PrintWriterUtils out) {
		

	}

	private void writeClassHeader(PrintWriterUtils out) {
		out.println("public class WebAgent extends Agent ");
		out.println("{");
		out.setTabNum(1);
		out
				.println("private ContentManager manager = (ContentManager) getContentManager();");
		out.println("private Codec codec = new SLCodec();");

	}

	private void writeSetupMethod(PrintWriterUtils out) {

		String ontPkg = Common.getJadeGenRootPkg() + ".onto."
				+ Common.getServiceName();

		String ontClassName = ontPkg + ".WebOntology";
		out.println();
		out.println("protected void setup() ");
		out.println("{");
		out.setTabNum(2);
		out.println("manager.registerLanguage(codec);");
		out.println("manager.registerOntology(" + ontClassName
				+ ".getInstance());");
		out.println("addBehaviour(new HandleRequestBehaviour(this));");
		out.setTabNum(1);
		out.println("}");
		out.println();

	}

	private void writeBehaviourClass(PrintWriterUtils out) {
		out.write("}");
		
	}

}