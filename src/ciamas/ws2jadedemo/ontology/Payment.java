/*
 * Payment.java
 *
 * Created on 9 February 2005, 14:39
 */

package ciamas.ws2jadedemo.ontology;

import jade.content.*;

/**
 *
 * @author  403515
 */
public class Payment implements Concept {
    
    private String emailAddress;
    private String password;
    private String recipientEmailAddress;
    private String amount;
    private String currency;
    private String note;
    
    /** Creates a new instance of Payment */
    public Payment() {
    }
    
    public void setEmailAddress(String emailAddress) {
        this.emailAddress = emailAddress; 
    }
    
    public String getEmailAddress() {
        return emailAddress;
    }
    
    public void setPassword(String password) {
        this.password = password; 
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setRecipientEmailAddress(String recipientEmailAddress) {
        this.recipientEmailAddress = recipientEmailAddress; 
    }
    
    public String getRecipientEmailAddress() {
        return recipientEmailAddress;
    }
    
    public void setAmount(String amount) {
        this.amount = amount;
    }
    
    public String getAmount() {
        return amount;
    }
    
     public void setCurrency(String currency) {
        this.currency = currency; 
    }
    
    public String getCurrency() {
        return currency;
    }
    
     public void setNote(String note) {
        this.note = note; 
    }
    
    public String getNote() {
        return note;
    }
    
}
